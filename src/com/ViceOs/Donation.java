package com.ViceOs;

import java.sql.ResultSet;

import com.ViceOs.world.entity.impl.player.Player;

public class Donation implements Runnable {

	public static final String HOST_ADDRESS = "198.38.82.92";
	public static final String USERNAME = "brunor95_donate";
	public static final String PASSWORD = "brunor9595";
	public static final String DATABASE = "brunor95_donate";
	
	private Player player;
	
	@Override
	public void run() {
		try {
			Database db = new Database(HOST_ADDRESS, USERNAME, PASSWORD, DATABASE);
			
			if (!db.init()) {
				System.err.println("[Donation] Failed to connect to database!");
				return;
			}
			
			String name = player.getUsername().replace("_", " ");
			ResultSet rs = db.executeQuery("SELECT * FROM payments WHERE player_name='"+name+"' AND claimed=0");
			
			while(rs.next()) {
				String item_name = rs.getString("item_name");
				int item_number = rs.getInt("item_number");
				double amount = rs.getDouble("amount");
				int quantity = rs.getInt("quantity");
				
				ResultSet result = db.executeQuery("SELECT * FROM products WHERE item_id="+item_number+" LIMIT 1");
				
				if (result == null || !result.next()
						|| !result.getString("item_name").equalsIgnoreCase(item_name)
						|| result.getDouble("item_price") != amount
						|| quantity < 1 || quantity > Integer.MAX_VALUE) {
					System.out.println("[Donation] Invalid purchase for "+name+" (item: "+item_name+", id: "+item_number+")");
					continue;
				}
				
				handleItems(item_number, quantity);
				rs.updateInt("claimed", quantity);
				rs.updateRow();
			}
			
			db.destroyAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void handleItems(int productId, int quantity) {
		switch(productId) {
		case 10:
			player.getPacketSender().sendMessage("<shad=cc0ff><img=1>You Have Recieved Your donation! Thank you for your support!");
			player.getPointsHandler().incrementDonationPoints(5);
			break;
		case 11:
			player.getPacketSender().sendMessage("<shad=cc0ff><img=1>You Have Recieved Your donation! Thank you for your support!");
			player.getPointsHandler().incrementDonationPoints(10); 
			break;
		case 12:
			player.getPacketSender().sendMessage("<shad=cc0ff><img=1>You Have Recieved Your donation! Thank you for your support!");
			player.getPointsHandler().incrementDonationPoints(15); 
			break;
		case 13:
			player.getPacketSender().sendMessage("<shad=cc0ff><img=1>You Have Recieved Your donation! Thank you for your support!");
			player.getPointsHandler().incrementDonationPoints(20);
			break;
		case 14:
			player.getPacketSender().sendMessage("<shad=cc0ff><img=1>You Have Recieved Your donation! Thank you for your support!");
			player.getPointsHandler().incrementDonationPoints(25);
			break;
		case 15:
			player.getPacketSender().sendMessage("<shad=cc0ff><img=1>You Have Recieved Your donation! Thank you for your support!");
			player.getPointsHandler().incrementDonationPoints(30);
			break;
		case 16:
			player.getPacketSender().sendMessage("<shad=cc0ff><img=1>You Have Recieved Your donation! Thank you for your support!");
			player.getPointsHandler().incrementDonationPoints(35);
			break;
		case 17:
			player.getPacketSender().sendMessage("<shad=cc0ff><img=1>You Have Recieved Your donation! Thank you for your support!");
			player.getPointsHandler().incrementDonationPoints(40);
			break;
		case 18:
			player.getPacketSender().sendMessage("<shad=cc0ff><img=1>You Have Recieved Your donation! Thank you for your support!");
			player.getPointsHandler().incrementDonationPoints(45);
			break;
		case 19:
			player.getPacketSender().sendMessage("<shad=cc0ff><img=1>You Have Recieved Your donation! Thank you for your support!");
			player.getPointsHandler().incrementDonationPoints(50);
			break;
		case 20:
			player.getPacketSender().sendMessage("<shad=cc0ff><img=1>You Have Recieved Your donation! Thank you for your support!");
			player.getPointsHandler().incrementDonationPoints(100);
			break;
		}
	}
	
	public Donation(Player player) {
		this.player = player;
	}
}
