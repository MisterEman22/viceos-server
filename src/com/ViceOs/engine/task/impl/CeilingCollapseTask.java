package com.ViceOs.engine.task.impl;


import com.ViceOs.engine.task.Task;
import com.ViceOs.model.CombatIcon;
import com.ViceOs.model.Graphic;
import com.ViceOs.model.Hit;
import com.ViceOs.model.Hitmask;
import com.ViceOs.model.Locations.Location;
import com.ViceOs.util.RandomUtility;
import com.ViceOs.world.entity.impl.player.Player;

/**
 * Barrows
 * @author Gabriel Hannason
 */
public class CeilingCollapseTask extends Task {

	public CeilingCollapseTask(Player player) {
		super(9, player, false);
		this.player = player;
	}

	private Player player;

	@Override
	public void execute() {
		if(player == null || !player.isRegistered() || player.getLocation() != Location.BARROWS || player.getLocation() == Location.BARROWS && player.getPosition().getY() < 8000) {
			player.getPacketSender().sendCameraNeutrality();
			stop();
			return;
		}
		player.performGraphic(new Graphic(60));
		player.getPacketSender().sendMessage("Some rocks fall from the ceiling and hit you.");
		player.forceChat("Ouch!");
		player.dealDamage(new Hit(30 + RandomUtility.getRandom(20), Hitmask.RED, CombatIcon.BLOCK));
	}
}
