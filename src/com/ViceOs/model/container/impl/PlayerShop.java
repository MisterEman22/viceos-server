package com.ViceOs.model.container.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ViceOs.model.Item;
import com.ViceOs.model.container.ItemContainer;
import com.ViceOs.model.definitions.ItemDefinition;
import com.ViceOs.world.World;
import com.ViceOs.world.content.PlayerOwnedShops;
import com.ViceOs.world.entity.impl.player.Player;

/**
 * Created by Greg on 25/04/2017.
 */
public class PlayerShop extends Shop {

    private List<SoldItem> salesQueue = new ArrayList<>();

    public void addSale(int item, int amount, int price) {
        salesQueue.add(new SoldItem(item, amount, price));
        checkSales();
    }

    public void checkSales() {
        Player player = World.getPlayerByName(username);
        if(player == null)//Player not online
            return;
        long total = 0;
        for(int i = 0; i < salesQueue.size(); i++) {
            SoldItem sale = salesQueue.get(i);
            if(sale.itemId == -2)
                player.getPacketSender().sendMessage("[@red@POS@bla@] There was a problem with your featured shop; you have been refunded " + PlayerOwnedShops.shopPrice(sale.price  * sale.amount) + ".");
            else
                player.getPacketSender().sendMessage("[@red@POS@bla@] You sold @red@" + sale.amount + "x@bla@ " + ItemDefinition.forId(sale.itemId).getName() + " for @red@" + PlayerOwnedShops.shopPrice(sale.price  * sale.amount));
            int coins = sale.amount * sale.price;
            //-5% Shop Tax
            if(PlayerOwnedShops.SHOP_TAX != 1.0 && coins > PlayerOwnedShops.SHOP_TAX && coins < PlayerOwnedShops.SHOP_TAX_MAX)
                coins -= ((coins/100) * PlayerOwnedShops.SHOP_TAX);
            player.setMoneyInPouch(player.getMoneyInPouch() + coins);
        }
        salesQueue.clear();
        player.getPacketSender().sendString(8135, "" + player.getMoneyInPouch());
        publicRefresh();
    }

    public void update() {
        lastUpdated = System.currentTimeMillis();
    }

    public String getLastUpdated() {
        return PlayerOwnedShops.milliToReadable(System.currentTimeMillis() - lastUpdated);
    }

    public class SoldItem {
        private int itemId;
        private int amount;
        private int price;

        public SoldItem(int itemId, int amount, int price) {
            this.itemId = itemId;
            this.amount = amount;
            this.price = price;
        }
    }
    /**
     * Add to sales queue
     *
     * every other 15 seconds save sales queue
     * every other 15 seconds check sales
     * for every sale in queue
     * check if user is online
     */

    private Map<Integer, Integer> prices = new HashMap<>();
    public long featuredTime;
    public long lastUpdated;

    public PlayerShop(Player player, int id, String username, Item currency, Item[] stockItems, int[] prices, long featuredTime, long lastUpdated) {
        super(player, id, username + "'s Store", currency, stockItems, null);
        for (int i = 0; i < prices.length; i++)
            this.prices.put(stockItems[i].getId(), prices[i]);
        this.featuredTime = featuredTime;
        this.lastUpdated = lastUpdated;
        this.username = username;
    }

    public static boolean shopBuysItem(Player player, String username) {
        PlayerShop shop = PlayerOwnedShops.getShops().get(username);
        if (shop != null) {
            if (player.getUsername().equals(shop.username))
                return true;
        }
        return false;
    }

    //this file
    public void setPrice(int item, int price) {
    	//System.out.println("ITEM: " + item + " PRICE: " + price);
        if(ItemDefinition.forId(item).isNoted()) {
        	//System.out.println("THINKINS NOTED: " + item  + " PRICE: " + price);
            item -= 1;
        }
        prices.put(item, price);
    }

    public int getPrice(Item item) {
        int id = item.getDefinition().isNoted() ? item.getId() -1 : item.getId();
        if (prices.containsKey(id))
            return prices.get(id);
        return -1;
    }

    public List<Integer> getValidPrices() {
        List<Integer> prices = new ArrayList<>();
        for (Item item : getValidItems()) {
            int i = getPrice(item);
            prices.add(i);
        }
        return prices;
    }

    public void addToSalesQueue(SoldItem[] sales) {
        for(SoldItem sale : sales)
            salesQueue.add(sale);
    }

    public List<SoldItem> getSalesQueue() {
        return salesQueue;
    }

    @Override
    public Shop open(Player player) {
        if (!player.getLocation().canBank(player)) {
            player.getPacketSender().sendMessage("You cannot open player owned shops here.");
        }
        return super.open(player);
    }

    @Override
    public Shop switchItem(ItemContainer to, Item item, int slot, boolean sort, boolean refresh) {

        return super.switchItem(to, item, slot, sort, refresh);
    }
}
