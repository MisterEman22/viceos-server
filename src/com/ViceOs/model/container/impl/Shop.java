package com.ViceOs.model.container.impl;

import com.ViceOs.engine.task.Task;
import com.ViceOs.engine.task.TaskManager;
import com.ViceOs.engine.task.impl.ShopRestockTask;
import com.ViceOs.model.GameMode;
import com.ViceOs.model.Item;
import com.ViceOs.model.PlayerRights;
import com.ViceOs.model.Skill;
import com.ViceOs.model.container.ItemContainer;
import com.ViceOs.model.container.StackType;
import com.ViceOs.model.definitions.ItemDefinition;
import com.ViceOs.model.definitions.drops.DropTable;
import com.ViceOs.model.definitions.drops.DropTableEntry;
import com.ViceOs.model.input.impl.EnterAmountToBuyFromShop;
import com.ViceOs.model.input.impl.EnterAmountToSellItemFor;
import com.ViceOs.model.input.impl.EnterAmountToSellToShop;
import com.ViceOs.util.JsonLoader;
import com.ViceOs.util.Misc;
import com.ViceOs.world.World;
import com.ViceOs.world.content.PlayerOwnedShops;
import com.ViceOs.world.content.PlayerPanel;
import com.ViceOs.world.content.PointsHandler;
import com.ViceOs.world.content.minigames.impl.RecipeForDisaster;
import com.ViceOs.world.entity.impl.player.Player;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Messy but perfect Shop System
 * 
 * @author Gabriel Hannason
 */

public class Shop extends ItemContainer {

	/*
	 * The shop constructor
	 */
	public Shop(Player player, int id, String name, Item currency, Item[] stockItems, int[] prices) {
		super(player);
		if (stockItems.length > 42)
			throw new ArrayIndexOutOfBoundsException(
					"Stock cannot have more than 40 items; check shop[" + id + "]: stockLength: " + stockItems.length);
		this.id = id;
		this.name = name.length() > 0 ? name : "General Store";
		this.currency = currency;
		this.originalStock = new Item[stockItems.length];
		this.prices = prices;
		for (int i = 0; i < stockItems.length; i++) {
			Item item = new Item(stockItems[i].getId(), stockItems[i].getAmount());
			add(item, false);
			this.originalStock[i] = item;
		}
	}

	private final int id;

	public String username;

	private String name;

	private Item currency;

	private Item[] originalStock;
	private int[] prices;

	public Item[] getOriginalStock() {
		return this.originalStock;
	}

	public boolean isInOriginalStock(int itemId) {
		for (Item item : getOriginalStock()) {
			if (item.getId() == itemId) {
				return true;
			}
		}
		return false;
	}

	public int getId() {
		return this.id;
	}

	public String getName() {
		return name;
	}

	public Shop setName(String name) {
		this.name = name;
		return this;
	}

	public Item getCurrency() {
		return currency;
	}
	
	public int getType(int shopId) {
		//1 = tokkul
		//2 = donator
		//3 = donation
		//4 = pk
		//5 = slayer/boss
		//donator stores
		if (shopId == PKING_REWARDS_STORE){ 
			return 4;
		}
		if (shopId == DUNGEONEERING_STORE) {
			return 10;
		}
		if (shopId == TRIVIA_STORE) {
			return 8;
		}
		if (shopId == DONATOR_STORE_1) {
			return 2;
		}
		if (shopId == DONATOR_STORE_2) {
			return 2;
		}
		if (shopId == STARDUST_STORE) {
			return 9;
		}
		if (shopId == 169) {
			return 2;
		}
		if (shopId == RECIPE_FOR_DISASTER_STORE) {
			return 0;
		}
		if (shopId == TOKKUL_EXCHANGE_STORE) {
			return 1;
		}
		if (shopId == SLAYER_STORE) {
			return 5;
		}
		if (shopId == VOTING_REWARDS_STORE) {
			return 3;
		}
		if (shopId == BOSS_POINT_STORE) {
			return 5;
		}
		
		//regular
		return 0;
	}

	public Shop setCurrency(Item currency) {
		this.currency = currency;
		return this;
	}

	private boolean restockingItems;

	public boolean isRestockingItems() {
		return restockingItems;
	}

	public void setRestockingItems(boolean restockingItems) {
		this.restockingItems = restockingItems;
	}

	/**
	 * Opens a shop for a player
	 * 
	 * @param player
	 *            The player to open the shop for
	 * @return The shop instance
	 */
	//this file
	public Shop open(Player player) {
		setPlayer(player);
		getPlayer().getPacketSender().sendInterfaceRemoval().sendClientRightClickRemoval();
		getPlayer().setShop(id != -1 ? ShopManager.getShops().get(id) : PlayerOwnedShops.getShops().get(username)).setInterfaceId(INTERFACE_ID).setShopping(true);
		refreshItems();
		int type = getType(id);

		if(id == -1){
			getPlayer().getPacketSender().sendShopInfo(type, PlayerOwnedShops.getShops().get(username).getItems(), prices, username);
		} else {
			getPlayer().getPacketSender().sendShopInfo(type, originalStock, prices, username);
		}
		player.viewingUser = username;
		if (Misc.getMinutesPlayed(getPlayer()) <= 190 && id != -1)
			getPlayer().getPacketSender()
					.sendMessage("Note: When selling an item to a store, it loses 15% of its original value.");
		return this;
	}

	/**
	 * Refreshes a shop for every player who's viewing it
	 */
	public void publicRefresh() {
		Shop publicShop = id != -1 ? ShopManager.getShops().get(id) : PlayerOwnedShops.getShops().get(username);
		if (publicShop == null)
			return;
		publicShop.setItems(getItems());
		for (Player player : World.getPlayers()) {
			if (player == null || player.getShop() == null)
				continue;
			if (player.getShop() != null && player.getShop().id == id && (publicShop.username != null && player.getShop().username != null && player.getShop().username.equals((publicShop.username))) && player.isShopping())
				player.getShop().setItems(publicShop.getItems());
		}
		if(id == -1)
			PlayerOwnedShops.save(publicShop.username);
	}

	public int getPrice(int slot) {
		return this.prices[slot];
	}

	/**
	 * Checks a value of an item in a shop
	 * 
	 * @param player
	 *            The player who's checking the item's value
	 * @param slot
	 *            The shop item's slot (in the shop!)
	 * @param sellingItem
	 *            Is the player selling the item?
	 */
	public void checkValue(Player player, int slot, boolean sellingItem) {
		this.setPlayer(player);
		Item shopItem = new Item(getItems()[slot].getId());
		if (!player.isShopping()) {
			player.getPacketSender().sendInterfaceRemoval();
			return;
		}
		Item item = sellingItem ? player.getInventory().getItems()[slot] : getItems()[slot];
		if (item.getId() == 995)
			return;
		if (sellingItem) {
			if (!shopBuysItem(player, id, username, item)) {
				player.getPacketSender().sendMessage("You cannot sell this item to this store.");
				return;
			}
		}
		int finalValue = 0;
		String finalString = sellingItem ? "" + ItemDefinition.forId(item.getId()).getName() + ": shop will buy for "
				: "" + ItemDefinition.forId(shopItem.getId()).getName() + " currently costs ";
		if (getCurrency().getId() != -1) {
			if(!sellingItem && this instanceof PlayerShop) {
				PlayerShop ps = (PlayerShop) this;
				finalValue = ps.getPrice(item);
			} else
				finalValue = ItemDefinition.forId(item.getId()).getValue();
			String s = currency.getDefinition().getName().toLowerCase().endsWith("s")
					? currency.getDefinition().getName().toLowerCase()
					: currency.getDefinition().getName().toLowerCase() + "s";
			/** CUSTOM CURRENCY, CUSTOM SHOP VALUES **/
			if (id == TOKKUL_EXCHANGE_STORE || id == ENERGY_FRAGMENT_STORE || id == STARDUST_STORE|| id == AGILITY_TICKET_STORE
					|| id == GRAVEYARD_STORE || id == DONATOR_STORE_1) {
				finalValue = this.prices[slot];
			}
			if (sellingItem) {
				if (finalValue != 1) {
					finalValue = (int) (finalValue * 0.85);
				}
			}
			finalString += "" + finalValue + " " + s + "" + shopPriceEx(finalValue) + ".";
		} else if(id != -1) {
			finalValue = this.prices[slot];
			if (sellingItem) {
				if (finalValue != 1) {
					finalValue = (int) (finalValue * 0.85);
				}
			}
			finalString += "" + finalValue +" .";
		}
		if (player != null && finalValue > 0 && (id != -1 || (id == -1 && !sellingItem))) {
			player.getPacketSender().sendMessage(finalString);
			return;
		}
	}

	public void sellItem(Player player, int slot, int amountToSell) {
		sellItem(player, slot, amountToSell, false);
	}

	public void sellItem(Player player, int slot, int amountToSell, boolean bypass) {
		this.setPlayer(player);
		if (!player.isShopping() || player.isBanking()) {
			player.getPacketSender().sendInterfaceRemoval();
			return;
		}
		if (!player.isShopping() || player.isBanking()) {
			player.getPacketSender().sendInterfaceRemoval();
			return;
		}
		Item itemToSell = player.getInventory().getItems()[slot];
		if (!itemToSell.sellable()) {
			player.getPacketSender().sendMessage("This item cannot be sold.");
			return;
		}
			
		if (!shopBuysItem(player, id, username, itemToSell)) {
			player.getPacketSender().sendMessage("You cannot sell this item to this store.");
			return;
		}
		if (!player.getInventory().contains(itemToSell.getId()) || itemToSell.getId() == 995)
			return;
		if (this.full(itemToSell.getId()))
			return;
		if (player.getInventory().getAmount(itemToSell.getId()) < amountToSell)
			amountToSell = player.getInventory().getAmount(itemToSell.getId());
		if (amountToSell == 0)
			return;
		/*
		 * if(amountToSell > 300) { String s =
		 * ItemDefinition.forId(itemToSell.getId()).getName().endsWith("s") ?
		 * ItemDefinition.forId(itemToSell.getId()).getName() :
		 * ItemDefinition.forId(itemToSell.getId()).getName() + "s";
		 * player.getPacketSender().sendMessage("You can only sell 300 "+s+
		 * " at a time."); return; }
		 */
		int itemId = itemToSell.getId();
		boolean customShop = this.getCurrency().getId() == -1;
		boolean inventorySpace = customShop ? true : false;
		if (!customShop) {
			if (!itemToSell.getDefinition().isStackable()) {
				if (!player.getInventory().contains(this.getCurrency().getId()))
					inventorySpace = true;
			}
			if (player.getInventory().getFreeSlots() <= 0
					&& player.getInventory().getAmount(this.getCurrency().getId()) > 0)
				inventorySpace = true;
			if (player.getInventory().getFreeSlots() > 0
					|| player.getInventory().getAmount(this.getCurrency().getId()) > 0)
				inventorySpace = true;
			if(id == -1 && (this.contains(itemId) || (ItemDefinition.forId(itemId).isNoted() && this.contains(itemId - 1))))
				bypass = true;
		}
		int itemValue = 0;
		if (getCurrency().getId() > 0) {
			itemValue = ItemDefinition.forId(itemToSell.getId()).getValue();
		} else if(id != -1) {
			itemValue = this.prices[slot];
		}
		if (id != -1 && itemValue <= 0)
			return;
		itemValue = (int) (itemValue * 0.85);
		if (itemValue <= 0) {
			itemValue = 1;
		}
		for (int i = amountToSell; i > 0; i--) {
			itemToSell = new Item(itemId);
			if (this.full(itemToSell.getId()) || !player.getInventory().contains(itemToSell.getId())
					|| !player.isShopping() || (id == -1 && !bypass))
				break;
			if (!itemToSell.getDefinition().isStackable()) {
				if (inventorySpace) {
					if(player.getShop().getName().toLowerCase().contains(player.getUsername().toLowerCase())){
						super.switchItem(player.getInventory(), this, itemToSell.getId(), -1);
					} else {
						super.switchItem(player.getInventory(), null, itemToSell.getId(), -1);
					}
					if(bypass) {
						amountToSell--;
						continue;
					}
					if (!customShop) {
						player.getInventory().add(new Item(getCurrency().getId(), itemValue), false);
					} else {
						// Return points here
					}
				} else {
					player.getPacketSender().sendMessage("Please free some inventory space before doing that.");
					break;
				}
			} else {
				if (inventorySpace) {
					if(player.getShop().getName().toLowerCase().contains(player.getUsername().toLowerCase())){
						super.switchItem(player.getInventory(), this, itemToSell.getId(), amountToSell);
					} else {
						super.switchItem(player.getInventory(), null, itemToSell.getId(), amountToSell);
					}
					if(bypass) {
						amountToSell--;
						break;
					}
					if (!customShop) {
						player.getInventory().add(new Item(getCurrency().getId(), itemValue * amountToSell), false);
					} else {
						// Return points here
					}
					break;
				} else {
					player.getPacketSender().sendMessage("Please free some inventory space before doing that.");
					break;
				}
			}
			amountToSell--;
		}
		//int noted = id + 1;
		if (customShop)
			/**if(Item.noted) {
				player.getPacketSender().sendMessage("You cannot sell noted items to your POS.");
				return;
			}**/
			player.getPointsHandler().refreshPanel();
		player.getInventory().refreshItems();
		if(id != -1)
			fireRestockTask();
		refreshItems();
		publicRefresh();
		if(id == -1 && !bypass) {
			if(player.isBanking())
				return;
			if(player.isShopping()) {
				final int ats = amountToSell;
				TaskManager.submit(new Task(1, player, false) {
					@Override
					protected void execute() {
						
						player.setInputHandling(new EnterAmountToSellItemFor(itemId, slot, ats));
						player.getPacketSender().sendEnterAmountPrompt("How much would you like to sell it for?");
						player.getShop().setPlayer(player);
						stop();
					}
				});
			}
		}
	}

	/**
	 * Buying an item from a shop
	 */
	@Override
	public Shop switchItem(ItemContainer to, Item item, int slot, boolean sort, boolean refresh) {
		final Player player = getPlayer();
		PointsHandler p = new PointsHandler(player);
		if (player == null)
			return this;
		if (!player.isShopping() || player.isBanking()) {
			player.getPacketSender().sendInterfaceRemoval();
			return this;
		}
		if (this.id == GENERAL_STORE) {
			if (player.getGameMode() == GameMode.IRONMAN) {
				player.getPacketSender()
						.sendMessage("Ironman-players are not allowed to buy items from the general-store.");
				return this;
			}
			if (player.getGameMode() == GameMode.HARDCORE_IRONMAN) {
				player.getPacketSender()
						.sendMessage("Hardcore-ironman-players are not allowed to buy items from the general-store.");
				return this;
			}
		}
		if (!shopSellsItem(item))
			return this;
		if (getItems()[slot].getAmount() <= 1 && id != GENERAL_STORE && id != -1) {
			player.getPacketSender()
					.sendMessage("The shop can't be 1 items and needs to regenerate some items first..");
		}   

		if (item.getAmount() > getItems()[slot].getAmount())
			item.setAmount(getItems()[slot].getAmount());
		int amountBuying = item.getAmount();
		if (id == 21) { //farming cheapfix
			if (getItems()[slot].getAmount() - amountBuying <= 1) {
				amountBuying = getItems()[slot].getAmount() - 1;
				while(getItems()[slot].getAmount() - amountBuying <= 1) {
					if (getItems()[slot].getAmount() - amountBuying == 1) break;
					amountBuying--;
				}
			}
		}
		if (getItems()[slot].getAmount() < amountBuying) {
			amountBuying = getItems()[slot].getAmount() - 101;
		}
		if (amountBuying == 0)
			return this;

		if (amountBuying > 5000) {
			player.getPacketSender().sendMessage(
					"You can only buy 5000 " + ItemDefinition.forId(item.getId()).getName() + "s at a time.");
			return this;
		}
		boolean customShop = getCurrency().getId() == -1;
		boolean usePouch = false;
		int playerCurrencyAmount = 0;
		int value = ItemDefinition.forId(item.getId()).getValue();
		boolean isOwner = false;

		if(id == -1 && this instanceof PlayerShop) {
			PlayerShop ps = (PlayerShop) this;
			value = ps.getPrice(item);
			if(player.getUsername().equals(username))
				isOwner = true;
			if(isOwner)
				value = 0;
		}
		String currencyName = "";
		if (getCurrency().getId() != -1) {
			playerCurrencyAmount = player.getInventory().getAmount(currency.getId());
			currencyName = ItemDefinition.forId(currency.getId()).getName().toLowerCase();
			if (currency.getId() == 995) {
				if (player.getMoneyInPouch() >= value) {
					playerCurrencyAmount = player.getMoneyInPouchAsInt();
					if (!(player.getInventory().getFreeSlots() == 0
							&& player.getInventory().getAmount(currency.getId()) == value)) {
						usePouch = true;
					}
				}
			} else {
				/** CUSTOM CURRENCY, CUSTOM SHOP VALUES **/
				if (id == TOKKUL_EXCHANGE_STORE || id == ENERGY_FRAGMENT_STORE || id == STARDUST_STORE || id == AGILITY_TICKET_STORE
						|| id == GRAVEYARD_STORE) {
					value = this.prices[slot];
				}
			}
		} else {
			value = this.prices[slot];
			if (id == PKING_REWARDS_STORE) {
				playerCurrencyAmount = player.getPointsHandler().getPkPoints();
			} else if (id == VOTING_REWARDS_STORE) {
				playerCurrencyAmount = player.getPointsHandler().getVotingPoints();
			} else if (id == 169) {
				playerCurrencyAmount = player.getPointsHandler().getDonationPoints();
			} else if (id == DUNGEONEERING_STORE) {
				playerCurrencyAmount = player.getPointsHandler().getDungeoneeringTokens();
			} else if (id == DONATOR_STORE_1) {
				playerCurrencyAmount = player.getPointsHandler().getDonationPoints();
			} else if (id == TRIVIA_STORE) {
				playerCurrencyAmount = player.getPointsHandler().getTriviaPoints();
			} else if (id == BOSS_POINT_STORE) {
				playerCurrencyAmount = player.getBossPoints();
			} else if (id == DONATOR_STORE_2) {
				playerCurrencyAmount = player.getPointsHandler().getDonationPoints();
			} else if (id == PRESTIGE_STORE) {
				playerCurrencyAmount = player.getPointsHandler().getPrestigePoints();
			} else if (id == SLAYER_STORE) {
				playerCurrencyAmount = player.getPointsHandler().getSlayerPoints();
			}
		}
		if (value <= 0 && id != -1) {
			return this;
		}
		if (!hasInventorySpace(player, item, getCurrency().getId(), value)) {
			player.getPacketSender().sendMessage("You do not have any free inventory slots.");
			return this;
		}
		if (!isOwner && (playerCurrencyAmount <= 0 || playerCurrencyAmount < value)) {
			player.getPacketSender()
					.sendMessage("You do not have enough "
							+ ((currencyName.endsWith("s") ? (currencyName) : (currencyName + "s")))
							+ " to purchase this item.");
			return this;
		}
		if (id == SKILLCAPE_STORE_1 || id == SKILLCAPE_STORE_2 || id == SKILLCAPE_STORE_3) {
			for (int i = 0; i < item.getDefinition().getRequirement().length; i++) {
				int req = item.getDefinition().getRequirement()[i];
				if ((i == 3 || i == 5) && req == 99)
					req *= 10;
				if (req > player.getSkillManager().getMaxLevel(i)) {
					player.getPacketSender().sendMessage("You need to have at least level 99 in "
							+ Misc.formatText(Skill.forId(i).toString().toLowerCase()) + " to buy this item.");
					return this;
				}
			}
		}

		for (int i = amountBuying; i > 0; i--) {
			if (!shopSellsItem(item)) {
				break;
			}
			if (getItems()[slot].getAmount() < amountBuying) {
				amountBuying = getItems()[slot].getAmount() - 101;
			}

			if (getItems()[slot].getAmount() <= 1 && id != GENERAL_STORE && id != -1) {
				player.getPacketSender()
						.sendMessage("The shop can't be below 1 items and needs to regenerate some items first...");
				break;
			}
			if(id == -1 && isOwner) {
				super.switchItem(to, new Item(item.getId(), amountBuying), slot, false, false);
				break;
			}
			if (!item.getDefinition().isStackable()) {
				if (playerCurrencyAmount >= value && hasInventorySpace(player, item, getCurrency().getId(), value)) {

					if (!customShop) {
						if (usePouch) {
							player.setMoneyInPouch((player.getMoneyInPouch() - value));
						} else {
							player.getInventory().delete(currency.getId(), value, false);
						}
						if(id == -1 && this instanceof PlayerShop) {
							PlayerShop ps = (PlayerShop) this;
							ps.addSale(item.getId(), 1, value);
						}
					} else {
						if (id == PKING_REWARDS_STORE) {
							player.getPointsHandler().setPkPoints(-value, true);
							PlayerPanel.refreshPanel(player);
						} else if (id == VOTING_REWARDS_STORE) {
							player.getPointsHandler().setVotingPoints(-value, true);
							PlayerPanel.refreshPanel(player); 
						} else if (id == 169) {
							player.getPointsHandler().setDonationPoints(-value, true);
							PlayerPanel.refreshPanel(player);
						} else if (id == DUNGEONEERING_STORE) {
							player.getPointsHandler().setDungeoneeringTokens(-value, true);
							PlayerPanel.refreshPanel(player);
						} else if (id == DONATOR_STORE_1) {
							player.getPointsHandler().setDonationPoints(-value, true);
							PlayerPanel.refreshPanel(player);
						} else if (id == BOSS_POINT_STORE) {
							player.setBossPoints(player.getBossPoints() - value);
						} else if (id == TRIVIA_STORE) {
							player.getPointsHandler().setTriviaPoints(-value, true);
							PlayerPanel.refreshPanel(player);
						} else if (id == DONATOR_STORE_2) {
							player.getPointsHandler().setDonationPoints(-value, true);
							PlayerPanel.refreshPanel(player);
						} else if (id == PRESTIGE_STORE) {
							player.getPointsHandler().setPrestigePoints(-value, true);
							PlayerPanel.refreshPanel(player);
						} else if (id == SLAYER_STORE) {
							player.getPointsHandler().setSlayerPoints(-value, true);
							PlayerPanel.refreshPanel(player);
						}
					}

					super.switchItem(to, new Item(item.getId(), 1), slot, false, false);

					playerCurrencyAmount -= value;
				} else {
					break;
				}
			} else {
				if (playerCurrencyAmount >= value && hasInventorySpace(player, item, getCurrency().getId(), value)) {

					int canBeBought = playerCurrencyAmount / (value);
					if (canBeBought >= amountBuying) {
						canBeBought = amountBuying;
					}
					if (canBeBought == 0)
						break;

					if (!customShop) {
						if (usePouch) {
							player.setMoneyInPouch((player.getMoneyInPouch() - (value * canBeBought)));
						} else {
							player.getInventory().delete(currency.getId(), value * canBeBought, false);
						}
						if(id == -1 && this instanceof PlayerShop) {
							PlayerShop ps = (PlayerShop) this;
							ps.addSale(item.getId(), canBeBought, value);
						}
					} else {
						if (id == PKING_REWARDS_STORE) {
							player.getPointsHandler().setPkPoints(-value * canBeBought, true);
							PlayerPanel.refreshPanel(player);
						} else if (id == VOTING_REWARDS_STORE) {
							player.getPointsHandler().setVotingPoints(-value * canBeBought, true);
							PlayerPanel.refreshPanel(player);
						} else if (id == 169) {
							player.getPointsHandler().setDonationPoints(-value * canBeBought, true);
							PlayerPanel.refreshPanel(player);
						} else if (id == DUNGEONEERING_STORE) {
							player.getPointsHandler().setDungeoneeringTokens(-value * canBeBought, true);
							PlayerPanel.refreshPanel(player);
						} else if (id == DONATOR_STORE_1) {
							player.getPointsHandler().setDonationPoints(-value * canBeBought, true);
							PlayerPanel.refreshPanel(player);
						} else if (id == TRIVIA_STORE) {
							player.getPointsHandler().setTriviaPoints(-value * canBeBought, true);
							PlayerPanel.refreshPanel(player);
						} else if (id == BOSS_POINT_STORE) {
							player.setBossPoints(player.getBossPoints() - (value * canBeBought));
							PlayerPanel.refreshPanel(player);
						} else if (id == DONATOR_STORE_2) {
							player.getPointsHandler().setDonationPoints(-value * canBeBought, true);
							PlayerPanel.refreshPanel(player);
						} else if (id == PRESTIGE_STORE) {
							player.getPointsHandler().setPrestigePoints(-value * canBeBought, true);
							PlayerPanel.refreshPanel(player);
						} else if (id == SLAYER_STORE) {
							player.getPointsHandler().setSlayerPoints(-value * canBeBought, true);
							PlayerPanel.refreshPanel(player);
						}
					}
					super.switchItem(to, new Item(item.getId(), canBeBought), slot, false, false);
					playerCurrencyAmount -= value;
					break;
				} else {
					break;
				}
			}
			amountBuying--;
		}
		if (!customShop) {
			if (usePouch) {
				player.getPacketSender().sendString(8135, "" + player.getMoneyInPouch()); // Update
																							// the
																							// money
																							// pouch
			}
		} else {
			player.getPointsHandler().refreshPanel();
		}
		player.getInventory().refreshItems();
		if(id != -1)
			fireRestockTask();
		refreshItems();
		publicRefresh();
		return this;
	}

	/**
	 * Checks if a player has enough inventory space to buy an item
	 * 
	 * @param item
	 *            The item which the player is buying
	 * @return true or false if the player has enough space to buy the item
	 */
	public static boolean hasInventorySpace(Player player, Item item, int currency, int pricePerItem) {
		if (player.getInventory().getFreeSlots() >= 1) {
			return true;
		}
		if (item.getDefinition().isStackable()) {
			if (player.getInventory().contains(item.getId())) {
				return true;
			}
		}
		if (currency != -1) {
			if (player.getInventory().getFreeSlots() == 0
					&& player.getInventory().getAmount(currency) == pricePerItem) {
				return true;
			}
		}
		return false;
	}

	@Override
	public Shop add(Item item, boolean refresh) {
		super.add(item, false);
		if (id != RECIPE_FOR_DISASTER_STORE)
			publicRefresh();
		return this;
	}

	@Override
	public int capacity() {
		return 42;
	}

	@Override
	public StackType stackType() {
		return StackType.STACKS;
	}

	@Override
	public Shop refreshItems() {
		if (id == RECIPE_FOR_DISASTER_STORE) {
			RecipeForDisaster.openRFDShop(getPlayer());
			return this;
		}
		for (Player player : World.getPlayers()) {
			if (player == null || !player.isShopping() || player.getShop() == null || player.getShop().id != id || (username != null && player.getShop().username != null && !player.getShop().username.equals(username)))
				continue;
			player.getPacketSender().sendItemContainer(player.getInventory(), INVENTORY_INTERFACE_ID);
			player.getPacketSender().sendItemContainer(id != -1 ? ShopManager.getShops().get(id) : PlayerOwnedShops.getShops().get(username), ITEM_CHILD_ID);
			player.getPacketSender().sendString(NAME_INTERFACE_CHILD_ID, name);
			if (player.getInputHandling() == null || !(player.getInputHandling() instanceof EnterAmountToSellToShop || player.getInputHandling() instanceof EnterAmountToBuyFromShop || player.getInputHandling() instanceof EnterAmountToSellItemFor))
				player.getPacketSender().sendInterfaceSet(id != -1 ? INTERFACE_ID : PlayerOwnedShops.INTERFACE_ID + 561, INVENTORY_INTERFACE_ID - 1);
		}
		return this;
	}

	@Override
	public Shop full() {
		getPlayer().getPacketSender().sendMessage("The shop is currently full. Please come back later.");
		return this;
	}

	public String shopPriceEx(int shopPrice) {
		String ShopAdd = "";
		if (shopPrice >= 1000 && shopPrice < 1000000) {
			ShopAdd = " (" + (shopPrice / 1000) + "K)";
		} else if (shopPrice >= 1000000) {
			ShopAdd = " (" + (shopPrice / 1000000) + " million)";
		}
		return ShopAdd;
	}

	private boolean shopSellsItem(Item item) {
		return contains(item.getId());
	}

	public void fireRestockTask() {
		if (isRestockingItems() || fullyRestocked())
			return;
		setRestockingItems(true);
		TaskManager.submit(new ShopRestockTask(this));
	}

	public void restockShop() {
		for (int shopItemIndex = 0; shopItemIndex < getOriginalStock().length; shopItemIndex++) {
			int currentStockAmount = getItems()[shopItemIndex].getAmount();
			add(getItems()[shopItemIndex].getId(), getOriginalStock()[shopItemIndex].getAmount() - currentStockAmount);
//			publicRefresh();
			refreshItems();
		}

	}

	public int getAmount(Item item) {
		for (Item stockItem : getItems()) {
			if (item.getId() == stockItem.getId())
				return stockItem.getAmount();
		}
		return 0;
	}

	public int getOriginalAmount(Item item) {
		for (Item stockItem : getOriginalStock()) {
			if (item.getId() == stockItem.getId())
				return stockItem.getAmount();
		}
		return 0;
	}

	public boolean fullyRestocked() {
		if (id == GENERAL_STORE) {
			return getValidItems().size() == 0;
		} else if (id == RECIPE_FOR_DISASTER_STORE) {
			return true;
		}
		if (getOriginalStock() != null) {
			for (int shopItemIndex = 0; shopItemIndex < getOriginalStock().length; shopItemIndex++) {
				if (getItems()[shopItemIndex].getAmount() != getOriginalStock()[shopItemIndex].getAmount())
					return false;
			}
		}
		return true;
	}

	public static boolean shopBuysItem(Player player, int shopId, String username, Item item) {
		if(shopId == -1 && item.getDefinition().isNoted())
			return false;
		if (shopId == -1)
			return PlayerShop.shopBuysItem(player, username);
		if (shopId == GENERAL_STORE)
			return true;
		if (shopId == DUNGEONEERING_STORE || shopId == BOSS_POINT_STORE || shopId == TRIVIA_STORE
				|| shopId == DONATOR_STORE_1 || shopId == DONATOR_STORE_2 || shopId == PKING_REWARDS_STORE
				|| shopId == VOTING_REWARDS_STORE ||  shopId == 169 || shopId == RECIPE_FOR_DISASTER_STORE
				|| shopId == ENERGY_FRAGMENT_STORE || shopId == AGILITY_TICKET_STORE || shopId == GRAVEYARD_STORE
				|| shopId == TOKKUL_EXCHANGE_STORE || shopId == PRESTIGE_STORE || shopId == STARDUST_STORE || shopId == SLAYER_STORE)
			return false;
		Shop shop = ShopManager.getShops().get(shopId);
		if (shop != null && shop.getOriginalStock() != null) {
			for (Item it : shop.getOriginalStock()) {
				if (it != null && it.getId() == item.getId())
					return true;
			}
		}
		return false;
	}

	public static class ShopManager {

		private static final String SHOP_FILE_DIR = "./data/def/json/shops/";

		private static Map<Integer, Shop> shops = new HashMap<Integer, Shop>();

		public static Map<Integer, Shop> getShops() {
			return shops;
		}

		public static void init() {
			File[] shopFiles = new File(SHOP_FILE_DIR).listFiles();
			for (File file : shopFiles) {
				try {
					InputStream is = new FileInputStream(file);
					JSONObject fileObj = new JSONObject(IOUtils.toString(is, "UTF-8"));
					is.close();
					int shopId = fileObj.getInt("id");
					String shopName = fileObj.getString("name");
					int currency = fileObj.getInt("currency");
					JSONArray itemsJson = fileObj.getJSONArray("items");
					Item[] items = new Item[itemsJson.length()];
					int[] specialPrices = new int[itemsJson.length()];

					for (int i = 0; i < itemsJson.length(); i++) {
						JSONObject item = itemsJson.getJSONObject(i);
						items[i] = (new Item(item.getInt("id"), item.getInt("amount")));
						if (item.has("price")) {
							specialPrices[i] = item.getInt("price");
						}
					}
					shops.put(shopId, new Shop(null, shopId, shopName, new Item(currency), items, specialPrices));

				} catch (IOException e) {
					System.out.println("Error loading " + file.getName());
				} catch (JSONException e) {
					System.out.println("JSON error in " + file.getName() + ": " + e.getMessage());
				}
			}
			System.out.println(shops.keySet().size() + " Shop(s) loaded.");
		}
	}

	/**
	 * The shop interface id.
	 */
	public static final int INTERFACE_ID = 3824;

	/**
	 * The starting interface child id of items.
	 */
	public static final int ITEM_CHILD_ID = 3900;

	/**
	 * The interface child id of the shop's name.
	 */
	public static final int NAME_INTERFACE_CHILD_ID = 3901;

	/**
	 * The inventory interface id, used to set the items right click values to
	 * 'sell'.
	 */
	public static final int INVENTORY_INTERFACE_ID = 3823;

	/*
	 * Declared shops
	 */

	public static final int DONATOR_STORE_1 = 48;
	public static final int DONATOR_STORE_2 = 49;

	public static final int TRIVIA_STORE = 50;

	public static final int GENERAL_STORE = 12;
	public static final int RECIPE_FOR_DISASTER_STORE = 36;

	private static final int VOTING_REWARDS_STORE = 27;
	private static final int PKING_REWARDS_STORE = 26;
	private static final int ENERGY_FRAGMENT_STORE = 33;
	private static final int AGILITY_TICKET_STORE = 39;
	private static final int GRAVEYARD_STORE = 42;
	private static final int TOKKUL_EXCHANGE_STORE = 43;
	private static final int SKILLCAPE_STORE_1 = 8;
	private static final int SKILLCAPE_STORE_2 = 9;
	private static final int SKILLCAPE_STORE_3 = 10;
	private static final int GAMBLING_STORE = 41;
	private static final int DUNGEONEERING_STORE = 44;
	private static final int PRESTIGE_STORE = 46;
	public static final int BOSS_POINT_STORE = 92;
	private static final int SLAYER_STORE = 47;
	public static final int STARDUST_STORE = 55;
}
