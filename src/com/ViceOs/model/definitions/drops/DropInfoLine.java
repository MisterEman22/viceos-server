package com.ViceOs.model.definitions.drops;

/**
 * Author: Nick (MisterEman22)
 * Created: 6/5/2017
 */
public class DropInfoLine {
    private int chanceDenom;
    private String itemInfo;

    public DropInfoLine(int chanceDenom, String itemInfo) {
        this.chanceDenom = chanceDenom;
        this.itemInfo = itemInfo;
    }

    public String getInfoString() {
        return this.itemInfo + "  1/" + this.chanceDenom;
    }

    public int getChanceDenom() {
        return this.chanceDenom;
    }
}
