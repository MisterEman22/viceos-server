package com.ViceOs.model.definitions.drops;

import com.ViceOs.model.Item;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * Author: Nick (MisterEman22)
 * Created: 6/1/2017
 */
public class DropTable {
    private List<DropTableEntry> entries;

    public DropTable(DropTableEntry[] entries) {
        this(new ArrayList<>(Arrays.asList(entries)));
    }

    public DropTable(List<DropTableEntry> entries) {
        this.entries = entries;
}

    public List<Item> getDrops(double dropRateMod) {
        List<Item> drops = new ArrayList<>();

        for (DropTableEntry entry : this.entries) {
            if (entry.doesDrop(dropRateMod)) {
                drops.add(entry.getDrop());
            }
        }
        return drops;
    }

    public List<DropTableEntry> getEntries() {
        return this.entries;
    }
}
