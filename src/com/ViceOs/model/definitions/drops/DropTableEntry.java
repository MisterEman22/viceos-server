package com.ViceOs.model.definitions.drops;

import com.ViceOs.model.Item;

import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Author: Nick (MisterEman22)
 * Created: 6/1/2017
 */
public abstract class DropTableEntry {

    // Represents the chance of getting a drop (acts as a weight in an exclusive drops entry)
    private double dropChance;

    public DropTableEntry(double dropChance) {
        this.dropChance = dropChance;
    }

    public boolean doesDrop(double dropRateMod) {
        double actingDropChance = this.dropChance * dropRateMod;
        double randomVal = Math.random();
        return randomVal < actingDropChance;
    }

    public abstract Item getDrop();

    public abstract List<DropInfoLine> getInfoLines(double chance);

    public double getDropChance() {
        return this.dropChance;
    }

}
