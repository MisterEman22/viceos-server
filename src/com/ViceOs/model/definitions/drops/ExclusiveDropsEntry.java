package com.ViceOs.model.definitions.drops;

import com.ViceOs.model.Item;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: Nick (MisterEman22)
 * Created: 6/2/2017
 *
 * Entry for a set of items from which only a single one can drop per kill
 */
public class ExclusiveDropsEntry extends DropTableEntry {
    private List<DropTableEntry> entries;

    public ExclusiveDropsEntry(double dropChance) {
        super(dropChance);
        this.entries = new ArrayList<>();
    }

    public void addEntry(DropTableEntry entry) {
        this.entries.add(entry);
    }


    @Override
    public Item getDrop() {
        double completeWeight = 0.0;
        for (DropTableEntry e : entries)
            completeWeight += e.getDropChance();
        double r = Math.random() * completeWeight;
        double countWeight = 0.0;
        for (DropTableEntry e : entries) {
            countWeight += e.getDropChance();
            if (countWeight >= r)
                return e.getDrop();
        }
        throw new RuntimeException("No drop was received from exclusive drop table");
    }

    @Override
    public List<DropInfoLine> getInfoLines(double chance) {
        List<DropInfoLine> lines = new ArrayList<>();
        double entryChance = getDropChance() * chance / getCompleteWeight();
        for (DropTableEntry entry : this.entries) {
            lines.addAll(entry.getInfoLines(entryChance));
        }
        return lines;
    }

    public double getCompleteWeight() {
        double weight = 0;
        for (DropTableEntry e : this.entries) {
            weight += e.getDropChance();
        }
        return weight;
    }

}
