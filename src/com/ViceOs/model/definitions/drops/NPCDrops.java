package com.ViceOs.model.definitions.drops;

import com.ViceOs.model.*;
import com.ViceOs.model.Locations.Location;
import com.ViceOs.model.container.impl.Bank;
import com.ViceOs.model.container.impl.Equipment;
import com.ViceOs.model.definitions.ItemDefinition;
import com.ViceOs.util.JsonLoader;
import com.ViceOs.util.Misc;
import com.ViceOs.util.RandomUtility;
import com.ViceOs.world.World;
import com.ViceOs.world.content.DropLog;
import com.ViceOs.world.content.PlayerLogs;
import com.ViceOs.world.content.WellOfWealth;
import com.ViceOs.world.content.DropLog.DropLogEntry;
import com.ViceOs.world.content.clan.ClanChatManager;
import com.ViceOs.world.content.minigames.impl.WarriorsGuild;
import com.ViceOs.world.content.skill.impl.prayer.BonesData;
import com.ViceOs.world.content.skill.impl.summoning.CharmingImp;
import com.ViceOs.world.entity.impl.GroundItemManager;
import com.ViceOs.world.entity.impl.npc.NPC;
import com.ViceOs.world.entity.impl.player.Player;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.SystemUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Controls the npc drops
 * 
 * @author 2012 <http://www.rune-server.org/members/dexter+morgan/>, Gabbe &
 *         Samy
 * 
 */
public class NPCDrops {

	private static final String DROP_FILE_DIR = "./data/def/json/drops/";

	/**
	 * The map containing all the npc drops.
	 */
	private static Map<Integer, DropTable> dropTables = new HashMap<Integer, DropTable>();

	public static void init() {
		File[] dropFiles = new File(DROP_FILE_DIR).listFiles();
		for (File file : dropFiles) {
			try {
				InputStream is = new FileInputStream(file);
				JSONObject fileObj = new JSONObject(IOUtils.toString(is, "UTF-8"));
				is.close();
				JSONArray npcIds = fileObj.getJSONArray("npcIds");
				JSONArray dropsArray = fileObj.getJSONArray("drops");
				List<DropTableEntry> entries = new ArrayList<>();
				for (int i = 0; i < dropsArray.length(); i++) {
					JSONObject drop = dropsArray.getJSONObject(i);
					entries.add(entryFromJson(drop));
				}
				for (int i = 0; i < npcIds.length(); i++) {
					dropTables.put(npcIds.getInt(i), new DropTable(entries));
				}
			} catch (IOException e) {
				System.out.println("Error loading " + file.getName());
			} catch (JSONException e) {
				System.out.println("JSON error in " + file.getName() + ": " + e.getMessage());
			}
		}
		System.out.println(dropTables.keySet().size() + " Drop Table(s) loaded.");
	}

	private static DropTableEntry entryFromJson(JSONObject jsonObject) {
		double chance = jsonObject.getDouble("chance");
		if (jsonObject.has("drops")) {
			ExclusiveDropsEntry entry = new ExclusiveDropsEntry(chance);
			JSONArray drops = jsonObject.getJSONArray("drops");
			for (int i = 0; i < drops.length(); i++) {
				JSONObject drop = drops.getJSONObject(i);
				entry.addEntry(entryFromJson(drop));
			}
			return entry;
		} else {
			int itemId, min, max;
			itemId = jsonObject.getInt("id");
			if (jsonObject.has("min")) {
				min = jsonObject.getInt("min");
			} else {
				min = 1;
			}
			if (jsonObject.has("max")) {
				max = jsonObject.getInt("max");
			} else {
				max = min;
			}
			return new SingleDropEntry(itemId, min	, max, chance);
		}
	}

	public static List<Item> getDrops(int npcId, double dropRateMod) {
		if (dropTables.containsKey(npcId)) {
			return dropTables.get(npcId).getDrops(dropRateMod);
		} else {
			return new ArrayList<>();
		}
	}

	public static DropTable getDropTable(int npcId) {
		if (dropTables.containsKey(npcId)) {
			return dropTables.get(npcId);
		} else {
			return null;
		}
	}
	/**
	 * Drops items for a player after killing an npc.
	 * 
	 * @param p
	 *            Player to receive drop.
	 * @param npc
	 *            NPC to receive drop FROM.
	 */
	public static void dropItems(Player p, NPC npc) {
		if (npc.getLocation() == Location.WARRIORS_GUILD)
			WarriorsGuild.handleDrop(p, npc);
		List<Item> drops = getDrops(npc.getId(), p.getDropRateMod());
		final boolean goGlobal = p.getPosition().getZ() >= 0 && p.getPosition().getZ() < 4;
		final Position npcPos = npc.getPosition().copy();

		for (Item i : drops) {
			drop(p, i, npc, goGlobal);
		}
	}


	public static void drop(Player player, Item item, NPC npc, boolean goGlobal) {

		int itemId = item.getId();
		int amount = item.getAmount();
		
		if (itemId == 6731 ||  itemId == 6914 || itemId == 7158 ||  itemId == 6889 || itemId == 6733 || itemId == 15019 || itemId == 11235 || itemId == 15020 || itemId == 15018 || itemId == 15220 || itemId == 6735 || itemId == 6737 || itemId == 6585 || itemId == 4151 || itemId == 4087 || itemId == 2577 || itemId == 2581 || itemId == 11732 || itemId == 18782 ) {
			player.getPacketSender().sendMessage("@red@ YOU HAVE RECIEVED A MEDIUM DROP, CHECK THE GROUND!");

		}

		if (itemId == CharmingImp.GOLD_CHARM
				|| itemId == CharmingImp.GREEN_CHARM
				|| itemId == CharmingImp.CRIM_CHARM
				|| itemId == CharmingImp.BLUE_CHARM) {
			if (player.getInventory().contains(6500)
					&& CharmingImp.handleCharmDrop(player, itemId, amount)) {
				return;
			}
		}

		Player toGive = player;

		boolean ccAnnounce = false;
		if(Location.inMulti(player)) {
			if(player.getCurrentClanChat() != null && player.getCurrentClanChat().getLootShare()) {
				CopyOnWriteArrayList<Player> playerList = new CopyOnWriteArrayList<Player>();
				for(Player member : player.getCurrentClanChat().getMembers()) {
					if(member != null) {
						if(member.getPosition().isWithinDistance(player.getPosition())) {
							playerList.add(member);
						}
					}
				}
				if(playerList.size() > 0) {
					toGive = playerList.get(RandomUtility.getRandom(playerList.size() - 1));
					if(toGive == null || toGive.getCurrentClanChat() == null || toGive.getCurrentClanChat() != player.getCurrentClanChat()) {
						toGive = player;
					}
					ccAnnounce = true;
				}
			}
		}
		
		if(itemId == 18778) { //Effigy, don't drop one if player already has one
			if(toGive.getInventory().contains(18778) || toGive.getInventory().contains(18779) || toGive.getInventory().contains(18780) || toGive.getInventory().contains(18781)) {
				return;
			} 
			for(Bank bank : toGive.getBanks()) {
				if(bank == null) {
					continue;
				}
				if(bank.contains(18778) || bank.contains(18779) || bank.contains(18780) || bank.contains(18781)) {
					return;
				}
			}
		}

		Position pos = npc.getPosition().copy();

		if(npc.getId() == 2042 || npc.getId() == 2043 || npc.getId() == 2044){
			pos.set(2268, 3069, toGive.getPosition().getZ());
			GroundItemManager.spawnGroundItem(toGive, new GroundItem(item, pos,
					toGive.getUsername(), false, 150, goGlobal, 200));
		} else {
			GroundItemManager.spawnGroundItem(toGive, new GroundItem(item, pos,
					toGive.getUsername(), false, 150, goGlobal, 200));
		}
		
		DropLog.submit(toGive, new DropLogEntry(itemId, item.getAmount()));
	}
}