package com.ViceOs.model.definitions.drops;

import com.ViceOs.model.Item;
import com.ViceOs.model.definitions.ItemDefinition;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Author: Nick (MisterEman22)
 * Created: 6/2/2017
 *
 * Individual item drop entry
 */
public class SingleDropEntry extends DropTableEntry {
    private int itemId;
    private int min;
    private int max;

    public SingleDropEntry(int itemId, int min, int max, double dropChance) {
        super(dropChance);
        this.itemId = itemId;
        this.min = min;
        this.max = max;
    }

    @Override
    public List<DropInfoLine> getInfoLines(double chance) {
        String itemName = ItemDefinition.forId(this.itemId).getName();
        String quantityString;
        if (min == max) {
            quantityString = " x" + min;
        } else {
            quantityString = " x" + min + "-" + max;
        }
        double totalDropChance = getDropChance() * chance;
        int chanceDenom = (int) Math.ceil(1 / totalDropChance);
        return Arrays.asList(new DropInfoLine[] {new DropInfoLine(chanceDenom, itemName + quantityString)});
    }

    @Override
    public Item getDrop() {
        int quantity = ThreadLocalRandom.current().nextInt(min, max + 1);
        return new Item(this.itemId, quantity);
    }
}
