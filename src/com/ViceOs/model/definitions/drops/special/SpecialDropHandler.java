package com.ViceOs.model.definitions.drops.special;

import com.ViceOs.model.Item;

import java.util.List;

/**
 * Author: Nick (MisterEman22)
 * Created: 6/16/2017
 */
public abstract class SpecialDropHandler {

    public abstract List<Item> getDrops(int npcId, double dropRateMod);

}
