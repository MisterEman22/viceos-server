package com.ViceOs.model.input.impl;

import com.ViceOs.model.input.EnterAmount;
import com.ViceOs.world.content.WellOfWealth;
import com.ViceOs.world.entity.impl.player.Player;

public class DonateWealth extends EnterAmount {

	@Override
	public void handleAmount(Player player, int amount) {
		WellOfWealth.donate(player, amount);
	}

}
