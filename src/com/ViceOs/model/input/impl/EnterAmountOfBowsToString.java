package com.ViceOs.model.input.impl;

import com.ViceOs.model.input.EnterAmount;
import com.ViceOs.world.content.skill.impl.fletching.Fletching;
import com.ViceOs.world.entity.impl.player.Player;

public class EnterAmountOfBowsToString extends EnterAmount {

	@Override
	public void handleAmount(Player player, int amount) {
		Fletching.stringBow(player, amount);
	}

}
