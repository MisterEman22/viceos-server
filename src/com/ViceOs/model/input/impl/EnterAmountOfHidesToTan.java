package com.ViceOs.model.input.impl;

import com.ViceOs.model.input.EnterAmount;
import com.ViceOs.world.content.skill.impl.crafting.Tanning;
import com.ViceOs.world.entity.impl.player.Player;

public class EnterAmountOfHidesToTan extends EnterAmount {

	private int button;
	public EnterAmountOfHidesToTan(int button) {
		this.button = button;
	}
	
	@Override
	public void handleAmount(Player player, int amount) {
		Tanning.tanHide(player, button, amount);
	}

}
