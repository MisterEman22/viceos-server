package com.ViceOs.model.input.impl;

import com.ViceOs.model.container.impl.PlayerShop;
import com.ViceOs.model.input.EnterAmount;
import com.ViceOs.world.content.PlayerOwnedShops;
import com.ViceOs.world.entity.impl.player.Player;

/**
 * Created by Greg on 25/04/2017.
 */
public class EnterAmountToSellItemFor extends EnterAmount {
    private int amountToSell;
    public EnterAmountToSellItemFor(int item, int slot, int amount) {
        super(item, slot);
        amountToSell = amount;
    }
//this file
    @Override
    public void handleAmount(Player player, int price) {
    	//System.out.println("PRICE: " + price);
        if(player.isShopping() && getItem() > 0 && getSlot() >= 0 && player.getShop() instanceof PlayerShop) {
            PlayerShop shop = (PlayerShop) player.getShop();
            if(shop != null) {
                if(getSlot() >= player.getInventory().getItems().length || player.getInventory().getItems()[getSlot()].getId() != getItem() || price > Integer.MAX_VALUE)
                    return;
                shop.sellItem(player, getSlot(), amountToSell, true);
                System.out.println("TRYING PRICE: " + price);
                shop.setPrice(getItem(), price);

                player.getPacketSender().sendShopInfo(0, PlayerOwnedShops.getShops().get(player.viewingUser).getItems(), null, player.viewingUser);
            } else
                player.getPacketSender().sendInterfaceRemoval();
        } else
            player.getPacketSender().sendInterfaceRemoval();

    }
}
