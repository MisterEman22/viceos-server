package com.ViceOs.model.input.impl;

import com.ViceOs.model.input.EnterAmount;
import com.ViceOs.world.content.grandexchange.GrandExchange;
import com.ViceOs.world.entity.impl.player.Player;

public class EnterGePricePerItem extends EnterAmount {

	@Override
	public void handleAmount(Player player, int amount) {
		GrandExchange.setPricePerItem(player, amount);
	}

}
