package com.ViceOs.model.input.impl;

import com.ViceOs.model.input.EnterAmount;
import com.ViceOs.world.content.PlayerOwnedShops;
import com.ViceOs.world.entity.impl.player.Player;

/**
 * Created by Greg on 25/04/2017.
 */
public class EnterNumberOfMinutesToFeature extends EnterAmount {

    @Override
    public void handleAmount(Player player, int amount) {
        PlayerOwnedShops.buyFeature(player, amount);
    }

}