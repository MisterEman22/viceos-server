package com.ViceOs.model.input.impl;

import com.ViceOs.model.input.Input;
import com.ViceOs.world.content.PlayerOwnedShops;
import com.ViceOs.world.entity.impl.player.Player;

/**
 * Created by Greg on 25/04/2017.
 */
public class EnterPlayerShopItemToSearchFor extends Input {

    @Override
    public void handleSyntax(Player player, String syntax) {
        if (syntax.length() <= 1) {
            player.getPacketSender().sendMessage("Invalid syntax entered.");
            return;
        }
        PlayerOwnedShops.searchItem(player, syntax);
    }
}
