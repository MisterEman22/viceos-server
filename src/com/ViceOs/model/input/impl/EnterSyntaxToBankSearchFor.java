package com.ViceOs.model.input.impl;

import com.ViceOs.model.container.impl.Bank.BankSearchAttributes;
import com.ViceOs.model.input.Input;
import com.ViceOs.world.entity.impl.player.Player;

public class EnterSyntaxToBankSearchFor extends Input {

	@Override
	public void handleSyntax(Player player, String syntax) {
		boolean searchingBank = player.isBanking() && player.getBankSearchingAttribtues().isSearchingBank();
		if(searchingBank)
			BankSearchAttributes.beginSearch(player, syntax);
	}
}
