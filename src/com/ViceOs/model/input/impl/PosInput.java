package com.ViceOs.model.input.impl;

import com.ViceOs.model.input.Input;
import com.ViceOs.world.entity.impl.player.Player;

public class PosInput extends Input {

	@Override
	public void handleSyntax(Player player, String syntax) {
		player.getPacketSender().sendClientRightClickRemoval();
		player.getPlayerOwnedShopManager().updateFilter(syntax);
		
	}
}
