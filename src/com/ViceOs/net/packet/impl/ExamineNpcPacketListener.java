package com.ViceOs.net.packet.impl;

import com.ViceOs.model.definitions.NpcDefinition;
import com.ViceOs.net.packet.Packet;
import com.ViceOs.net.packet.PacketListener;
import com.ViceOs.world.entity.impl.player.Player;

public class ExamineNpcPacketListener implements PacketListener {

	@Override
	public void handleMessage(Player player, Packet packet) {
		int npc = packet.readShort();
		if(npc <= 0) {
			return;
		}
		NpcDefinition npcDef = NpcDefinition.forId(npc);
		if(npcDef != null) {
			player.getPacketSender().sendMessage(npcDef.getExamine());
		}
	}

}
