package com.ViceOs.net.packet.impl;

import com.ViceOs.net.packet.Packet;
import com.ViceOs.net.packet.PacketListener;
import com.ViceOs.util.Misc;
import com.ViceOs.world.content.Flagger;
import com.ViceOs.world.content.PlayerPunishment;
import com.ViceOs.world.content.clan.ClanChatManager;
import com.ViceOs.world.content.dialogue.DialogueManager;
import com.ViceOs.world.entity.impl.player.Player;

public class SendClanChatMessagePacketListener implements PacketListener {

	@Override
	public void handleMessage(Player player, Packet packet) {
		String clanMessage = Misc.readString(packet.getBuffer());
		if(clanMessage == null || clanMessage.length() < 1)
			return;
		if(PlayerPunishment.muted(player.getUsername()) || PlayerPunishment.IPMuted(player.getHostAddress())) {
			player.getPacketSender().sendMessage("You are muted and cannot chat.");
			return;
		}
		if(Flagger.isFlagged(clanMessage)) {
			Flagger.flag(player, clanMessage);
			//DialogueManager.sendStatement(player, "A word was blocked in your sentence. Please do not repeat it!");
			//return;
		}
		ClanChatManager.sendMessage(player, clanMessage);
	}

}
