package com.ViceOs.net.packet.impl;

import com.ViceOs.net.packet.Packet;
import com.ViceOs.net.packet.PacketListener;
import com.ViceOs.world.content.MoneyPouch;
import com.ViceOs.world.entity.impl.player.Player;

public class WithdrawMoneyFromPouchPacketListener implements PacketListener {

	@Override
	public void handleMessage(Player player, Packet packet) {
		int amount = packet.readInt();
		if(player.getTrading().inTrade() || player.getDueling().inDuelScreen) {
			player.getPacketSender().sendMessage("You cannot withdraw money at the moment.");
		} else {
			MoneyPouch.withdrawMoney(player, amount);
		}
	}

}
