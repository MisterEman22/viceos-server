package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.world.World;
import com.ViceOs.world.entity.impl.player.Player;

import static com.ViceOs.model.PlayerRights.*;

/**
 * Author: Nick (MisterEman22)
 * Created: 5/27/2017
 */
public class Afk extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        World.sendMessage("<img=10> <col=FF0000><shad=0>" + player.getUsername() + ": I'm away");
    }

    @Override
    public String getCommandString() {
        return "afk";
    }

    @Override
    public String getSyntax() {
        return "::afk";
    }

    @Override
    public String getDescription() {
        return "Sets your staff status as away";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return new PlayerRights[] {SUPPORT, MODERATOR, ADMINISTRATOR, OWNER, DEVELOPER};
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length == 0;
    }
}
