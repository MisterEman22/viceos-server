package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.world.content.TriviaBot;
import com.ViceOs.world.entity.impl.player.Player;

/**
 * Author: Nick (MisterEman22)
 * Created: 5/26/2017
 */
public class Answer extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        String triviaAnswer = String.join(" ", args);
        if (TriviaBot.acceptingQuestion()) {
            TriviaBot.attemptAnswer(player, triviaAnswer);
        } else {
            // TODO: Send message that no trivia question is currently available
        }
    }

    @Override
    public String getCommandString() {
        return "answer";
    }

    @Override
    public String getDescription() {
        return "Answers a trivia question";
    }

    @Override
    public String getSyntax() {
        return "::answer [answer]";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return PlayerRights.values();
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length > 0;
    }
}
