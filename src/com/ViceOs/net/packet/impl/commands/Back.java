package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.world.World;
import com.ViceOs.world.entity.impl.player.Player;

import static com.ViceOs.model.PlayerRights.*;

/**
 * Author: Nick (MisterEman22)
 * Created: 5/28/2017
 */
public class Back extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        World.sendMessage("<img=10> <col=FF0000><shad=0>" + player.getUsername() + ": I'm back.");
    }

    @Override
    public String getCommandString() {
        return "back";
    }

    @Override
    public String getSyntax() {
        return "::back";
    }

    @Override
    public String getDescription() {
        return "Sets your staff status as active";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return new PlayerRights[] {SUPPORT, MODERATOR, ADMINISTRATOR, OWNER, DEVELOPER};
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length == 0;
    }
}
