package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.world.World;
import com.ViceOs.world.content.PlayerLogs;
import com.ViceOs.world.content.PlayerPunishment;
import com.ViceOs.world.entity.impl.player.Player;
import com.ViceOs.world.entity.impl.player.PlayerSaving;

import static com.ViceOs.model.PlayerRights.*;

/**
 * Author: Nick (MisterEman22)
 * Created: 6/5/2017
 */
public class Ban extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        String targetPlayerName = String.join(" ", args);
        if (!PlayerSaving.playerExists(targetPlayerName)) {
            player.getPacketSender().sendMessage("Player " + targetPlayerName + " does not exist.");
        } else if (PlayerPunishment.banned(targetPlayerName)) {
            player.getPacketSender().sendMessage("Player " + targetPlayerName + " already has an active ban.");
        } else {
            PlayerLogs.log(player.getUsername(), "" + player.getUsername() + " just banned " + targetPlayerName + "!");
            PlayerPunishment.ban(targetPlayerName);
            player.getPacketSender().sendMessage("Player " + targetPlayerName + " was successfully banned. Command logs written.");
            Player targetPlayer = World.getPlayerByName(targetPlayerName);
            if (targetPlayer != null) {
                World.deregister(targetPlayer);
            }
        }
    }

    @Override
    public String getCommandString() {
        return "ban";
    }

    @Override
    public String getSyntax() {
        return "::ban [username]";
    }

    @Override
    public String getDescription() {
        return "Bans a player";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return new PlayerRights[] {ADMINISTRATOR, DEVELOPER, OWNER};
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length > 0;
    }
}
