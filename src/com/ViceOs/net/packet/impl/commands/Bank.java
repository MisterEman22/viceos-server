package com.ViceOs.net.packet.impl.commands;

import static com.ViceOs.model.PlayerRights.*;

import com.ViceOs.model.Locations;
import com.ViceOs.model.PlayerRights;
import com.ViceOs.world.entity.impl.player.Player;

/**
 * Author: Nick (MisterEman22)
 * Created: 5/26/2017
 */
public class Bank extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        player.getBank(player.getCurrentBankTab()).open();
    }

    @Override
    public String getCommandString() {
        return "bank";
    }

    @Override
    public String getSyntax() {
        return "::bank";
    }

    @Override
    public String getDescription() {
        return "Opens your bank";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return new PlayerRights[] {UBER_DONATOR, SUPPORT, MODERATOR, ADMINISTRATOR, OWNER, DEVELOPER};
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length == 0;
    }
}
