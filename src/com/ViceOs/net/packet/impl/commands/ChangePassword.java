package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.util.NameUtils;
import com.ViceOs.world.entity.impl.player.Player;

/**
 * Author: Nick (MisterEman22)
 * Created: 5/26/2017
 */
public class ChangePassword extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        String syntax = String.join(" ", args);
        if (syntax == null || syntax.length() <= 2 || syntax.length() > 15 || !NameUtils.isValidName(syntax)) {
            player.getPacketSender().sendMessage("That password is invalid. Please try another password.");
            return;
        }
        if (syntax.contains("_")) {
            player.getPacketSender().sendMessage("Your password can not contain underscores.");
            return;
        }
        player.setPassword(syntax);
        player.getPacketSender().sendMessage("Your new password is: [" + syntax + "] Write it down!");
    }

    @Override
    public String getCommandString() {
        return "changepassword";
    }

    @Override
    public String getDescription() {
        return "Changes your password";
    }

    @Override
    public String getSyntax() {
        return "::changepassword [newpassword]";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return PlayerRights.values();
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length > 0;
    }
}
