package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.*;
import com.ViceOs.model.container.impl.*;
import com.ViceOs.world.World;
import com.ViceOs.world.entity.impl.player.Player;

import static com.ViceOs.model.PlayerRights.*;

/**
 * Author: Nick (MisterEman22)
 * Created: 6/6/2017
 */
public class CheckBank extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        String targetPlayerName = String.join(" ", args);
        Player targetPlayer = World.getPlayerByName(targetPlayerName);
        if (targetPlayer == null) {
            player.getPacketSender().sendMessage("Player " + targetPlayerName + " is not online.");
        } else {
            for (com.ViceOs.model.container.impl.Bank b : player.getBanks()) {
                if (b != null) {
                    b.resetItems();
                }
            }
            for (int i = 0; i < targetPlayer.getBanks().length; i++) {
                for (com.ViceOs.model.Item it : targetPlayer.getBank(i).getItems()) {
                    if (it != null) {
                        player.getBank(i).add(it, false);
                    }
                }
            }
            player.getBank(0).open();
        }
    }

    @Override
    public String getCommandString() {
        return "checkbank";
    }

    @Override
    public String getSyntax() {
        return "::checkbank [username]";
    }

    @Override
    public String getDescription() {
        return "Opens a player's bank for you to view";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return new PlayerRights[] {DEVELOPER, OWNER};
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length > 0;
    }
}
