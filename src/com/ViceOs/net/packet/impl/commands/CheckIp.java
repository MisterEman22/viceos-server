package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.world.World;
import com.ViceOs.world.content.PlayerPunishment;
import com.ViceOs.world.entity.impl.player.Player;

import java.util.ArrayList;
import java.util.List;

import static com.ViceOs.model.PlayerRights.*;

/**
 * Author: Nick (MisterEman22)
 * Created: 6/5/2017
 */
public class CheckIp extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        String targetPlayerName = String.join(" ", args);
        Player targetPlayer = World.getPlayerByName(targetPlayerName);
        if (targetPlayer == null) {
            player.getPacketSender().sendMessage("Player " + targetPlayerName + " is not online.");
        } else {
            String ip = targetPlayer.getHostAddress();
            List<String> players = new ArrayList<>();
            for (Player p : World.getPlayers()) {
                if (p != null && p.getHostAddress().equals(ip)) {
                    players.add(p.getUsername());
                }
            }
            player.getPacketSender().sendMessage(targetPlayer.getUsername() + "'s IP address is " + ip);
            player.getPacketSender().sendMessage("Online players with this IP: " + String.join(", ", players));
        }
    }

    @Override
    public String getCommandString() {
        return "checkip";
    }

    @Override
    public String getSyntax() {
        return "::checkip [username]";
    }

    @Override
    public String getDescription() {
        return "Checks the IP of a player";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return new PlayerRights[] {ADMINISTRATOR, DEVELOPER, OWNER};
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length > 0;
    }
}
