package com.ViceOs.net.packet.impl.commands;

import java.util.Arrays;
import java.util.List;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.world.entity.impl.player.Player;

/**
 * Author: Nick (MisterEman22)
 * Created: 5/26/2017
 */
public abstract class Command {
    public abstract void doCommand(Player player, String[] args);

    public abstract String getCommandString();

    public abstract String getSyntax();

    public abstract String getDescription();

    protected abstract PlayerRights[] getAllowedPlayerRights();

    public boolean isAllowed(Player player) {
        List<PlayerRights> allowedPlayerRights = Arrays.asList(getAllowedPlayerRights());
        return allowedPlayerRights.contains(player.getRights());
    }

    public abstract boolean isValidArgs(String[] args);

}
