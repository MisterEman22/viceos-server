package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.net.packet.impl.CommandPacketListener;
import com.ViceOs.world.entity.impl.player.Player;

/**
 * Author: Nick (MisterEman22)
 * Created: 5/26/2017
 */
public class Commands extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        for (Command c : CommandPacketListener.commands.values()) {
            if (c.isAllowed(player)) {
                player.getPacketSender().sendMessage(c.getSyntax() + " - " + c.getDescription());
            }
        }
    }

    @Override
    public String getCommandString() {
        return "commands";
    }

    @Override
    public String getSyntax() {
        return "::commands";
    }

    @Override
    public String getDescription() {
        return "Displays this list of commands";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return PlayerRights.values();
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length == 0;
    }
}
