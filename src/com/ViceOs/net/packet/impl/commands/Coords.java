package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.model.Position;
import com.ViceOs.world.entity.impl.player.Player;

import static com.ViceOs.model.PlayerRights.*;

/**
 * Author: Nick (MisterEman22)
 * Created: 6/20/2017
 */
public class Coords extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        Position p = player.getPosition();
        player.getPacketSender().sendMessage("X: " + p.getX() + ", Y: " + p.getY() + ", Z: " + p.getZ());
    }

    @Override
    public String getCommandString() {
        return "coords";
    }

    @Override
    public String getSyntax() {
        return "::coords";
    }

    @Override
    public String getDescription() {
        return "Displays your current position";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return new PlayerRights[] {DEVELOPER};
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length == 0;
    }
}
