package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.world.content.MemberScrolls;
import com.ViceOs.world.entity.impl.player.Player;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * Author: Nick (MisterEman22)
 * Created: 5/28/2017
 */
public class DClaim extends Command {
    private static final String SECRET = "5dccf0618d5bc054f1cddf66b5a49c55";

    @Override
    public void doCommand(Player player, String[] args) {
        try {
            String playerName = player.getUsername();
            URL url = new URL("http://app.gpay.io/api/runescape/" + playerName + "/" + SECRET);
            BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
            String results = reader.readLine();
            if (results.toLowerCase().contains("!error:")) {
                System.out.println(results);
                player.getPacketSender().sendMessage("An error occurred. Please report this to an administrator.");
            } else {
                String[] donationIds = results.split(",");
                int donationAmount = 0;
                for (String donationId : donationIds) {
                    switch (Integer.parseInt(donationId)) {
                        case 0:
                            break;
                        case 21937:
                            donationAmount += 10;
                            break;
                        case 21938:
                            donationAmount += 30;
                            break;
                        case 21939:
                            donationAmount += 60;
                            break;
                        case 21940:
                            donationAmount += 120;
                            break;
                        case 21941:
                            donationAmount += 240;
                            break;
                        case 21942:
                            donationAmount += 600;
                            break;
                    }
                }
                if (donationAmount > 0) {
                    player.getPointsHandler().incrementDonationPoints(donationAmount);
                    player.incrementAmountDonated(donationAmount);
                    player.getPointsHandler().refreshPanel();
                    player.getPacketSender().sendMessage("Thanks for your donation! You have been credited with " + donationAmount + " donation points.");
                    System.out.println(player.getUsername() + " claimed $" + donationAmount + " in donations");
                    MemberScrolls.checkForRankUpdate(player);
                } else {
                    player.getPacketSender().sendMessage("No donations found. If this is in error, please contact an administrator.");
                }
            }
        } catch (IOException e) {}
    }

    @Override
    public String getCommandString() {
        return "dclaim";
    }

    @Override
    public String getSyntax() {
        return "::dclaim";
    }

    @Override
    public String getDescription() {
        return "Claims any donations you've made";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return PlayerRights.values();
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length == 0;
    }
}
