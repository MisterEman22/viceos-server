package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.world.World;
import com.ViceOs.world.content.MemberScrolls;
import com.ViceOs.world.entity.impl.player.Player;

import java.util.HashMap;
import java.util.Map;

import static com.ViceOs.model.PlayerRights.*;
import static com.ViceOs.model.PlayerRights.DEVELOPER;
import static com.ViceOs.model.PlayerRights.OWNER;

/**
 * Author: Nick (MisterEman22)
 * Created: 5/29/2017
 */
public class Demote extends Command {
    private static final Map<PlayerRights, PlayerRights> DEMOTIONS = new HashMap<PlayerRights, PlayerRights>(){
        {
            put(OWNER, DEVELOPER);
            put(DEVELOPER, ADMINISTRATOR);
            put(ADMINISTRATOR, MODERATOR);
            put(MODERATOR, SUPPORT);
            put(SUPPORT, PLAYER);
        }
    };

    @Override
    public void doCommand(Player player, String[] args) {
        String targetPlayerName = String.join(" ", args);
        Player targetPlayer = World.getPlayerByName(targetPlayerName);

        if (targetPlayer == null) {
            player.getPacketSender().sendMessage("Player " + targetPlayerName + " is not online.");
        } else {
            PlayerRights oldRights = targetPlayer.getRights();
            PlayerRights newRights;
            if (DEMOTIONS.containsKey(oldRights)) {
                newRights = DEMOTIONS.get(oldRights);
            } else {
                player.getPacketSender().sendMessage(targetPlayer.getUsername() + " cannot be demoted from their current rank.");
                return;
            }

            targetPlayer.setRights(newRights);
            targetPlayer.getPacketSender().sendRights();
            MemberScrolls.checkForRankUpdate(targetPlayer);
            targetPlayer.getPacketSender().sendMessage(player.getUsername() + " has demoted you to " + targetPlayer.getRights().name() + ".");
            player.getPacketSender().sendMessage("You have demoted " + targetPlayer.getUsername() + " to " + targetPlayer.getRights().name() + ".");
        }
    }

    @Override
    public String getCommandString() {
        return "demote";
    }

    @Override
    public String getSyntax() {
        return "::demote [username]";
    }

    @Override
    public String getDescription() {
        return "Demotes a player to the previous staff rank";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return new PlayerRights[] {OWNER, DEVELOPER};
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length > 0;
    }
}
