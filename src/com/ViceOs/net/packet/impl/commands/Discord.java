package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.world.entity.impl.player.Player;

/**
 * Author: Nick (MisterEman22)
 * Created: 5/26/2017
 */
public class Discord extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        player.getPacketSender().sendString(1, "http://discord.gg/TUvZgQn");
        player.getPacketSender().sendMessage("Opening Discord!");
    }

    @Override
    public String getCommandString() {
        return "discord";
    }

    @Override
    public String getSyntax() {
        return "::discord";
    }

    @Override
    public String getDescription() {
        return "Opens our Discord";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return PlayerRights.values();
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length == 0;
    }
}
