package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.world.entity.impl.player.Player;

/**
 * Author: Nick (MisterEman22)
 * Created: 5/28/2017
 */
public class Donate extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        player.getPacketSender().sendString(1, "http://app.gpay.io/store/viceos/5867");
        player.getPacketSender().sendMessage("Opening donate webpage...");
        player.getPacketSender().sendMessage("Once you've donated, use ::dclaim to claim your donation.");
    }

    @Override
    public String getCommandString() {
        return "donate";
    }

    @Override
    public String getSyntax() {
        return "::donate";
    }

    @Override
    public String getDescription() {
        return "Opens the donating webpage";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return PlayerRights.values();
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length == 0;
    }
}
