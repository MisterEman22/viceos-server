package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.model.definitions.ItemDefinition;
import com.ViceOs.model.definitions.NpcDefinition;
import com.ViceOs.model.definitions.drops.DropInfoLine;
import com.ViceOs.model.definitions.drops.DropTable;
import com.ViceOs.model.definitions.drops.DropTableEntry;
import com.ViceOs.model.definitions.drops.NPCDrops;
import com.ViceOs.world.entity.impl.player.Player;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Author: Nick (MisterEman22)
 * Created: 5/28/2017
 */
public class Drop extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        final String inputName = String.join(" ", args);

        final int npcId = NpcDefinition.forName(inputName).getId();
        if (npcId == -1) {
            player.sendMessage("Npc not found: " + inputName);
            return;
        }

        String npcName = NpcDefinition.forName(inputName).getName();

        DropTable dropTable = NPCDrops.getDropTable(npcId);
        if (dropTable == null) {
            player.sendMessage(
                    "No drop table found for " + npcName + " " + npcId + " " + NpcDefinition.forName(npcName).getId());
            return;
        }

        for (int i = 26141; i <= 26220; i++) {
            player.getPA().sendFrame126(i, "");
        }

        int line = 26141;
        List<DropTableEntry> entries = dropTable.getEntries();
        List<DropInfoLine> lines = new ArrayList<>();

        for (DropTableEntry e : entries) {
            lines.addAll(e.getInfoLines(1));
        }
        lines.sort(new Comparator<DropInfoLine>() {
            @Override
            public int compare(DropInfoLine o1, DropInfoLine o2) {
                int chanceComp = o1.getChanceDenom() - o2.getChanceDenom();
                if (chanceComp == 0) {
                    return o1.getInfoString().compareTo(o2.getInfoString());
                } else {
                    return chanceComp;
                }
            }
        });

        for (int i = 0; i < lines.size(); i++) {
            player.getPA().sendFrame126(line + i, lines.get(i).getInfoString());
        }
        player.getPA().sendFrame126(26113, "");
        player.getPA().sendFrame126(26114, "");
        player.getPA().sendFrame126(26110, "Drop Rates: " + npcName);
        player.getPA().sendInterface(26139);
    }

    @Override
    public String getCommandString() {
        return "drop";
    }

    @Override
    public String getSyntax() {
        return "::drop [npcName]";
    }

    @Override
    public String getDescription() {
        return "Opens the drop table for an enemy";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return PlayerRights.values();
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length > 0;
    }
}
