package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.model.Position;
import com.ViceOs.world.content.transportation.TeleportHandler;
import com.ViceOs.world.entity.impl.player.Player;

/**
 * Author: Nick (MisterEman22)
 * Created: 5/26/2017
 */
public class Duel extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        if (player.getSummoning().getFamiliar() != null) {
            player.getPacketSender().sendMessage("You must dismiss your familiar before teleporting to the arena!");
        } else {
            TeleportHandler.teleportPlayer(player, new Position(3364, 3267),
                    player.getSpellbook().getTeleportType());
        }
    }

    @Override
    public String getCommandString() {
        return "duel";
    }

    @Override
    public String getSyntax() {
        return "::duel";
    }

    @Override
    public String getDescription() {
        return "Teleports you to the duel arena";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return PlayerRights.values();
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length == 0;
    }
}
