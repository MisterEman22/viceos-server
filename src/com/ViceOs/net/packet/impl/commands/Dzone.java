package com.ViceOs.net.packet.impl.commands;

import static com.ViceOs.model.PlayerRights.*;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.model.Position;
import com.ViceOs.world.content.transportation.TeleportHandler;
import com.ViceOs.world.entity.impl.player.Player;

/**
 * Author: Nick (MisterEman22)
 * Created: 5/26/2017
 */
public class Dzone extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        TeleportHandler.teleportPlayer(player, new Position(3363, 9638),
                player.getSpellbook().getTeleportType());
    }

    @Override
    public String getCommandString() {
        return "dzone";
    }

    @Override
    public String getSyntax() {
        return "::dzone";
    }

    @Override
    public String getDescription() {
        return "Teleports you to the donator zone";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return new PlayerRights[] {DONATOR, SUPER_DONATOR, EXTREME_DONATOR, LEGENDARY_DONATOR, UBER_DONATOR, SUPPORT, MODERATOR, ADMINISTRATOR, OWNER, DEVELOPER};
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length == 0;
    }
}
