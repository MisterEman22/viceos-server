package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.world.entity.impl.player.Player;

/**
 * Author: Nick (MisterEman22)
 * Created: 5/28/2017
 */
public class Empty extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        player.getPacketSender().sendInterfaceRemoval().sendMessage("You clear your inventory.");
        player.getSkillManager().stopSkilling();
        player.getInventory().resetItems().refreshItems();
    }

    @Override
    public String getCommandString() {
        return "empty";
    }

    @Override
    public String getSyntax() {
        return "::empty";
    }

    @Override
    public String getDescription() {
        return "Empties your inventory";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return PlayerRights.values();
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length == 0;
    }
}
