package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.world.content.ffa.FreeForAll;
import com.ViceOs.world.entity.impl.player.Player;

/**
 * Author: Nick (MisterEman22)
 * Created: 5/26/2017
 */
public class Ffa extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        FreeForAll.tryJoin(player);
    }

    @Override
    public String getCommandString() {
        return "ffa";
    }

    @Override
    public String getSyntax() {
        return "::ffa";
    }

    @Override
    public String getDescription() {
        return "Enters you into the Free-For-All lobby";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return PlayerRights.values();
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length == 0;
    }
}
