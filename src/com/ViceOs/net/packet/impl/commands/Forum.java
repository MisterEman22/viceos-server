package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.world.entity.impl.player.Player;

/**
 * Author: Nick (MisterEman22)
 * Created: 5/26/2017
 */
public class Forum extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        player.getPacketSender().sendString(1, "http://www.vice-rsps.club/forums");
        player.getPacketSender().sendMessage("Attempting to open: ViceOS Forums");
    }

    @Override
    public String getCommandString() {
        return "forum";
    }

    @Override
    public String getSyntax() {
        return "::forum";
    }

    @Override
    public String getDescription() {
        return "Opens the forum website";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return PlayerRights.values();
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length == 0;
    }
}
