package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.model.Position;
import com.ViceOs.world.content.transportation.TeleportHandler;
import com.ViceOs.world.entity.impl.player.Player;

/**
 * Author: Nick (MisterEman22)
 * Created: 5/26/2017
 */
public class Gamble extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        TeleportHandler.teleportPlayer(player, new Position(2440, 3088), player.getSpellbook().getTeleportType());
        player.getPacketSender().sendMessage("@red@Video evidence is required to file a report.");
        player.getPacketSender().sendMessage("@red@Only Staff + Ranked Players in 'Dice' Can Middleman!");
        player.getPacketSender().sendMessage("@red@No MM = No Refunds. SO USE A MM!");
        player.getPacketSender().sendMessage("@red@Type ::gamble2 For a second gambling location!");
    }

    @Override
    public String getCommandString() {
        return "gamble";
    }

    @Override
    public String getSyntax() {
        return "::gamble";
    }

    @Override
    public String getDescription() {
        return "Teleports you to the gamble zone";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return PlayerRights.values();
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length == 0;
    }
}
