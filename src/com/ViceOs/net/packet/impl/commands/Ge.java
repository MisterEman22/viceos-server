package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.Locations;
import com.ViceOs.model.PlayerRights;
import com.ViceOs.world.content.grandexchange.GrandExchange;
import com.ViceOs.world.entity.impl.player.Player;

/**
 * Author: Nick (MisterEman22)
 * Created: 6/6/2017
 */
public class Ge extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        GrandExchange.open(player);
    }

    @Override
    public String getCommandString() {
        return "ge";
    }

    @Override
    public String getSyntax() {
        return "::ge";
    }

    @Override
    public String getDescription() {
        return "Opens the Grand Exchange interface";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return PlayerRights.values();
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length == 0;
    }
}
