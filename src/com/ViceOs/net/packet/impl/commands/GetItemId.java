package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.model.definitions.ItemDefinition;
import com.ViceOs.world.entity.impl.player.Player;

import static com.ViceOs.model.PlayerRights.*;

/**
 * Author: Nick (MisterEman22)
 * Created: 5/29/2017
 */
public class GetItemId extends Command {
    private static final int MAX_ITEMS_FOUND = 20;

    @Override
    public void doCommand(Player player, String[] args) {
        String itemName = String.join(" ", args).toLowerCase();
        player.getPacketSender().sendMessage("---------- Finding item id for item - " + itemName + " ----------");
        int itemsFound = 0;
        for (int i = 0; i < ItemDefinition.getMaxAmountOfItems(); i++) {
            if (ItemDefinition.forId(i).getName().toLowerCase().contains(itemName)) {
                player.getPacketSender().sendMessage(ItemDefinition.forId(i).getName() + "] - id: " + i);
                itemsFound++;
            }
            if (itemsFound >= MAX_ITEMS_FOUND) {
                player.getPacketSender().sendMessage("Max results found (" + MAX_ITEMS_FOUND + "). If you did not get the correct item, try using a more specific item name.");
                return;
            }
        }
        if (itemsFound == 0) {
            player.getPacketSender().sendMessage("No items found.");
        }
    }

    @Override
    public String getCommandString() {
        return "getitemid";
    }

    @Override
    public String getSyntax() {
        return "::getitemid [itemName]";
    }

    @Override
    public String getDescription() {
        return "Gets the id of an item by name";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return new PlayerRights[] {ADMINISTRATOR, OWNER, DEVELOPER};
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length > 0;
    }
}
