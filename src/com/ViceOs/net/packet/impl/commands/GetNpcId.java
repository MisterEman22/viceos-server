package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.model.definitions.NpcDefinition;
import com.ViceOs.world.entity.impl.player.Player;

import static com.ViceOs.model.PlayerRights.*;

/**
 * Author: Nick (MisterEman22)
 * Created: 6/3/2017
 */
public class GetNpcId extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        final String inputName = String.join(" ", args);

        final NpcDefinition npcDef = NpcDefinition.forName(inputName);
        player.getPacketSender().sendMessage(npcDef.getName() + " - id: " + npcDef.getId());
    }

    @Override
    public String getCommandString() {
        return "getnpcid";
    }

    @Override
    public String getSyntax() {
        return "::getnpcid [npcName]";
    }

    @Override
    public String getDescription() {
        return "Gets an NPC id from its name";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return new PlayerRights[] {DEVELOPER};
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length > 0;
    }
}
