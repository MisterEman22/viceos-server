package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.world.World;
import com.ViceOs.world.entity.impl.player.Player;

import java.util.Arrays;

import static com.ViceOs.model.PlayerRights.*;

/**
 * Author: Nick (MisterEman22)
 * Created: 5/28/2017
 */
public class GiveDPoints extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        int amount = Integer.parseInt(args[0]);
        String targetPlayerName = String.join(" ", Arrays.copyOfRange(args, 1, args.length));
        Player targetPlayer = World.getPlayerByName(targetPlayerName);
        if (targetPlayer == null) {
            player.getPacketSender().sendMessage("Player " + targetPlayerName + " is not online.");
        } else {
            targetPlayer.getPointsHandler().incrementDonationPoints(amount);
            targetPlayer.getPointsHandler().refreshPanel();
            player.getPacketSender().sendMessage(targetPlayer.getUsername() + " has been granted " + amount + " donation points.");
        }
    }

    @Override
    public String getCommandString() {
        return "givedpoints";
    }

    @Override
    public String getSyntax() {
        return "::givedpoints [amount] [username]";
    }

    @Override
    public String getDescription() {
        return "Gives donator points to a player";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return new PlayerRights[] {OWNER, DEVELOPER};
    }

    @Override
    public boolean isValidArgs(String[] args) {
        try {
            Integer.parseInt(args[0]);
        } catch (NumberFormatException e) {
            return false;
        }
        return args.length > 1;
    }
}
