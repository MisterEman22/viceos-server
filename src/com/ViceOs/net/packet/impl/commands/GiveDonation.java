package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.world.World;
import com.ViceOs.world.content.MemberScrolls;
import com.ViceOs.world.content.PlayerPanel;
import com.ViceOs.world.entity.impl.player.Player;

import java.util.Arrays;

import static com.ViceOs.model.PlayerRights.*;

/**
 * Author: Nick (MisterEman22)
 * Created: 5/29/2017
 */
public class GiveDonation extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        int amount = Integer.parseInt(args[0]);
        String targetPlayerName = String.join(" ", Arrays.copyOfRange(args, 1, args.length));
        Player targetPlayer = World.getPlayerByName(targetPlayerName);
        if (targetPlayer == null) {
            player.getPacketSender().sendMessage("Player " + targetPlayerName + " is not online.");
        } else {
            targetPlayer.incrementAmountDonated(amount);
            targetPlayer.getPacketSender().sendMessage("Your account has gained funds worth $" + amount + ". Your total is now at $" + targetPlayer.getAmountDonated() + ".");
            player.getPacketSender().sendMessage(targetPlayer.getUsername() + " has been given a $" + amount + " donation. Their total is now at $" + targetPlayer.getAmountDonated() + ".");
            MemberScrolls.checkForRankUpdate(targetPlayer);
            PlayerPanel.refreshPanel(targetPlayer);
        }
    }

    @Override
    public String getCommandString() {
        return "givedonation";
    }

    @Override
    public String getSyntax() {
        return "::givedonation [amount] [player]";
    }

    @Override
    public String getDescription() {
        return "Adds to a player's donated amount (doesn't give points)";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return new PlayerRights[] {OWNER, DEVELOPER};
    }

    @Override
    public boolean isValidArgs(String[] args) {
        try {
            Integer.parseInt(args[0]);
        } catch (NumberFormatException e) {
            return false;
        }
        return args.length > 1;
    }
}
