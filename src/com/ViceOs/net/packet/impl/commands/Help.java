package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.world.World;
import com.ViceOs.world.entity.impl.player.Player;

/**
 * Author: Nick (MisterEman22)
 * Created: 5/26/2017
 */
public class Help extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        if (player.getLastYell().elapsed(30000)) {
            World.sendStaffMessage("<col=FF0066><img=10> [TICKET SYSTEM]<col=6600FF> " + player.getUsername()
                    + " has requested help. Please help them!");
            player.getLastYell().reset();
            player.getPacketSender()
                    .sendMessage("<col=663300>Your help request has been received. Please be patient.");
        } else {
            player.getPacketSender().sendMessage("")
                    .sendMessage("<col=663300>You need to wait 30 seconds before using this again.").sendMessage(
                    "<col=663300>If it's an emergency, please private message a staff member directly instead.");
        }
    }

    @Override
    public String getCommandString() {
        return "help";
    }

    @Override
    public String getSyntax() {
        return "::help";
    }

    @Override
    public String getDescription() {
        return "Sends a help request to online staff";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return PlayerRights.values();
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length == 0;
    }
}
