package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.world.entity.impl.player.Player;

import static com.ViceOs.model.PlayerRights.*;

/**
 * Author: Nick (MisterEman22)
 * Created: 6/2/2017
 */
public class Interface extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        int interfaceId = Integer.parseInt(args[0]);
        player.getPA().sendInterface(interfaceId);
    }

    @Override
    public String getCommandString() {
        return "interface";
    }

    @Override
    public String getSyntax() {
        return "::interface [interfaceId]";
    }

    @Override
    public String getDescription() {
        return "Opens an interface";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return new PlayerRights[] {DEVELOPER};
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return true;
    }
}
