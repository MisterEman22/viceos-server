package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.world.World;
import com.ViceOs.world.content.PlayerLogs;
import com.ViceOs.world.content.PlayerPunishment;
import com.ViceOs.world.entity.impl.player.Player;

import static com.ViceOs.model.PlayerRights.*;

/**
 * Author: Nick (MisterEman22)
 * Created: 6/5/2017
 */
public class Ipban extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        String targetPlayerName = String.join(" ", args);
        Player targetPlayer = World.getPlayerByName(targetPlayerName);
        if (targetPlayer == null) {
            player.getPacketSender().sendMessage("Player " + targetPlayerName + " is not online.");
        } else if (PlayerPunishment.IPBanned(targetPlayer.getHostAddress())) {
            player.getPacketSender().sendMessage("Player " + targetPlayer.getUsername() + "'s IP is already banned. Command logs written.");
        } else {
            final String bannedIP = targetPlayer.getHostAddress();
            PlayerPunishment.addBannedIP(bannedIP);
            player.getPacketSender().sendMessage("Player " + targetPlayer.getUsername() + "'s IP was successfully banned. Command logs written.");
            for (Player playersToBan : World.getPlayers()) {
                if (playersToBan == null) {
                    continue;
                }
                if (playersToBan.getHostAddress().equals(bannedIP)) {
                    PlayerLogs.log(player.getUsername(), "" + player.getUsername() + " just IPBanned " + playersToBan.getUsername() + "!");
                    World.deregister(playersToBan);
                    player.getPacketSender().sendMessage("Player " + playersToBan.getUsername() + " was successfully IPBanned. Command logs written.");
                }
            }
        }
    }

    @Override
    public String getCommandString() {
        return "ipban";
    }

    @Override
    public String getSyntax() {
        return "::ipban [username]";
    }

    @Override
    public String getDescription() {
        return "Bans the IP of a player";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return new PlayerRights[] {ADMINISTRATOR, DEVELOPER, OWNER};
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length > 0;
    }
}
