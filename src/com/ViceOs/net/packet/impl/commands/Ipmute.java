package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.world.World;
import com.ViceOs.world.content.PlayerLogs;
import com.ViceOs.world.content.PlayerPunishment;
import com.ViceOs.world.entity.impl.player.Player;

import static com.ViceOs.model.PlayerRights.*;

/**
 * Author: Nick (MisterEman22)
 * Created: 6/5/2017
 */
public class Ipmute extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        String targetPlayerName = String.join(" ", args);
        Player targetPlayer = World.getPlayerByName(targetPlayerName);
        if (targetPlayer == null) {
            player.getPacketSender().sendMessage("Player " + targetPlayerName + " is not online.");
        } else if (PlayerPunishment.IPMuted(targetPlayer.getHostAddress())) {
            player.getPacketSender().sendMessage("Player " + targetPlayer.getUsername() + "'s IP is already muted. Command logs written.");
        } else {
            final String mutedIP = targetPlayer.getHostAddress();
            PlayerPunishment.addMutedIP(mutedIP);
            player.getPacketSender().sendMessage("Player " + targetPlayer.getUsername() + " was successfully IPMuted. Command logs written.");
            targetPlayer.getPacketSender().sendMessage("You have been IPMuted by " + player.getUsername() + ".");
            PlayerLogs.log(player.getUsername(), "" + player.getUsername() + " just IPMuted " + targetPlayer.getUsername() + "!");
        }
    }

    @Override
    public String getCommandString() {
        return "ipmute";
    }

    @Override
    public String getSyntax() {
        return "::ipmute [username]";
    }

    @Override
    public String getDescription() {
        return "Mutes the IP of a player";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return new PlayerRights[] {ADMINISTRATOR, DEVELOPER, OWNER};
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length > 0;
    }
}
