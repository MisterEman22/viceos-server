package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.world.entity.impl.player.Player;

import static com.ViceOs.model.PlayerRights.*;

/**
 * Author: Nick (MisterEman22)
 * Created: 5/28/2017
 */
public class Item extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        int itemId = Integer.parseInt(args[0]);
        int itemAmount = Integer.parseInt(args[1]);
        if (itemAmount > Integer.MAX_VALUE) {
            itemAmount = Integer.MAX_VALUE;
        }
        com.ViceOs.model.Item item = new com.ViceOs.model.Item(itemId, itemAmount);
        player.getInventory().add(item, true);
    }

    @Override
    public String getCommandString() {
        return "item";
    }

    @Override
    public String getSyntax() {
        return "::item [itemId] [itemAmount]";
    }

    @Override
    public String getDescription() {
        return "Spawns items in your inventory";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return new PlayerRights[] {OWNER, DEVELOPER};
    }

    @Override
    public boolean isValidArgs(String[] args) {
        try {
            Integer.parseInt(args[0]);
            Integer.parseInt(args[1]);
        } catch (NumberFormatException e) {
            return false;
        }
        return args.length == 2;
    }
}
