package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.world.World;
import com.ViceOs.world.content.PlayerLogs;
import com.ViceOs.world.content.PlayerPunishment;
import com.ViceOs.world.entity.impl.player.Player;

import static com.ViceOs.model.PlayerRights.*;

/**
 * Author: Nick (MisterEman22)
 * Created: 5/28/2017
 */
public class Jail extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        String targetPlayerName = String.join(" ", args);
        Player targetPlayer = World.getPlayerByName(targetPlayerName);
        if (targetPlayer == null) {
            player.getPacketSender().sendMessage("Player " + targetPlayerName + " is not online.");
        } else {
            if (PlayerPunishment.Jail.isJailed(targetPlayer)) {
                player.getPacketSender().sendMessage("That player is already jailed!");
                return;
            }
            if (PlayerPunishment.Jail.jailPlayer(targetPlayer)) {
                targetPlayer.getSkillManager().stopSkilling();
                PlayerLogs.log(player.getUsername(),
                        "" + player.getUsername() + " just jailed " + targetPlayer.getUsername() + "!");
                player.getPacketSender().sendMessage("Jailed player: " + targetPlayer.getUsername() + "");
                targetPlayer.getPacketSender().sendMessage("You have been jailed by " + player.getUsername() + ".");
            } else {
                player.getPacketSender().sendMessage("Jail is currently full.");
            }
        }
    }

    @Override
    public String getCommandString() {
        return "jail";
    }

    @Override
    public String getSyntax() {
        return "::jail [username]";
    }

    @Override
    public String getDescription() {
        return "Jails a player";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return new PlayerRights[] {SUPPORT, MODERATOR, ADMINISTRATOR, OWNER, DEVELOPER};
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length > 0;
    }
}
