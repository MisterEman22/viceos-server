package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.world.content.droppreview.SLASHBASH;
import com.ViceOs.world.entity.impl.player.Player;

/**
 * Author: Nick (MisterEman22)
 * Created: 5/28/2017
 */
public class Kbd extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        player.getPacketSender().sendMessage("Sorry this command isn't implemented yet.");
    }

    @Override
    public String getCommandString() {
        return "kbd";
    }

    @Override
    public String getSyntax() {
        return "::kbd";
    }

    @Override
    public String getDescription() {
        return "Teleports you to King Black Dragon";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return PlayerRights.values();
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return true;
    }
}
