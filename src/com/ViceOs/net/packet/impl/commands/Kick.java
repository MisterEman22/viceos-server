package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.Locations;
import com.ViceOs.model.PlayerRights;
import com.ViceOs.world.World;
import com.ViceOs.world.content.PlayerLogs;
import com.ViceOs.world.entity.impl.player.Player;
import com.ViceOs.world.entity.impl.player.PlayerHandler;

import static com.ViceOs.model.PlayerRights.*;

/**
 * Author: Nick (MisterEman22)
 * Created: 5/28/2017
 */
public class Kick extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        String targetPlayerName = String.join(" ", args);
        Player targetPlayer = World.getPlayerByName(targetPlayerName);
        if (targetPlayer == null) {
            player.getPacketSender().sendMessage("Player " + targetPlayerName + " is not online.");
        } else {
            if (targetPlayer.getLocation() != Locations.Location.WILDERNESS) {
                World.deregister(targetPlayer);
                PlayerHandler.handleLogout(targetPlayer);
                player.getPacketSender().sendMessage("Kicked " + targetPlayer.getUsername() + ".");
                PlayerLogs.log(player.getUsername(), player.getUsername() + " just kicked " + targetPlayer.getUsername() + "!");
            } else {
                player.getPacketSender().sendMessage("Cannot kick a player that is in the wilderness.");
            }
        }
    }

    @Override
    public String getCommandString() {
        return "kick";
    }

    @Override
    public String getSyntax() {
        return "::kick [username]";
    }

    @Override
    public String getDescription() {
        return "Kicks a player from the server";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return new PlayerRights[] {MODERATOR, ADMINISTRATOR, OWNER, DEVELOPER};
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length > 0;
    }
}
