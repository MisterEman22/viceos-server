package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.Flag;
import com.ViceOs.model.PlayerRights;
import com.ViceOs.model.Skill;
import com.ViceOs.world.content.skill.SkillManager;
import com.ViceOs.world.entity.impl.player.Player;

import static com.ViceOs.model.PlayerRights.*;

/**
 * Author: Nick (MisterEman22)
 * Created: 5/29/2017
 */
public class Master extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        for (Skill skill : Skill.values()) {
            int level = SkillManager.getMaxAchievingLevel(skill);
            player.getSkillManager().setCurrentLevel(skill, level).setMaxLevel(skill, level).setExperience(skill,
                    SkillManager.getExperienceForLevel(level == 120 ? 120 : 99));
        }
        player.getPacketSender().sendMessage("You are now a master of all skills.");
        player.getUpdateFlag().flag(Flag.APPEARANCE);
    }

    @Override
    public String getCommandString() {
        return "master";
    }

    @Override
    public String getSyntax() {
        return "::master";
    }

    @Override
    public String getDescription() {
        return "Sets all of your skills to max level";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return new PlayerRights[] {ADMINISTRATOR, OWNER, DEVELOPER};
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length == 0;
    }
}
