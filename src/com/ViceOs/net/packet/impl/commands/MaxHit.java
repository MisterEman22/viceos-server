package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.world.content.combat.DesolaceFormulas;
import com.ViceOs.world.entity.impl.player.Player;

/**
 * Author: Nick (MisterEman22)
 * Created: 5/26/2017
 */
public class MaxHit extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        int attack = DesolaceFormulas.getMeleeAttack(player) / 10;
        int range = DesolaceFormulas.getRangedAttack(player) / 10;
        int magic = DesolaceFormulas.getMagicAttack(player) / 10;
        player.getPacketSender().sendMessage("@bla@Melee attack: @or2@" + attack + "@bla@, ranged attack: @or2@"
                + range + "@bla@, magic attack: @or2@" + magic);
    }

    @Override
    public String getCommandString() {
        return "maxhit";
    }

    @Override
    public String getSyntax() {
        return "::maxhit";
    }

    @Override
    public String getDescription() {
        return "Displays your current max damage output";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return PlayerRights.values();
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length == 0;
    }
}
