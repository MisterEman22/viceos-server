package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.GameSettings;
import com.ViceOs.model.PlayerRights;
import com.ViceOs.world.World;
import com.ViceOs.world.entity.impl.player.Player;

import static com.ViceOs.model.PlayerRights.*;

/**
 * Author: Nick (MisterEman22)
 * Created: 5/28/2017
 */
public class MoveHome extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        String targetPlayerName = String.join(" ", args);
        Player targetPlayer = World.getPlayerByName(targetPlayerName);
        if (targetPlayer == null) {
            player.getPacketSender().sendMessage("Player " + targetPlayerName + " is not online.");
        } else {
            targetPlayer.moveTo(GameSettings.DEFAULT_POSITION.copy());
            targetPlayer.getPacketSender().sendMessage("You've been teleported home by " + player.getUsername() + ".");
            player.getPacketSender().sendMessage("Sucessfully moved " + targetPlayer.getUsername() + " to home.");
        }
    }

    @Override
    public String getCommandString() {
        return "movehome";
    }

    @Override
    public String getSyntax() {
        return "::movehome [username]";
    }

    @Override
    public String getDescription() {
        return "Moves a player to home";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return new PlayerRights[] {SUPPORT, MODERATOR, ADMINISTRATOR, OWNER, DEVELOPER};
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length > 0;
    }
}
