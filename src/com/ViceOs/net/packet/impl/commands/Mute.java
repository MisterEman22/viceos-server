package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.util.Misc;
import com.ViceOs.world.World;
import com.ViceOs.world.content.PlayerLogs;
import com.ViceOs.world.content.PlayerPunishment;
import com.ViceOs.world.entity.impl.player.Player;
import com.ViceOs.world.entity.impl.player.PlayerSaving;

import static com.ViceOs.model.PlayerRights.*;

/**
 * Author: Nick (MisterEman22)
 * Created: 6/5/2017
 */
public class Mute extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        String targetPlayerName = String.join(" ", args);
        if (!PlayerSaving.playerExists(targetPlayerName)) {
            player.getPacketSender().sendMessage("Player " + targetPlayerName + " does not exist.");
        } else if (PlayerPunishment.muted(targetPlayerName)) {
            player.getPacketSender().sendMessage("Player " + targetPlayerName + " already has an active mute.");
        } else {
            PlayerLogs.log(player.getUsername(), "" + player.getUsername() + " just muted " + targetPlayerName + "!");
            PlayerPunishment.mute(targetPlayerName);
            player.getPacketSender()
                    .sendMessage("Player " + targetPlayerName + " was successfully muted. Command logs written.");
            Player targetPlayer = World.getPlayerByName(targetPlayerName);
            if (targetPlayer != null) {
                targetPlayer.getPacketSender().sendMessage("You have been muted by " + player.getUsername() + ".");
            }
        }
    }

    @Override
    public String getCommandString() {
        return "mute";
    }

    @Override
    public String getSyntax() {
        return "::mute [username]";
    }

    @Override
    public String getDescription() {
        return "Mutes a player";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return new PlayerRights[] {SUPPORT, MODERATOR, ADMINISTRATOR, DEVELOPER, OWNER};
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length > 0;
    }
}
