package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.model.Position;
import com.ViceOs.world.content.transportation.TeleportHandler;
import com.ViceOs.world.entity.impl.player.Player;

/**
 * Author: Nick (MisterEman22)
 * Created: 5/26/2017
 */
public class Pk extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        TeleportHandler.teleportPlayer(player, new Position(3086, 3519), player.getSpellbook().getTeleportType());
    }

    @Override
    public String getCommandString() {
        return "pk";
    }

    @Override
    public String getSyntax() {
        return "::pk";
    }

    @Override
    public String getDescription() {
        return "Teleports you to Edgeville pking";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return PlayerRights.values();
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length == 0;
    }
}
