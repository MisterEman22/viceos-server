package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.Animation;
import com.ViceOs.model.PlayerRights;
import com.ViceOs.world.entity.impl.player.Player;

import static com.ViceOs.model.PlayerRights.*;

/**
 * Author: Nick (MisterEman22)
 * Created: 5/29/2017
 */
public class PlayAnimation extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        int animationId = Integer.parseInt(args[0]);
        player.performAnimation(new Animation(animationId));
    }

    @Override
    public String getCommandString() {
        return "playanimation";
    }

    @Override
    public String getSyntax() {
        return "::playanimation [animationId]";
    }

    @Override
    public String getDescription() {
        return "Plays an animation";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return new PlayerRights[] {OWNER, DEVELOPER};
    }

    @Override
    public boolean isValidArgs(String[] args) {
        try {
            Integer.parseInt(args[0]);
        } catch (NumberFormatException e) {
            return false;
        }
        return args.length == 1;
    }
}
