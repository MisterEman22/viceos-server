package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.Graphic;
import com.ViceOs.model.PlayerRights;
import com.ViceOs.world.entity.impl.player.Player;

import static com.ViceOs.model.PlayerRights.*;

/**
 * Author: Nick (MisterEman22)
 * Created: 6/1/2017
 */
public class PlayGraphic extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        int graphicId = Integer.parseInt(args[0]);
        player.performGraphic(new Graphic(graphicId));
    }

    @Override
    public String getCommandString() {
        return "playgraphic";
    }

    @Override
    public String getSyntax() {
        return "::playgraphic [graphicId]";
    }

    @Override
    public String getDescription() {
        return "Plays a graphic";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return new PlayerRights[] {OWNER, DEVELOPER};
    }

    @Override
    public boolean isValidArgs(String[] args) {
        try {
            Integer.parseInt(args[0]);
        } catch (NumberFormatException e) {
            return false;
        }
        return args.length == 1;
    }
}
