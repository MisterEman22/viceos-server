package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.world.content.PlayersOnlineInterface;
import com.ViceOs.world.entity.impl.player.Player;

/**
 * Author: Nick (MisterEman22)
 * Created: 5/26/2017
 */
public class Players extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        PlayersOnlineInterface.showInterface(player);
    }

    @Override
    public String getCommandString() {
        return "players";
    }

    @Override
    public String getSyntax() {
        return "::players";
    }

    @Override
    public String getDescription() {
        return "Displays the current online players";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return PlayerRights.values();
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length == 0;
    }
}
