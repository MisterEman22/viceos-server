package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.world.World;
import com.ViceOs.world.content.ProfileViewing;
import com.ViceOs.world.entity.impl.player.Player;

/**
 * Author: Nick (MisterEman22)
 * Created: 5/26/2017
 */
public class Profile extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        if (args.length < 1) {
            ProfileViewing.view(player, player);
            return;
        }
        final String name = String.join(" ", args);
        final Player targetPlayer = World.getPlayerByName(name);
        if (targetPlayer == null) {
            player.sendMessage("Player not online: " + name);
            return;
        }
        ProfileViewing.view(player, targetPlayer);
        return;
    }

    @Override
    public String getCommandString() {
        return "profile";
    }

    @Override
    public String getSyntax() {
        return "::profile [username]";
    }

    @Override
    public String getDescription() {
        return "Displays a player's profile";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return PlayerRights.values();
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length > 0;
    }
}
