package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.world.World;
import com.ViceOs.world.entity.impl.player.Player;

import java.util.HashMap;
import java.util.Map;

import static com.ViceOs.model.PlayerRights.*;

/**
 * Author: Nick (MisterEman22)
 * Created: 5/29/2017
 */
public class Promote extends Command {
    private static final Map<PlayerRights, PlayerRights> PROMOTIONS = new HashMap<PlayerRights, PlayerRights>(){
        {
            put(null, SUPPORT);
            put(SUPPORT, MODERATOR);
            put(MODERATOR, ADMINISTRATOR);
            put(ADMINISTRATOR, DEVELOPER);
            put(DEVELOPER, OWNER);
            put(OWNER, null);
        }
    };

    @Override
    public void doCommand(Player player, String[] args) {
        String targetPlayerName = String.join(" ", args);
        Player targetPlayer = World.getPlayerByName(targetPlayerName);

        if (targetPlayer == null) {
            player.getPacketSender().sendMessage("Player " + targetPlayerName + " is not online.");
        } else {
            PlayerRights oldRights = targetPlayer.getRights();
            PlayerRights newRights;
            if (PROMOTIONS.containsKey(oldRights)) {
                newRights = PROMOTIONS.get(oldRights);
            } else {
                newRights = PROMOTIONS.get(null);
            }

            if (newRights == null) {
                player.getPacketSender().sendMessage(targetPlayer.getUsername() + " cannot be promoted from their current rank.");
                return;
            }

            targetPlayer.setRights(newRights);
            targetPlayer.getPacketSender().sendRights();
            targetPlayer.getPacketSender().sendMessage(player.getUsername() + " has promoted you to " + newRights.name() + ".");
            player.getPacketSender().sendMessage("You have promoted " + targetPlayer.getUsername() + " to " + newRights.name() + ".");
        }
    }

    @Override
    public String getCommandString() {
        return "promote";
    }

    @Override
    public String getSyntax() {
        return "::promote [username]";
    }

    @Override
    public String getDescription() {
        return "Promotes a player to the next staff rank";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return new PlayerRights[] {OWNER, DEVELOPER};
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length > 0;
    }
}
