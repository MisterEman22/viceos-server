package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.model.definitions.drops.NPCDrops;
import com.ViceOs.world.entity.impl.player.Player;

import static com.ViceOs.model.PlayerRights.*;

/**
 * Author: Nick (MisterEman22)
 * Created: 6/6/2017
 */
public class ReloadDrops extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        NPCDrops.init();
        player.getPacketSender().sendMessage("Drops have been reloaded");
    }

    @Override
    public String getCommandString() {
        return "reloaddrops";
    }

    @Override
    public String getSyntax() {
        return "::reloaddrops";
    }

    @Override
    public String getDescription() {
        return "Reloads monster drop tables";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return new PlayerRights[] {DEVELOPER, OWNER};
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length == 0;
    }
}
