package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.model.container.impl.Shop;
import com.ViceOs.world.entity.impl.player.Player;

import static com.ViceOs.model.PlayerRights.*;

/**
 * Author: Nick (MisterEman22)
 * Created: 6/12/2017
 */
public class ReloadShops extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        Shop.ShopManager.init();
        player.getPacketSender().sendMessage("Shops have been reloaded");
    }

    @Override
    public String getCommandString() {
        return "reloadshops";
    }

    @Override
    public String getSyntax() {
        return "::reloadshops";
    }

    @Override
    public String getDescription() {
        return "Reloads the shops";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return new PlayerRights[] {DEVELOPER, OWNER};
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length == 0;
    }
}
