package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.Flag;
import com.ViceOs.model.PlayerRights;
import com.ViceOs.model.Skill;
import com.ViceOs.world.content.skill.SkillManager;
import com.ViceOs.world.entity.impl.player.Player;

import static com.ViceOs.model.PlayerRights.*;

/**
 * Author: Nick (MisterEman22)
 * Created: 6/1/2017
 */
public class Reset extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        for (Skill skill : Skill.values()) {
            int level = skill.equals(Skill.CONSTITUTION) ? 100 : skill.equals(Skill.PRAYER) ? 10 : 1;
            player.getSkillManager().setCurrentLevel(skill, level).setMaxLevel(skill, level).setExperience(skill,
                    SkillManager.getExperienceForLevel(skill == Skill.CONSTITUTION ? 10 : 1));
        }
        player.getPacketSender().sendConsoleMessage("Your skill levels have now been reset.");
        player.getUpdateFlag().flag(Flag.APPEARANCE);
    }

    @Override
    public String getCommandString() {
        return "reset";
    }

    @Override
    public String getSyntax() {
        return "::reset";
    }

    @Override
    public String getDescription() {
        return "Resets all your skills to their starting level";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return new PlayerRights[] {DEVELOPER, OWNER};
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length == 0;
    }
}
