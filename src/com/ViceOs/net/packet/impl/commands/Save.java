package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.world.entity.impl.player.Player;

/**
 * Author: Nick (MisterEman22)
 * Created: 5/26/2017
 */
public class Save extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        player.save();
        player.getPacketSender().sendMessage("Your progress has been saved.");
    }

    @Override
    public String getCommandString() {
        return "save";
    }

    @Override
    public String getSyntax() {
        return "::save";
    }

    @Override
    public String getDescription() {
        return "Saves your account";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return PlayerRights.values();
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length == 0;
    }
}
