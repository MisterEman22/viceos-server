package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.world.World;
import com.ViceOs.world.entity.impl.player.Player;

import static com.ViceOs.model.PlayerRights.*;

/**
 * Author: Nick (MisterEman22)
 * Created: 5/28/2017
 */
public class SaveAll extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        World.savePlayers();
        player.getPacketSender().sendMessage("Saved players!");
    }

    @Override
    public String getCommandString() {
        return "saveall";
    }

    @Override
    public String getSyntax() {
        return "::saveall";
    }

    @Override
    public String getDescription() {
        return "Saves all players' accounts";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return new PlayerRights[] {ADMINISTRATOR, OWNER, DEVELOPER};
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length == 0;
    }
}
