package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.world.content.clan.ClanChatManager;
import com.ViceOs.world.entity.impl.player.Player;

import static com.ViceOs.model.PlayerRights.*;

/**
 * Author: Nick (MisterEman22)
 * Created: 6/6/2017
 */
public class SaveClans extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        ClanChatManager.save();
    }

    @Override
    public String getCommandString() {
        return "saveclans";
    }

    @Override
    public String getSyntax() {
        return "::saveclans";
    }

    @Override
    public String getDescription() {
        return "Manually saves all clan channels";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return new PlayerRights[] {DEVELOPER, OWNER};
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length == 0;
    }
}
