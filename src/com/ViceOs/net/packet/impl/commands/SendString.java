package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.world.entity.impl.player.Player;

import java.util.Arrays;

import static com.ViceOs.model.PlayerRights.DEVELOPER;

/**
 * Author: Nick (MisterEman22)
 * Created: 6/27/2017
 */
public class SendString extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        int stringId = Integer.parseInt(args[0]);
        String s = String.join(" ", Arrays.copyOfRange(args, 1, args.length));
        player.getPacketSender().sendString(stringId, s);
    }

    @Override
    public String getCommandString() {
        return "sendstring";
    }

    @Override
    public String getSyntax() {
        return "::sendstring [id] [string]";
    }

    @Override
    public String getDescription() {
        return "Sends string to the client";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return new PlayerRights[] {DEVELOPER};
    }

    @Override
    public boolean isValidArgs(String[] args) {
        if (args.length < 2) return false;
        try {
            Integer.parseInt(args[0]);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
