package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.world.content.PlayerPanel;
import com.ViceOs.world.entity.impl.player.Player;

/**
 * Author: Nick (MisterEman22)
 * Created: 5/26/2017
 */
public class SetEmail extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        String email = String.join(" ", args);
        player.setEmailAddress(email);
        player.getPacketSender().sendMessage("You set your account's email adress to: [" + email + "] ");
        // Achievements.finishAchievement(player,
        // AchievementData.SET_AN_EMAIL_ADDRESS);
        PlayerPanel.refreshPanel(player);
    }

    @Override
    public String getCommandString() {
        return "setemail";
    }

    @Override
    public String getDescription() {
        return "Sets your email";
    }

    @Override
    public String getSyntax() {
        return "::setemail [email]";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return PlayerRights.values();
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length == 1;
    }
}
