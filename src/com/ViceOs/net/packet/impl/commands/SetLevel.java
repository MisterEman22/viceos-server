package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.model.Skill;
import com.ViceOs.world.content.skill.SkillManager;
import com.ViceOs.world.entity.impl.player.Player;

import static com.ViceOs.model.PlayerRights.*;

/**
 * Author: Nick (MisterEman22)
 * Created: 5/29/2017
 */
public class SetLevel extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        int skillId = Integer.parseInt(args[0]);
        int level = Integer.parseInt(args[1]);
        if (level > 15000) {
            player.getPacketSender().sendMessage("Sorry, the maximum level is 15000.");
            return;
        }
        Skill skill = Skill.forId(skillId);
        player.getSkillManager().setCurrentLevel(skill, level).setMaxLevel(skill, level).setExperience(skill,
                SkillManager.getExperienceForLevel(level));
        player.getPacketSender().sendMessage("You have set your " + skill.getName() + " level to " + level);
    }

    @Override
    public String getCommandString() {
        return "setlevel";
    }

    @Override
    public String getSyntax() {
        return "::setlevel [skillId] [level]";
    }

    @Override
    public String getDescription() {
        return "Sets the level of a skill";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return new PlayerRights[] {OWNER, DEVELOPER};
    }

    @Override
    public boolean isValidArgs(String[] args) {
        try {
            Integer.parseInt(args[0]);
            Integer.parseInt(args[1]);
        } catch (NumberFormatException e) {
            return false;
        }
        return args.length == 2;
    }
}
