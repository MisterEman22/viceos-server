package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.GameServer;
import com.ViceOs.model.PlayerRights;
import com.ViceOs.world.World;
import com.ViceOs.world.content.WellOfGoodwill;
import com.ViceOs.world.content.WellOfWealth;
import com.ViceOs.world.content.clan.ClanChatManager;
import com.ViceOs.world.content.grandexchange.GrandExchangeOffers;
import com.ViceOs.world.entity.impl.player.Player;
import com.ViceOs.world.entity.impl.player.PlayerHandler;

import static com.ViceOs.model.PlayerRights.*;

/**
 * Author: Nick (MisterEman22)
 * Created: 5/29/2017
 */
public class Shutdown extends Command {
    private ShutdownTimer shutdownProcess;
    @Override
    public void doCommand(Player player, String[] args) {
        if (shutdownProcess == null) {
            shutdownProcess = new ShutdownTimer();
            shutdownProcess.start();
            player.getPacketSender().sendMessage("Shutdown process has been started. Server will shut down in 2 minutes.");
            System.out.println("Shutdown process has been started by " + player.getUsername() + ". Server will shut down in 2 minutes.");
        } else {
            player.getPacketSender().sendMessage("Shutdown process is already active.");
        }
    }

    @Override
    public String getCommandString() {
        return "shutdown";
    }

    @Override
    public String getSyntax() {
        return "::shutdown";
    }

    @Override
    public String getDescription() {
        return "Shuts down the server after 2 minutes of warnings";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return new PlayerRights[] {OWNER, DEVELOPER};
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length == 0;
    }
}

class ShutdownTimer extends Thread {
    @Override
    public void run() {
        try {
            World.sendMessage("<col=FF0000><shad=0>Server restart in 2 minutes. Please finish what you are doing and log out.");
            sleep(60000);
            World.sendMessage("<col=FF0000><shad=0>Server restart in 1 minute. Please finish what you are doing and log out.");
            sleep(30000);
            World.sendMessage("<col=FF0000><shad=0>Server restart in 30 seconds. Please finish what you are doing and log out.");
            sleep(20000);
            for (int i = 10; i > 0; i--) {
                World.sendMessage("<col=FF0000><shad=0>Server restart in " + i + " seconds. Please finish what you are doing and log out.");
                sleep(1000);
            }
        } catch (Exception e) {
        }

        System.exit(0);

    }
}
