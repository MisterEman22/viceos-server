package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.world.content.combat.weapon.CombatSpecial;
import com.ViceOs.world.entity.impl.player.Player;

import static com.ViceOs.model.PlayerRights.*;

/**
 * Author: Nick (MisterEman22)
 * Created: 6/1/2017
 */
public class Spec extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        player.setSpecialPercentage(1000);
        CombatSpecial.updateBar(player);
    }

    @Override
    public String getCommandString() {
        return "spec";
    }

    @Override
    public String getSyntax() {
        return "::spec";
    }

    @Override
    public String getDescription() {
        return "Fills your special attack meter";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return new PlayerRights[] {DEVELOPER, OWNER};
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length == 0;
    }
}
