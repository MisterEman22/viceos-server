package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.model.Position;
import com.ViceOs.world.content.transportation.TeleportHandler;
import com.ViceOs.world.content.transportation.TeleportType;
import com.ViceOs.world.entity.impl.player.Player;

import static com.ViceOs.model.PlayerRights.*;

/**
 * Author: Nick (MisterEman22)
 * Created: 5/28/2017
 */
public class Staffzone extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        TeleportHandler.teleportPlayer(player, new Position(2038, 4497), TeleportType.NORMAL);
    }

    @Override
    public String getCommandString() {
        return "staffzone";
    }

    @Override
    public String getSyntax() {
        return "::staffzone";
    }

    @Override
    public String getDescription() {
        return "Teleports you to the staff zone";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return new PlayerRights[] {SUPPORT, MODERATOR, ADMINISTRATOR, OWNER, DEVELOPER};
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length == 0;
    }
}
