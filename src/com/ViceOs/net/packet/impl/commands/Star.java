package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.world.content.ShootingStar;
import com.ViceOs.world.entity.impl.player.Player;

/**
 * Author: Nick (MisterEman22)
 * Created: 5/26/2017
 */
public class Star extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        player.getPacketSender().sendMessage("<img=4> <shad=1><col=FF9933> The Crashed Star has landed at: "
                + ShootingStar.CRASHED_STAR.getStarLocation().playerPanelFrame + "");
    }

    @Override
    public String getCommandString() {
        return "star";
    }

    @Override
    public String getSyntax() {
        return "::star";
    }

    @Override
    public String getDescription() {
        return "Displays the current crashed star location";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return PlayerRights.values();
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length == 0;
    }
}
