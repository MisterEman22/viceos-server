package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.world.content.ffa.FreeForAll;
import com.ViceOs.world.entity.impl.player.Player;

import static com.ViceOs.model.PlayerRights.*;

/**
 * Author: Nick (MisterEman22)
 * Created: 6/20/2017
 */
public class StartFfa extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        String type = args[0];
        if (!FreeForAll.isValidType(type)) {
            player.getPacketSender().sendMessage("Invalid FFA type: " + type);
        } else if (FreeForAll.isEventStarted()) {
            player.getPacketSender().sendMessage("FFA cannot be started because one is already active. Type ::ffa to join.");
        } else {
            FreeForAll.startEvent(type);
            if (!FreeForAll.isEventStarted()) {
                player.getPacketSender().sendMessage("Error starting FFA with type " + type);
            }
        }
    }

    @Override
    public String getCommandString() {
        return "startffa";
    }

    @Override
    public String getSyntax() {
        return "::startffa [type]";
    }

    @Override
    public String getDescription() {
        return "Starts a free-for-all event";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return new PlayerRights[] {ADMINISTRATOR, DEVELOPER, OWNER};
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length == 1;
    }
}
