package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.net.packet.impl.CommandPacketListener;
import com.ViceOs.world.entity.impl.player.Player;

/**
 * Author: Nick (MisterEman22)
 * Created: 5/28/2017
 */
public class Syntax extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        String targetCommandString = args[0];
        if (CommandPacketListener.commands.containsKey(targetCommandString)) {
            player.getPacketSender().sendMessage(CommandPacketListener.commands.get(targetCommandString).getSyntax());
        } else {
            player.getPacketSender().sendMessage("The command \"" + targetCommandString + "\" does not exist.");
        }
    }

    @Override
    public String getCommandString() {
        return "syntax";
    }

    @Override
    public String getSyntax() {
        return "::syntax [command]";
    }

    @Override
    public String getDescription() {
        return "Shows the syntax for a command";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return PlayerRights.values();
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length == 1;
    }
}
