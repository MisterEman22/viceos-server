package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.model.Position;
import com.ViceOs.world.content.transportation.TeleportHandler;
import com.ViceOs.world.content.transportation.TeleportType;
import com.ViceOs.world.entity.impl.player.Player;

import static com.ViceOs.model.PlayerRights.*;

/**
 * Author: Nick (MisterEman22)
 * Created: 5/29/2017
 */
public class Tele extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        int x, y, z;
        x = Integer.parseInt(args[0]);
        y = Integer.parseInt(args[1]);
        if (args.length > 2) {
            z = Integer.parseInt(args[2]);
        } else {
            z = player.getPosition().getZ();
        }
        Position position = new Position(x, y, z);
        TeleportHandler.teleportPlayer(player, position, TeleportType.NORMAL);
        player.getPacketSender().sendMessage("Teleporting to " + position.toString());
    }

    @Override
    public String getCommandString() {
        return "tele";
    }

    @Override
    public String getSyntax() {
        return "::tele [x] [y] [?z]";
    }

    @Override
    public String getDescription() {
        return "Teleports you to a position";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return new PlayerRights[] {SUPPORT, MODERATOR, ADMINISTRATOR, OWNER, DEVELOPER};
    }

    @Override
    public boolean isValidArgs(String[] args) {
        try {
            Integer.parseInt(args[0]);
            Integer.parseInt(args[1]);
            if (args.length > 2)
                Integer.parseInt(args[2]);
        } catch (NumberFormatException e) {
            return false;
        }
        return args.length == 2 || args.length == 3;
    }
}
