package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.world.World;
import com.ViceOs.world.content.transportation.TeleportHandler;
import com.ViceOs.world.content.transportation.TeleportType;
import com.ViceOs.world.entity.impl.player.Player;

import static com.ViceOs.model.PlayerRights.*;
import static com.ViceOs.model.PlayerRights.DEVELOPER;

/**
 * Author: Nick (MisterEman22)
 * Created: 5/28/2017
 */
public class TeleTo extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        String targetPlayerName = String.join(" ", args);
        Player targetPlayer = World.getPlayerByName(targetPlayerName);

        if (targetPlayer == null) {
            player.getPacketSender().sendMessage("Player " + targetPlayerName + " is not online.");
        } else {
            boolean canTele = TeleportHandler.checkReqs(player, targetPlayer.getPosition().copy())
                    && player.getRegionInstance() == null && targetPlayer.getRegionInstance() == null;
            if (player.getRights() == PlayerRights.SUPPORT || player.getRights() == PlayerRights.MODERATOR) {
                if (targetPlayer.getMinigameAttributes().getDungeoneeringAttributes().getParty() != null) {
                    player.sendMessage("you can't teleport to someone who is in dungeonnering");
                    return;
                }
            }

            if (canTele) {
                TeleportHandler.teleportPlayer(player, targetPlayer.getPosition().copy(), TeleportType.NORMAL);
                player.getPacketSender().sendMessage("Teleporting to player: " + targetPlayer.getUsername() + "");
            } else {
                player.getPacketSender().sendMessage("You can not teleport to this player at the moment. Minigame maybe?");
            }
        }
    }

    @Override
    public String getCommandString() {
        return "teleto";
    }

    @Override
    public String getSyntax() {
        return "::teleto [username]";
    }

    @Override
    public String getDescription() {
        return "Teleports you to a player";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return new PlayerRights[] {SUPPORT, MODERATOR, ADMINISTRATOR, OWNER, DEVELOPER};
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length > 0;
    }
}
