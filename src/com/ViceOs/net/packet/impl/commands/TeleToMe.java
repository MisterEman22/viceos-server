package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.world.World;
import com.ViceOs.world.content.transportation.TeleportHandler;
import com.ViceOs.world.content.transportation.TeleportType;
import com.ViceOs.world.entity.impl.player.Player;

import static com.ViceOs.model.PlayerRights.*;

/**
 * Author: Nick (MisterEman22)
 * Created: 5/29/2017
 */
public class TeleToMe extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        String targetPlayerName = String.join(" ", args);
        Player targetPlayer = World.getPlayerByName(targetPlayerName);

        if (targetPlayer == null) {
            player.getPacketSender().sendMessage("Player " + targetPlayerName + " is not online.");
        } else {
            boolean canTele = TeleportHandler.checkReqs(player, targetPlayer.getPosition().copy())
                    && player.getRegionInstance() == null && targetPlayer.getRegionInstance() == null;
            if (canTele) {
                TeleportHandler.teleportPlayer(targetPlayer, player.getPosition().copy(), TeleportType.NORMAL);
                player.getPacketSender()
                        .sendMessage("Teleporting player to you: " + targetPlayer.getUsername() + "");
                targetPlayer.getPacketSender().sendMessage("You're being teleported to " + player.getUsername() + "...");
            } else {
                player.getPacketSender().sendMessage(
                        "You can not teleport that player at the moment. Maybe you or they are in a minigame?");
            }
        }
    }

    @Override
    public String getCommandString() {
        return "teletome";
    }

    @Override
    public String getSyntax() {
        return "::teletome [username]";
    }

    @Override
    public String getDescription() {
        return "Teleports a player to you";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return new PlayerRights[] {SUPPORT, MODERATOR, ADMINISTRATOR, OWNER, DEVELOPER};
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length > 0;
    }
}
