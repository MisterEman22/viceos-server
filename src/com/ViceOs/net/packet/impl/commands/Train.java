package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.model.Position;
import com.ViceOs.world.content.transportation.TeleportHandler;
import com.ViceOs.world.entity.impl.player.Player;

/**
 * Author: Nick (MisterEman22)
 * Created: 5/26/2017
 */
public class Train extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        TeleportHandler.teleportPlayer(player, new Position(2679, 3714), player.getSpellbook().getTeleportType());
    }

    @Override
    public String getCommandString() {
        return "train";
    }

    @Override
    public String getSyntax() {
        return "::train";
    }

    @Override
    public String getDescription() {
        return "Teleports you to the training zone";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return PlayerRights.values();
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length == 0;
    }
}
