package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.world.content.EvilTrees;
import com.ViceOs.world.entity.impl.player.Player;

/**
 * Author: Nick (MisterEman22)
 * Created: 5/26/2017
 */
public class Tree extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        player.getPacketSender().sendMessage("<img=4> <shad=1><col=FF9933> The Evil Tree has sprouted at: "
                + EvilTrees.SPAWNED_TREE.getTreeLocation().playerPanelFrame + "");
    }

    @Override
    public String getCommandString() {
        return "tree";
    }

    @Override
    public String getSyntax() {
        return "::tree";
    }

    @Override
    public String getDescription() {
        return "Displays the current evil tree location";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return PlayerRights.values();
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length == 0;
    }
}
