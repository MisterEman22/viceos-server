package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.world.content.PlayerLogs;
import com.ViceOs.world.content.PlayerPunishment;
import com.ViceOs.world.entity.impl.player.Player;
import com.ViceOs.world.entity.impl.player.PlayerSaving;

import static com.ViceOs.model.PlayerRights.*;

/**
 * Author: Nick (MisterEman22)
 * Created: 6/5/2017
 */
public class Unban extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        String targetPlayerName = String.join(" ", args);
        if (!PlayerSaving.playerExists(targetPlayerName)) {
            player.getPacketSender().sendMessage("Player " + targetPlayerName + " does not exist.");
        } else if (!PlayerPunishment.banned(targetPlayerName)) {
            player.getPacketSender().sendMessage("Player " + targetPlayerName + " is not banned!");
        } else {
            PlayerLogs.log(player.getUsername(), "" + player.getUsername() + " just unbanned " + targetPlayerName + "!");
            PlayerPunishment.unban(targetPlayerName);
            player.getPacketSender().sendMessage("Player " + targetPlayerName + " was successfully unbanned. Command logs written.");
        }
    }

    @Override
    public String getCommandString() {
        return "unban";
    }

    @Override
    public String getSyntax() {
        return "::unban [username]";
    }

    @Override
    public String getDescription() {
        return "Unbans a player";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return new PlayerRights[] {ADMINISTRATOR, DEVELOPER, OWNER};
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length > 0;
    }
}
