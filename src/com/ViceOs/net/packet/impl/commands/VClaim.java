package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.world.content.WellOfGoodwill;
import com.ViceOs.world.entity.impl.player.Player;
import com.motivoters.motivote.service.MotivoteRS;

/**
 * Author: Nick (MisterEman22)
 * Created: 5/28/2017
 */
public class VClaim extends Command {
    private final static MotivoteRS motivote = new MotivoteRS("ViceRsPs", "66a4192ee89a5608c7acfa9e1045ea2e");

    @Override
    public void doCommand(Player player, String[] args) {
        String auth = args[0];
        boolean success = motivote.redeemVote(auth);

        if (success) {
            player.getInventory().add(995, 2000000); // replace	995, 1000000 with 19670, 1 to give a vote scroll instead of cash.
            player.getInventory().add(19670, 1);
            player.getPacketSender().sendMessage("Auth redeemed, thanks for voting!");
            System.out.println(player.getUsername() + " claimed voting auth code " + auth);
            WellOfGoodwill.vote(player);
        }
        else {
            player.getPacketSender().sendMessage("Invalid auth code supplied. If you believe this is an error, please contact an Administrator.");
        }
    }

    @Override
    public String getCommandString() {
        return "vclaim";
    }

    @Override
    public String getSyntax() {
        return "::vclaim [authcode]";
    }

    @Override
    public String getDescription() {
        return "Claims your voting reward";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return PlayerRights.values();
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length == 1;
    }
}
