package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.Flag;
import com.ViceOs.model.PlayerRights;
import com.ViceOs.world.entity.impl.player.Player;

import static com.ViceOs.model.PlayerRights.*;

/**
 * Author: Nick (MisterEman22)
 * Created: 5/29/2017
 */
public class Visible extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        if (player.getNpcTransformationId() > 0) {
            player.setNpcTransformationId(-1);
            player.getPacketSender().sendMessage("You are now visible.");
        } else {
            player.setNpcTransformationId(8254);
            player.getPacketSender().sendMessage("You are now invisible.");
        }
        player.getUpdateFlag().flag(Flag.APPEARANCE);
    }

    @Override
    public String getCommandString() {
        return "visible";
    }

    @Override
    public String getSyntax() {
        return "::visible";
    }

    @Override
    public String getDescription() {
        return "Toggles your visibility to other players";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return new PlayerRights[] {ADMINISTRATOR, OWNER, DEVELOPER};
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length == 0;
    }
}
