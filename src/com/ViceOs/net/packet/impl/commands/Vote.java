package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.world.entity.impl.player.Player;

/**
 * Author: Nick (MisterEman22)
 * Created: 5/28/2017
 */
public class Vote extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        player.getPacketSender().sendString(1, "http://vicersps.motivoters.com/motivote/");
        player.getPacketSender().sendMessage("Opening vote webpage...");
        player.getPacketSender().sendMessage("Once you've voted, use ::vclaim to claim your reward.");
    }

    @Override
    public String getCommandString() {
        return "vote";
    }

    @Override
    public String getSyntax() {
        return "::vote";
    }

    @Override
    public String getDescription() {
        return "Opens the voting webpage";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return PlayerRights.values();
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length == 0;
    }
}
