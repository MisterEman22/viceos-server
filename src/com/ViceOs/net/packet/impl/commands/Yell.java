package com.ViceOs.net.packet.impl.commands;

import com.ViceOs.model.PlayerRights;
import com.ViceOs.world.World;
import com.ViceOs.world.content.PlayerPunishment;
import com.ViceOs.world.entity.impl.player.Player;

import static com.ViceOs.model.PlayerRights.*;

/**
 * Author: Nick (MisterEman22)
 * Created: 5/28/2017
 */
public class Yell extends Command {
    @Override
    public void doCommand(Player player, String[] args) {
        if (PlayerPunishment.muted(player.getUsername()) || PlayerPunishment.IPMuted(player.getHostAddress())) {
            player.getPacketSender().sendMessage("You are muted and cannot yell.");
            return;
        }
        int delay = player.getRights().getYellDelay();
        if (!player.getLastYell().elapsed((delay * 1000))) {
            player.getPacketSender().sendMessage(
                    "You must wait at least " + delay + " seconds between every yell-message you send.");
            return;
        }
        String yellMessage = String.join(" ", args);

        player.getLastYell().reset();
        World.sendMessage(player.getRights().getYellPrefix() + "<img=" + player.getRights().ordinal() + "> " + player.getUsername() + ": " + yellMessage);
    }

    @Override
    public String getCommandString() {
        return "yell";
    }

    @Override
    public String getSyntax() {
        return "::yell [message]";
    }

    @Override
    public String getDescription() {
        return "Broadcasts a message to all players";
    }

    @Override
    protected PlayerRights[] getAllowedPlayerRights() {
        return new PlayerRights[] {DONATOR, SUPER_DONATOR, EXTREME_DONATOR, LEGENDARY_DONATOR, UBER_DONATOR, YOUTUBER, SUPPORT, MODERATOR, ADMINISTRATOR, OWNER, DEVELOPER};
    }

    @Override
    public boolean isValidArgs(String[] args) {
        return args.length > 0;
    }
}
