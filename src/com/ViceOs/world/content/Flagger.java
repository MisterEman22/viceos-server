package com.ViceOs.world.content;

import com.ViceOs.world.World;
import com.ViceOs.world.entity.impl.player.Player;

/**
 * Author: Nick (MisterEman22)
 * Created: 6/8/2017
 */
public class Flagger {

    private static final String[] BLOCKED_WORDS = new String[]{
            "devious", "firepk", "ventrilica", "ventrili",
            "hade5", "hades5", "pvplegacy", "junglepk", "runeinsanit",
            "n3t", "n e t", "n et", "ne t", "c  0 m", "c 0 m", "C 0  m",
            "c  o  m", "runeinsanity", "runeinsanit", "runeinsan",
            "rune-insanity", "rune-insanit", ".c0", ".c0m",
            "legendsdomain", "createaforum", "vampirez", "-scape",
            "rswebclients", "pvplegacy", "junglepk", ".n3t", ".c0m", "c0m",
            "n3t", ".tk", ",net", "os-base", "os base", "osbase", "leech", "pvpmaster", "pvpmasters",
            "PvPMasters.", ",org", ",com", ",be", ",nl", ",info", "dspk",
            ".info", ".org", ".tk", ".net", ".com", ".co.uk", ".be", ".nl",
            ".dk", ".co.cz", ".co", ".us", ".biz", ".eu", ".de", ".cc", ". n e  t",
            ".i n f o", ".o r g", ".t k", ".n e t", ".c o m", ".c o . u k",
            ".b e", ".n l", ".d k", ".c o . c z", ".c o", ".u s", ".b i z",
            ".e u", ".d e", ".c c", ".(c)om", "(.)", "kandarin", "o r g", "www",
            "w w w", "w.w.w", "voidrsps", "void-ps", "desolace", "ikov", "soulsplit",
            "soulspawn", "atiloc", "@cr", "i k o v", "ik0v", "<img=", ":tradereq:", ":duelreq:",
            "<col=", "<shad=", "hostilityps", "hostilityp", "hostility ps"};

    public static boolean isFlagged(String message) {
        for(String s : BLOCKED_WORDS) {
            if(message.contains(s)) {
                return true;
            }
        }
        return false;
    }

    public static void flag(Player player, String message) {
        World.sendStaffMessage("@red@[Flagger] Player flagged: " + player.getUsername());
        World.sendStaffMessage("@red@[Flagger] Message: " + message);
    }
}
