package com.ViceOs.world.content;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import com.ViceOs.model.Item;
import com.ViceOs.world.entity.impl.player.Player;

public class LootingBag {
	
	private Player player;
	public static final int BAG = 10834;
	private List<Item> contents = new ArrayList<>();
	
	public LootingBag(Player player) {
		this.player = player;
	}
	
	public static void open(Player player) {
		if (player.getLootingBag().getContents().size() > 0) {
			player.getPacketSender().sendString(31716, "");
		} else {
			player.getPacketSender().sendString(31716, "The bag is empty");
		}
		player.getPacketSender().sendItemsOnInterface(31715, player.getLootingBag().getContents());
		player.getPacketSender().sendTabInterface(3, 31710);
	}
	
	public static void add(Player player, int slot, int amount) {
		if (slot < 0 || slot > 28) {
			return;
		}
		Item item = player.getInventory().get(slot);
		if (item == null) {
			return;
		}
		if (amount > item.getAmount()) {
			amount = item.getAmount();
		}
		Item toAdd = new Item(item.getId(), amount);
		boolean stackable = false;
		if (player.getInventory().contains(item.getId())) {
			if (toAdd.getDefinition().isStackable()) {
				stackable = true;
			}
			player.getInventory().delete(item, slot);
			Optional<Item> existing = player.getLootingBag().contains(toAdd);
			if (stackable && existing.isPresent()) {
				int index = player.getLootingBag().getContents().indexOf(existing.get());
				Item newItem = new Item(toAdd.getId(), player.getLootingBag().getContents().get(index).getAmount() + amount);
				player.getLootingBag().getContents().set(index, newItem);
			} else {
				player.getLootingBag().getContents().add(toAdd);
			}
			player.sendMessage("You have added x"+ amount +" "+ toAdd.getDefinition().getName() +" into your bag.");
		}
	}
	
	public static void bankItems(Player player) {
		for (Item item : player.getLootingBag().getContents()) {
			if (item == null) {
				continue;
			}
			Item toAdd = new Item(item.getId(), player.getLootingBag().getContents().get(player.getLootingBag().getContents().indexOf(item)).getAmount());
			player.getBank(0).add(toAdd);
		}
		player.sendMessage("You have emptied your looting bag contents into the bank.");
		player.getLootingBag().getContents().clear();
	}
	
	public static void close(Player player) {
		player.getPacketSender().sendTabInterface(3, 3213);
	}

	public List<Item> getContents() {
		return contents;
	}
	
	public void setContents(Item[] items) {
		for (Item item : items) {
			if (item == null) {
				continue;
			}
			player.getLootingBag().getContents().add(item);
		}
	}
	
	public void setContents(List<Item> items) {
		this.contents = items;
	}
	
	public Optional<Item> contains(Item item) {
		return contents.stream().filter(Objects::nonNull).filter(i -> i.getId() == item.getId()).findAny();
	}
}
