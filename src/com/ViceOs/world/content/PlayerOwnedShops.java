package com.ViceOs.world.content;

import com.ViceOs.GameServer;
import com.ViceOs.model.Item;
import com.ViceOs.model.container.impl.PlayerShop;
import com.ViceOs.model.input.impl.EnterNumberOfMinutesToFeature;
import com.ViceOs.model.input.impl.EnterPlayerShopItemToSearchFor;
import com.ViceOs.model.input.impl.EnterPlayerShopToFind;
import com.ViceOs.util.Misc;
import com.ViceOs.util.Stopwatch;
import com.ViceOs.world.entity.impl.player.Player;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.logging.Level;

/**
 * Created by Greg on 11/03/2017.
 */
public class PlayerOwnedShops {

    /**
     * TODO
     * Reorder?
     * Tidy up
     */

    public static final double SHOP_TAX = 5.0;//% out of 100

    public static final int SHOP_TAX_MAX = 5000000;/*Integer.MAX_VALUE;*/   //Maximum value of item taxed (5% tax of 10m item leaves seller with only 8m) which didn't seem fair

    public static final int FEATURED_PRICE = 10000;//Cost Per Minute

    private static final int FEATURED_TIME = 300000;//300000 milli = 5 minutes

    private static Stopwatch timer = new Stopwatch();
    public static final int INTERFACE_ID = 50000;
    static Map<String, PlayerShop> shops = new HashMap<>();

    public static Map<String, PlayerShop> getShops() {
        return shops;
    }

    public static boolean handleButtonClick(int buttonId, Player player) {
        if (buttonId < 0)
            buttonId = 65536 + buttonId;

        if (buttonId >= INTERFACE_ID + 8 && buttonId <= INTERFACE_ID + 44) {
            int featuredOption = (buttonId - (INTERFACE_ID + 8)) / 4;

            Map<String, Long> featuredList = getFeaturedList();
            if(featuredOption > featuredList.size() - 1) {
                if(featuredList.containsKey(player.getUsername()))
                    player.getPacketSender().sendMessage("Your shop is already featured. Wait till it runs out before purchasing again.");
                else {
                    player.setInputHandling(new EnterNumberOfMinutesToFeature());
                    player.getPacketSender().sendEnterAmountPrompt("Enter the number of minutes you want to buy (1440 mins max).");
                }
            } else
                openPlayerShop(player, (String) featuredList.keySet().toArray()[featuredOption]);
        }
        if (buttonId >= INTERFACE_ID + 64 && buttonId <= INTERFACE_ID + 559) {
            int shopOption = (buttonId - (INTERFACE_ID + 64)) / 5;
            if (shopOption < player.getShopList().size())
                openPlayerShop(player, player.getShopList().get(shopOption));
        }
        switch (buttonId) {
            case INTERFACE_ID + 562://Return
                openShopsInterface(player);
                break;
            case INTERFACE_ID + 48://Search by name
                player.setInputHandling(new EnterPlayerShopToFind());
                player.getPacketSender().sendEnterInputPrompt("Enter the name of the player whose shop you wish to find.");
                break;
            case INTERFACE_ID + 49://Search by item
                player.setInputHandling(new EnterPlayerShopItemToSearchFor());
                player.getPacketSender().sendEnterInputPrompt("Enter the name of the item you wish to buy.");
                break;
            case INTERFACE_ID + 50://Exact
                player.setPlayerShopSearchExact(true);
                break;
            case INTERFACE_ID + 51://Contains
                player.setPlayerShopSearchExact(false);
                break;
        }
        return false;
    }

    public static void openPlayerShop(Player player, String shopOwner) {
        String username = shopOwner.substring(0, 1).toUpperCase() + shopOwner.substring(1, shopOwner.length());
        PlayerShop shop = shops.get(username);
        if (shop == null) {
            if(player.getUsername().equalsIgnoreCase(username)) {
                createPlayerShop(player.getUsername());
                shop = shops.get(player.getUsername());
            } else {
                player.getPacketSender().sendMessage("Could not find a player's shop with the username '" + username + "'!");
                return;
            }
        }
        shop.open(player);
    }

    public static void openShopsInterface(Player player) {
        player.getShopList().clear();
        displayFeaturedList(player);
        player.getPacketSender().sendInterface(INTERFACE_ID);
        displayShopList(player, null);
        if (Misc.getMinutesPlayed(player) <= 190)
            player.getPacketSender().sendMessage("<img=10>Note: @red@" + SHOP_TAX + "%@bla@ tax will be taken on every item over " + SHOP_TAX + "gp sold in player owned shops.");
    }

    private static Map<String, Long> getFeaturedList() {
        Map<String, Long> featuredList = new HashMap<>();
        for(String user : shops.keySet()) {
            PlayerShop shop = shops.get(user);
            if(shop == null || shop.featuredTime <= 0)
                continue;
            featuredList.put(user, shop.featuredTime);
        }
        return featuredList;
    }

    private static void displayFeaturedList(Player player) {
        Map<String, Long> featuredList = getFeaturedList();
        int max = featuredList.size();
        if(max > 10)
            max = 10;
        for(int i = 0; i < max; i++) {
            String name = (String) featuredList.keySet().toArray()[i];
            //Owner name
            player.getPacketSender().sendString(INTERFACE_ID + 9 + (i * 4), name);
            //Time Remaining
            player.getPacketSender().sendString(INTERFACE_ID + 10 + (i * 4), "" + getTime(featuredList.get(name)));
        }
        for (int i = max; i < 10; i++) {
            //Owner name
            player.getPacketSender().sendString(INTERFACE_ID + 9 + (i * 4), "-");
            //Time Remaining
            player.getPacketSender().sendString(INTERFACE_ID + 10 + (i * 4), shopPrice(FEATURED_PRICE) + "/min");
        }
    }

    private static String getTime(Long time) {
        long timeLeft = time - System.currentTimeMillis();
        return milliToReadable(timeLeft);
    }

    public static String milliToReadable(Long time) {
        int minutes = getMinutes(time);
        int hours = getHours(time);
        int days = getDays(time);

        StringBuilder sb = new StringBuilder();

        if(days > 1)
            sb.append(days + " days");
        else if(days == 1)
            sb.append(days + " day");
        else {
            if(hours > 1)
                sb.append(hours + " hours");
            else if(hours == 1)
                sb.append(hours + " hour");
            else {
                if(minutes > 1)
                    sb.append(minutes + " mins");
                else if(minutes == 1)
                    sb.append(minutes + " min");
                else
                    sb.append("0 mins");
            }
        }
        return sb.toString();
    }

    private static int getDays(Long time) {
        return (int) ((time / (1000 * 60 * 60)) / 24);
    }

    private static int getHours(Long time) {
        return (int) ((time / (1000 * 60 * 60)) % 24);
    }

    private static int getMinutes(Long time) {
        return (int) ((time / (1000 * 60)) % 60);
    }


    public static void loadPlayerShops() {
        shops.clear();
        File folder = new File("./data/saves/shops/");
        if (!folder.exists())
            folder.mkdirs();

        File[] listOfFiles = folder.listFiles();
        for (int i = 0; i < listOfFiles.length; i++)
            if (listOfFiles[i].isFile())
                if (listOfFiles[i].length() != 0)
                    load(listOfFiles[i]);
                else
                    createPlayerShop(nameFromFile(listOfFiles[i]));
    }

    public static void load(Player player) {
        String name = player.getUsername();
        if (accountExists(name)) {
            PlayerShop shop = shops.get(name);
            if (shop != null) {
                shop.setPlayer(player);
                shop.username = name;
            }
        } else {
            createPlayerShop(name);
        }
    }

    private static void createPlayerShop(String name) {
        shops.put(name, new PlayerShop(null, -1, name, new Item(995), new Item[0], new int[0], 0, 0));
        save(name);
    }

    private static boolean accountExists(String username) {
        try {
            Path path = Paths.get("./data/saves/shops", username + ".json");
            File file = path.toFile();

            if (file.exists())
                return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private static void load(File file) {
        if (!file.exists())
            return;

        try (FileReader fileReader = new FileReader(file)) {
            JsonParser fileParser = new JsonParser();
            Gson builder = new GsonBuilder().create();
            JsonObject reader = (JsonObject) fileParser.parse(fileReader);

            long featuredTime = 0;
            long lastUpdated = 0;

            if(reader.has("featured"))
                featuredTime = reader.get("featured").getAsLong();
            if(reader.has("last-updated"))
                lastUpdated = reader.get("last-updated").getAsLong();

            Item[] items = builder.fromJson(reader.get("items").getAsJsonArray(), Item[].class);
            int[] prices = builder.fromJson(reader.get("prices").getAsJsonArray(), int[].class);
            String name = nameFromFile(file);

            PlayerShop shop = new PlayerShop(null, -1, name, new Item(995), items, prices, featuredTime, lastUpdated);
            if (reader.has("sales")) {
                PlayerShop.SoldItem[] sales = builder.fromJson(reader.get("sales").getAsJsonArray(), PlayerShop.SoldItem[].class);
                shop.addToSalesQueue(sales);
            }
            shops.put(name, shop);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
    }

    private static String nameFromFile(File file) {
        String name = file.getName();
        int pos = name.lastIndexOf(".");
        if (pos > 0)
            name = name.substring(0, pos);
        return name;
    }

    public static void save(String username) {
        try {
            PlayerShop shop = shops.get(username);
            if (shop == null)
                return;

            Path path = Paths.get("./data/saves/shops/", username + ".json");
            File file = path.toFile();
            file.getParentFile().setWritable(true);

            // Attempt to make the player save directory if it doesn't exist.
            if (!file.getParentFile().exists()) {
                try {
                    file.getParentFile().mkdirs();
                } catch (SecurityException e) {
                    System.out.println("Unable to create directory for player shop data!");
                }
            }
            try (FileWriter writer = new FileWriter(file)) {

                Gson builder = new GsonBuilder().setPrettyPrinting().create();
                JsonObject object = new JsonObject();

                object.addProperty("featured", shop.featuredTime);
                object.addProperty("last-updated", shop.lastUpdated);
                object.add("items", builder.toJsonTree(shop.getValidItems()));
                object.add("prices", builder.toJsonTree(shop.getValidPrices()));
                object.add("sales", builder.toJsonTree(shop.getSalesQueue()));
                writer.write(builder.toJson(object));
                writer.close();
            } catch (Exception e) {
                // An error happened while saving.
                GameServer.getLogger().log(Level.WARNING, "An error has occurred while saving a character shop file!", e);
            }
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    public static void saveShop(Player player) {
        PlayerShop s1 = (player.getShop() != null && player.getShop() instanceof PlayerShop) ? (PlayerShop) player.getShop() : null;
        PlayerShop s2 = shops.get(player.getUsername());

        if (s1 == null || s2 == null)
            return;

        if (s1.username.equals(player.getUsername()))
            s1.publicRefresh();
    }

    public static void searchForPlayer(Player player, String syntax) {
        List<String> captions = new ArrayList<>();
        player.getShopList().clear();
        boolean exact = player.isPlayerShopSearchExact();

        for (String user : shops.keySet())
            if ((exact && user.equalsIgnoreCase(syntax)) || (!exact && user.toLowerCase().contains(syntax.toLowerCase()))) {
                player.getShopList().add(user);
                captions.add(user + "'s store");
            }
        Collections.sort(player.getShopList());
        displayShopList(player, captions);
    }

    public static void searchItem(Player player, String syntax) {
        Map<String, Integer> prices = new HashMap<>();
        Map<String, String> userCaptions = new HashMap<>();

        boolean exact = player.isPlayerShopSearchExact();
        player.getShopList().clear();
        for (String user : shops.keySet()) {
            PlayerShop shop = shops.get(user);
            if (shop == null)
                continue;

            for (Item item : shop.getValidItems())
                if ((exact && item.getDefinition().getName().equalsIgnoreCase(syntax)) || (!exact && item.getDefinition().getName().toLowerCase().contains(syntax.toLowerCase()) && !prices.containsKey(user))) {
                    prices.put(user, shop.getPrice(item));
                    userCaptions.put(user, "Found: " + item.getDefinition().getName() + " for " + shopPrice(shop.getPrice(item)));
                }
        }
        //Sort by price
        prices = sortByComparator(prices, true);

        List<String> captions = new ArrayList<>();
        for (int i = 0; i < prices.size(); i++) {
            String name = (String) prices.keySet().toArray()[i];
            player.getShopList().add(name);
            captions.add(userCaptions.get(name));
        }

        if (player.getShopList().size() == 0)
            player.getPacketSender().sendMessage("No results for item '" + syntax + "'");
        displayShopList(player, captions);
    }

    private static Map<String, Integer> sortByComparator(Map<String, Integer> unsortMap, final boolean order) {

        List<Map.Entry<String, Integer>> list = new LinkedList<>(unsortMap.entrySet());

        // Sorting the list based on values
        Collections.sort(list, (o1, o2) -> {
            if (order)
                return o1.getValue().compareTo(o2.getValue());
            else
                return o2.getValue().compareTo(o1.getValue());
        });

        // Maintaining insertion order with the help of LinkedList
        Map<String, Integer> sortedMap = new LinkedHashMap<>();

        for (Map.Entry<String, Integer> entry : list)
            sortedMap.put(entry.getKey(), entry.getValue());

        return sortedMap;
    }

    public static String shopPrice(int shopPrice) {
        String ShopAdd = "";
        if (shopPrice < 1000) {
            ShopAdd = shopPrice + "gp";
        } else if (shopPrice >= 1000 && shopPrice < 1000000) {
            ShopAdd = (shopPrice / 1000) + "K";
        } else if (shopPrice >= 1000000 && shopPrice < 1000000000) {
            ShopAdd = (shopPrice / 1000000) + "M";
        } else if (shopPrice >= 1000000000) {
            ShopAdd = (shopPrice / 1000000000) + "B";
        }
        return ShopAdd;
    }

    public static void displayShopList(Player player, List<String> captions) {
        int start = player.getShopList().size();
        if (start > 100)
            start = 100;
        for (int i = 0; i < start; i++) {
            //Owner Names
            player.getPacketSender().sendString(INTERFACE_ID + 61 + (i * 5), player.getShopList().get(i));
            //Captions
            player.getPacketSender().sendString(INTERFACE_ID + 63 + (i * 5), captions.get(i));
        }
        for (int i = start; i < 100; i++) {
            //Owner Names
            player.getPacketSender().sendString(INTERFACE_ID + 61 + (i * 5), "");
            //Captions
            player.getPacketSender().sendString(INTERFACE_ID + 63 + (i * 5), "");
        }
    }

    public static void checkSales(String username) {
        PlayerShop shop = shops.get(username);

        if (shop == null)
            return;

        shop.checkSales();
    }

    public static void buyFeature(Player player, int minutes) {

        if(minutes > 1440 || minutes <= 0) {
            player.getPacketSender().sendMessage("Invalid number of minutes.");
            return;
        }

        PlayerShop shop = shops.get(player.getUsername());

        if(shop == null)
            return;

        int cost = minutes * FEATURED_PRICE;
        boolean usePouch = false;

        if (player.getMoneyInPouch() >= cost)
            usePouch = true;

        if(usePouch) {
            player.setMoneyInPouch(player.getMoneyInPouch() - cost);
            player.getPacketSender().sendString(8135, "" + player.getMoneyInPouch());//Update the money pouch
        } else {
            if(player.getInventory().getAmount(995) > cost) {
                player.getInventory().delete(995, cost);
            } else {
                player.getPacketSender().sendMessage("You do not have enough coins to purchase this many hours.");
                return;
            }
        }

        shop.featuredTime = System.currentTimeMillis() + (minutes * 60000);//1 minute in milliseconds
        save(player.getUsername());
        player.getPacketSender().sendMessage("You successfully bought a featured slot.");
        player.getPacketSender().sendMessage("Your slot will be featured for the next " + getTime(shop.featuredTime) + ".");
        displayFeaturedList(player);
    }

    public static void sequence() {
        if(timer.elapsed(FEATURED_TIME)) {
            //Check featured times for running out
            Map<String, Long> featuredList = getFeaturedList();

            if(featuredList.size() > 10) {
                System.err.println("Error: too many player owned shops featured! Refunding additional players.");
                Set<String> names = featuredList.keySet();
                for(int i = 10; i < names.size(); i++) {
                    PlayerShop shop = shops.get(names.toArray()[i]);

                    if(shop == null)
                        continue;

                    int hours = getHours(shop.featuredTime - System.currentTimeMillis());
                    if(hours < 0)
                        continue;

                    shop.featuredTime = 0;
                    shop.addSale(-2, hours, FEATURED_PRICE);
                }
            }
            for (String user : featuredList.keySet()) {
                PlayerShop shop = shops.get(user);
                if(shop == null)
                    continue;

                if(shop.featuredTime > 0 && shop.featuredTime < System.currentTimeMillis())
                    shop.featuredTime = 0;
            }
            timer.reset();
        }
    }
}
