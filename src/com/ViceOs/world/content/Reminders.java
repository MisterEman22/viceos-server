package com.ViceOs.world.content;

import com.ViceOs.util.Misc;
import com.ViceOs.util.Stopwatch;
import com.ViceOs.world.World;

/*
 * @author Bas
 * www.BoxRune.com
 */

public class Reminders {
	
	
    private static final int TIME = 900000; //10 minutes
	private static Stopwatch timer = new Stopwatch().reset();
	public static String currentMessage;
	
	/*
	 * Random Message Data
	 */
	private static final String[][] MESSAGE_DATA = { 
			//{"@blu@[SERVER]@bla@ Vote in all 5 sites to receive 5 vote scrolls and 5m cash ::vote!"},
			{"@blu@[<img=10>SERVER]@bla@ Join 'Help' CC For Help/Tips!"},
			{"@blu@[<img=10>SERVER]@bla@ Do ::players to see who's online!"},
			{"@blu@[<img=10>SERVER]@bla@ Donate to help the server grow! ::donate"},
			{"@blu@[<img=10>SERVER]@bla@ Use the command ::drop npcname for drop tables"},
			{"@blu@[<img=10>SERVER]@bla@ Use the ::help command to ask staff for help"},
			{"@blu@[<img=10>SERVER]@bla@ Make sure to read the forums for information @ ::forum"},
			{"@blu@[<img=10>SERVER]@bla@ Remember to spread the word and invite your friends to play!"},
			{"@blu@[<img=10>SERVER]@bla@ Do ::players to see who's online!"},
			{"@blu@[<img=10>SERVER]@bla@ Donate to help the server grow! ::donate"},
			{"@blu@[<img=10>SERVER]@bla@ Do ::commands to find a list of commands"},
			{"@blu@[<img=10>SERVER]@bla@ Donate to help the server grow! ::donate"},
			{"@blu@[<img=10>SERVER]@bla@ Do ::players to see who's online!"},
			{"@blu@[<img=10>SERVER]@bla@ Toggle your client settings to your preference in the wrench tab!"},
			{"@blu@[<img=10>SERVER]@bla@ Register and post on the forums to keep them active! ::Forum"},
			{"@blu@[<img=10>SERVER]@bla@ Donate to help the server grow! ::donate"},
			{"@blu@[<img=10>SERVER]@bla@ Did you know you can get benefits to make videos? PM Special!"},
			{"@blu@[<img=10>SERVER]@bla@ Do ::players to see who's online!"},
			
		
	};

	/*
	 * Sequence called in world.java
	 * Handles the main method
	 * Grabs random message and announces it
	 */
	public static void sequence() {
		if(timer.elapsed(TIME)) {
			timer.reset();
			{
				
			currentMessage = MESSAGE_DATA[Misc.getRandom(MESSAGE_DATA.length - 1)][0];
			World.sendMessage(currentMessage);
			World.savePlayers();
					
				}
				
			World.savePlayers();
			}
		

          }

}