package com.ViceOs.world.content;

import com.ViceOs.util.Misc;
import com.ViceOs.world.World;
import com.ViceOs.world.entity.impl.player.Player;

/*
 * @author BoxRune - BoxRune rsps
 */
public class TriviaBot {
	
	public static final int TIMER = 1800; //1800
	public static int botTimer = TIMER;
	
	public static int answerCount;
	public static String firstPlace;
	public static String secondPlace;
	public static String thirdPlace;

	//public static List<String> attempts = new ArrayList<>(); 

	public static void sequence() {
		
		if(botTimer > 0)
			botTimer--;
		if(botTimer <= 0) {
			botTimer = TIMER;
			didSend = false;
			askQuestion();
		}
	}
	
	public static void attemptAnswer(Player p, String attempt) {
		
		if (!currentQuestion.equals("") && attempt.replaceAll("_", " ").equalsIgnoreCase(currentAnswer)) {
			
			if (answerCount == 0) {
				answerCount++;
				p.getPointsHandler().incrementTriviaPoints(10);
				p.getPacketSender().sendMessage("You Recieved 10 Particiaption points");
				p.getPointsHandler().refreshPanel();
				firstPlace = p.getUsername();
				return;
			}
			if (answerCount == 1) {
				if (p.getUsername().equalsIgnoreCase(firstPlace)) {
					p.getPacketSender().sendMessage("Already answered");
					return;
				}
				answerCount++;
				p.getPointsHandler().incrementTriviaPoints(6);
				p.getPacketSender().sendMessage("You Recieved 6 Particiaptions points.");
				p.getPointsHandler().refreshPanel();
				secondPlace = p.getUsername();
				return;
			
			}
			if (answerCount == 2) {
				if (p.getUsername().equalsIgnoreCase(firstPlace) || p.getUsername().equalsIgnoreCase(secondPlace)) {
					p.getPacketSender().sendMessage("Already answered");
					return;
				}
				p.getPointsHandler().incrementTriviaPoints(4);
				p.getPacketSender().sendMessage("You Recieved 4 Participation points.");
				p.getPointsHandler().refreshPanel();
				thirdPlace = p.getUsername();
				World.sendMessage("<col=055fb5>[<img=16>]Trivia bot:@bla@[" +firstPlace+"<col=268a05> (10 pts)@bla@] @bla@[" +secondPlace+"<col=268a05> (6 pts)@bla@] [" +thirdPlace+"<col=268a05> (4 pts)@bla@]");
				//String[] s = Arrays.asList(attempts);
				//World.sendMessage("@blu@[TRIVIA] @red@Failed attempts: "+s);
				currentQuestion = "";
				didSend = false;
				botTimer = TIMER;
				answerCount = 0;
				return;
			}
		} else {
			if(attempt.contains("question") || attempt.contains("repeat")){
				p.getPacketSender().sendMessage("<col=055fb5>"+(currentQuestion));
				return;
			}

			//attempts.add(attempt); // need to add a filter for bad strings (advs, curses)
			p.getPacketSender().sendMessage("@red@ Wrong answer! Try again!");
			p.getPacketSender().sendMessage("<col=055fb5>"+(currentQuestion));
			return;
		}
		
	}
	
	public static boolean acceptingQuestion() {
		return !currentQuestion.equals("");
	}
	
	private static void askQuestion() {
		for (int i = 0; i < TRIVIA_DATA.length; i++) {
			if (Misc.getRandom(TRIVIA_DATA.length - 1) == i) {
				if(!didSend) {
					didSend = true;
				currentQuestion = TRIVIA_DATA[i][0];
				currentAnswer = TRIVIA_DATA[i][1];
				World.sendMessage(currentQuestion);
				
				
				}
			}
		}
	}
	
	public static boolean didSend = false;
	
	private static final String[][] TRIVIA_DATA = {
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ htgnerts", "strength"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ lassyba", "abyssal"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ sOeciV", "ViceOs"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ epipwolb", "blowpipe"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ tabmoc", "combat"},

		//{"<col=055fb5>[<img=16>]Trivia bot:@bla@ What rank has a silver crown?", "Moderator"},
		//{"<col=055fb5>[<img=16>]Trivia bot:@bla@ What rank has a purple crown?", "Developer"},
		//{"<col=055fb5>[<img=16>]Trivia bot:@bla@ What rank has a red crown?", "Owner"},
		//{"<col=055fb5>[<img=16>]Trivia bot:@bla@ What rank has a gold crown?", "Administrator"},
		//{"<col=055fb5>[<img=16>]Trivia bot:@bla@ What is the max exp. in a skill?", "1B"},
		//{"<col=055fb5>[<img=16>]Trivia bot:@bla@ How much exp. do you need for level 99?", "13M"},
		//{"<col=055fb5>[<img=16>]Trivia bot:@bla@ How much exp. do you need for Dungeonering level 120 on Runescape?", "104M"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ What has a foot on each side and one in the middle?", "Yardstick"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ What city is the most populated city on earth?", "Tokyo"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ What is the biggest manmade structure on earth?", "The Great Wall"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ What goes up and down without moving? ", "Stairs"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ What belongs to you but others use it more than you do?", "Name"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ What goes up white and comes down yellow? ", "Egg"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ How many bones are there in an adult human body?", "206"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ What is the deadliest insect on the planet?", "Mosquito"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ You answer me, although I never ask you questions. What am I?", "Telephone"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ You can't keep this until you have given it.", "Promise"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ What has four eyes but cannot see?", "Mississippi"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ What grows when it eats, but dies when it drinks?", "Fire"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ What grows up while growing down?", "Goose"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ What has no beginning, end or middle and touches every continent?", "Ocean"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ What has four legs but can't walk?", "Table"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ Which NPC is wearing a 2H-Sword and a Dragon SQ Shield?", "Vannaka"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ What is the baby spider called?", "Spiderling"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ What Attack level do you need to wield an Abyssal Whip?", "70"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ What is the owner's name?", "Special"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ The more you take, the more you leave, who am I?", "Footsteps"},

		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ Take off my skin -- I won't cry, but you will! What am I?", "Onion"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ What is the max total level?", "2475"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ How many achievements are there in total?", "100"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ In this skill you can brew potions, what is it?	", "Herblore"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ Which skill consists of players making their own houses", "Construction"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ In this skill you can use tanned hides, what is it?", "Crafting"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ In which skill can players defeat special monsters that give rare drops?", "Slayer"},

		{"<col=6055fb5>[<img=16>]Trivia bot:@bla@ Guess a number 1-10?", "5"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ Guess a number 1-10?", "3"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ Guess a number 1-10?", "9"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ Guess a number 1-10?", "9"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ Guess a number 1-10?", "3"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ Guess a number 1-10?", "5"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ Guess a number 1-20?", "5"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ Guess a number 1-20?", "14"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ Guess a number 1-20?", "16"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ Guess a number 1-20?", "12"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ Guess a number 1-20?", "8"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ Guess a number 1-20?", "13"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ Guess a number 1-5?", "5"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ Guess a number 1-5?", "4"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ Guess a number 1-5?", "1"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ Guess a number 1-5?", "4"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ Guess a number 1-5?", "5"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ Guess a number 1-5?", "2"},
		
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ type the following ::anwser ViceOsftw", "ViceOsftw"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ type the following ::anwser iloveyousomuch143teamo", "iloveyousomuch143teamo"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ type the following ::anwser putsomemoneyinthewellman", "putsomemoneyinthewellman"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ type the following ::anwser loski4893dhncbv7539", "loski4893dhncbv7539"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ type the following ::anwser whostolethecookiefromthejar", "whostolethecookiefromthejar"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ type the following ::anwser ishitmypantsloljkwtfisthisanswer", "ishitmypantsloljkwtfisthisanswer"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ type the following ::anwser ihaveaverytwistedmind", "ihaveaverytwistedmind"},
		{"<col=055fb5>[<img=16>]Trivia bot:@bla@ type the following ::anwser modmarkwouldlovethisgame", "modmarkwouldlovethisgame"}
	};
	
	public static String currentQuestion;
	private static String currentAnswer;
}