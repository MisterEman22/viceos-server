package com.ViceOs.world.content.combat.pvp;

import com.ViceOs.util.Misc;
import com.ViceOs.world.World;
import com.ViceOs.world.entity.impl.player.Player;
public abstract class Killstreak {
	public static void kill(Player player, Player other, int killstreak) {
		int pkPoints = 1;
		int otherKillstreak = other.getPlayerKillingAttributes().getPlayerKillStreak();
		if (otherKillstreak >= 5) {
			pkPoints += 2;
			World.sendMessage("<icon=2><shad=ff0000>" + other.getUsername() + "'s killstreak of "
					+ other.getPlayerKillingAttributes().getPlayerKillStreak() + " has been ended by "
					+ player.getUsername() + "!");
		}
		if (killstreak % 5 == 0) {
			World.sendMessage("<icon=2><shad=ff0000>" + player.getUsername() + " is currently on a " + killstreak + " killstreak!");
			pkPoints += 1;
		}
		player.getPointsHandler().setPkPoints(pkPoints, true);
		player.getPacketSender().sendMessage("You've received " + pkPoints + " PK Point(s)!");
	}
}
