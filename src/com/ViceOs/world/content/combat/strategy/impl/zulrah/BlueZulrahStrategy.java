package com.ViceOs.world.content.combat.strategy.impl.zulrah;

import com.ViceOs.util.Misc;
import com.ViceOs.world.content.combat.CombatType;
import com.ViceOs.world.entity.impl.Character;
import com.ViceOs.world.entity.impl.player.Player;

/**
 * Created by Greg on 01/05/2017.
 */
public class BlueZulrahStrategy extends ZulrahStrategy {

    @Override
    public void action(Character entity, Character victim) {
        Player player = (Player) victim;
        if (player.getZulrah().phase == 2 && !player.getZulrah().cloudsSpawned) {
            player.getZulrah().spawnToxicClouds(entity);
        } else {
            int rand = Misc.getRandom(15);
            switch (rand) {
                case 0:
                    handleSpotChange(player, entity);
                    break;
                case 1:
                    player.getZulrah().spawnSnake(entity);
                    break;
                default:
                    defaultAttack(entity, victim, Misc.random(1) == 0 ? CombatType.RANGED : CombatType.MAGIC);
                    break;
            }
        }
    }

    @Override
    public void onDeath(Player player) {
        player.getZulrah().handleSpawn(this);
    }
}
