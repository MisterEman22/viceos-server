package com.ViceOs.world.content.combat.strategy.impl.zulrah;

import com.ViceOs.model.Animation;
import com.ViceOs.util.Misc;
import com.ViceOs.world.content.combat.CombatContainer;
import com.ViceOs.world.content.combat.CombatHitTask;
import com.ViceOs.world.content.combat.CombatType;
import com.ViceOs.world.entity.impl.Character;
import com.ViceOs.world.entity.impl.player.Player;

/**
 * Created by Greg on 01/05/2017.
 */
public class RedZulrahStrategy extends ZulrahStrategy {

    @Override
    public void action(Character entity, Character victim) {
        Player player = (Player) victim;
        if(!player.getZulrah().spawningEnded) {
            player.getZulrah().spawnToxicClouds(entity);
        } else {
            int rand = Misc.getRandom(10);
            switch (rand) {
                case 0:
                    handleSpotChange(player, entity);
                    break;
                default:
                    entity.performAnimation(new Animation(5069));
                    new CombatHitTask(entity.getCombatBuilder(), new CombatContainer(entity, victim, 1, CombatType.MELEE, true)).handleAttack();
                    break;
            }
        }
    }

    @Override
    public void onDeath(Player player) {
        player.getZulrah().handleSpawn(this);
    }
}
