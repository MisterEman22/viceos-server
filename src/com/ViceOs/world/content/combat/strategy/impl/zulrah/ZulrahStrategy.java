package com.ViceOs.world.content.combat.strategy.impl.zulrah;

import com.ViceOs.engine.task.Task;
import com.ViceOs.engine.task.TaskManager;
import com.ViceOs.model.Animation;
import com.ViceOs.model.Projectile;
import com.ViceOs.world.World;
import com.ViceOs.world.content.combat.CombatContainer;
import com.ViceOs.world.content.combat.CombatHitTask;
import com.ViceOs.world.content.combat.CombatType;
import com.ViceOs.world.content.combat.strategy.CombatStrategy;
import com.ViceOs.world.entity.impl.Character;
import com.ViceOs.world.entity.impl.player.Player;

/**
 * Created by Greg on 01/05/2017.
 */
public class ZulrahStrategy implements CombatStrategy {

    public boolean isRespawning = false;
    public boolean stopEvent = false;
    public static Animation dive = new Animation(5072);

    public void onDeath(Player player) {
    }

    public void action(Character entity, Character victim) {
    }

    public void defaultAttack(Character entity, Character victim, CombatType type) {
        TaskManager.submit(new Task(2, entity, true) {
            int tick;

            @Override
            public void execute() {
                switch (tick) {
                    case 0:
                        entity.performAnimation(new Animation(5069));
                        break;
                    case 2:
                        new Projectile(entity, victim, type == CombatType.RANGED ? 1044 : 1046, 44, 1, 43, 43, 0).sendProjectile();
                        new CombatHitTask(entity.getCombatBuilder(), new CombatContainer(entity, victim, 1, type, true)).handleAttack();
                        break;
                }
                tick++;
            }
        });
    }

    public void handleSpotChange(Player player, Character entity) {
        TaskManager.submit(new Task(1, true) {
            int tick;

            @Override
            public void execute() {
                if (tick == 0) {
                    player.getZulrah().entityConstitution = entity.getConstitution();
                    player.getZulrah().getNpc().performAnimation(dive);
                    TaskManager.submit(new Task(2, player.getZulrah().getNpc(), false) {
                        @Override
                        public void execute() {
                            World.deregister(player.getZulrah().getNpc());
                            player.getZulrah().despawnCloudTargets();
                            stop();
                        }
                    });
                    isRespawning = true;
                }
                if (tick == 3) {
                    isRespawning = false;
                    onDeath(player);
                    stop();
                }
                tick++;
            }
        });
    }

    @Override
    public boolean canAttack(Character entity, Character victim) {
        return true;
    }

    @Override
    public CombatContainer attack(Character entity, Character victim) {
        return null;
    }

    @Override
    public boolean customContainerAttack(Character entity, Character victim) {
        if(!victim.isPlayer())
            return true;

        Player player = (Player) victim;
        if (isRespawning)
            return true;
        if (player.getZulrah().isSpawning)
            return true;

        action(entity, victim);
        return true;
    }

    @Override
    public int attackDelay(Character entity) {
        return 4;
    }

    @Override
    public int attackDistance(Character entity) {
        return 50;
    }

    @Override
    public CombatType getCombatType() {
        return CombatType.MIXED;
    }
}
