package com.ViceOs.world.content.ffa;

import java.util.*;

import com.ViceOs.GameSettings;
import com.ViceOs.model.Item;
import com.ViceOs.model.Locations;
import com.ViceOs.model.Position;
import com.ViceOs.model.Skill;
import com.ViceOs.model.container.impl.Equipment;
import com.ViceOs.model.definitions.WeaponInterfaces;
import com.ViceOs.net.packet.impl.EquipPacketListener;
import com.ViceOs.world.World;
import com.ViceOs.world.content.ffa.impl.Lbox;
import com.ViceOs.world.content.ffa.impl.Mbox;
import com.ViceOs.world.content.skill.SkillManager;
import com.ViceOs.world.content.skill.SkillManager.Skills;
import com.ViceOs.world.entity.impl.player.Player;


public abstract class FreeForAll {

	private static final int WAIT_TIME = 150;
	private static final int MIN_PLAYERS = 2;
	private static final Position ARENA_MIN_POS = new Position(3144, 5065, 0);
	private static final Position ARENA_MAX_POS = new Position(3164, 5086, 0);
	private static final Position LOBBY_POS = new Position(2223,3799,0);
	private static final Map<String, Class<? extends FreeForAll>> ffaTypes = getFfaTypes();

	private static FreeForAll instance;

	protected List<Player> players;
	protected int waitTimer;

	public FreeForAll() {
		this.players = new ArrayList<>();
		this.waitTimer = WAIT_TIME;
	}

	private static Map<String, Class<? extends FreeForAll>> getFfaTypes() {
		Map<String, Class<? extends FreeForAll>> types = new HashMap<>();
		types.put("mbox", Mbox.class);
		types.put("lbox", Lbox.class);
		return types;
	}

	private void saveOldStats(Player player) {
		Skills currentSkills = player.getSkillManager().getSkills();
		player.oldSkillLevels = Arrays.copyOf(currentSkills.level, currentSkills.level.length);
		player.oldSkillXP = Arrays.copyOf(currentSkills.experience, currentSkills.experience.length);
		player.oldSkillMaxLevels = Arrays.copyOf(currentSkills.maxLevel, currentSkills.maxLevel.length);
	}

	public static boolean isGameStarted() {
		return instance != null && instance.isStarted();
	}

	public static boolean isEventStarted() {
		return instance != null;
	}

	public boolean isStarted() {
		return this.waitTimer <= 0;
	}

	protected void setSkills(Player p) {
		SkillManager sm = p.getSkillManager();
		sm.setMaxLevel(Skill.ATTACK, 99);
		sm.setMaxLevel(Skill.STRENGTH, 99);
		sm.setMaxLevel(Skill.DEFENCE, 99);
		sm.setMaxLevel(Skill.RANGED, 99);
		sm.setMaxLevel(Skill.MAGIC, 99);
		sm.setMaxLevel(Skill.CONSTITUTION, 990);
		sm.setMaxLevel(Skill.PRAYER, 990);


		for (Skill skill : Skill.values()) {
			p.getSkillManager().setCurrentLevel(skill, p.getSkillManager().getMaxLevel(skill))
					.setExperience(skill,
							SkillManager.getExperienceForLevel(p.getSkillManager().getMaxLevel(skill)));
		}
		updateSkills(p);
	}

	protected abstract void setItems(Player p);


	private void startGame() {
		if (players.size() < MIN_PLAYERS) {
			this.waitTimer = WAIT_TIME;
			return;
		}
		for (Player player : players) {
			player.getPA().closeAllWindows();
			saveOldStats(player);
			setSkills(player);
			setItems(player);
			player.moveTo(Position.random(ARENA_MIN_POS, ARENA_MAX_POS));
			player.getPacketSender().sendInterfaceRemoval();
		}
	}

	public static void sequence() {
		if (instance != null) {
			if (instance.isStarted()) {
				instance.gameSequence();
			} else {
				instance.lobbySequence();
			}
		}
	}

	private void lobbySequence() {
		this.waitTimer--;
		if (this.waitTimer <= 0) {
			this.startGame();
		} else {
			this.updateGameInterface();
		}
	}

	private void gameSequence() {
		if (players.size() <= 1) {
			List<Player> playersCopy = new ArrayList(players);
			for (Player p : playersCopy) {
				World.sendMessage("@or2@[FFA] " + p.getUsername() + " has won the FFA.");
				leaveGame(p);
			}
			instance = null;
		}
	}
	
	private void updateGameInterface() {
		for (Player p : players) {
			if(p == null)
				continue;
			if(p.getWalkableInterfaceId() != 21005)
				p.sendParallellInterfaceVisibility(21005,true);
			p.getPacketSender().sendString(21006, "Starting in: " + waitTimer + "");
			p.getPacketSender().sendString(21007, "Players Ready: " + players.size() + "");
			p.getPacketSender().sendString(21008, "");
			p.getPacketSender().sendString(21009, "");
		}
	}
	
	public static void startEvent(String type) {
		type = type.toLowerCase();
		if(instance == null && isValidType(type)) {
			try {
				instance = ffaTypes.get(type).newInstance();
				World.sendMessage("@or2@[FFA] A free-for-all event has been started. Use ::ffa to join.");
			} catch (Exception e) {
				System.out.println("Error starting free-for-all of type " + type);
			}
		}
	}

	public static boolean isValidType(String type) {
		type = type.toLowerCase();
		return ffaTypes.containsKey(type);
	}

	public static boolean tryJoin(Player p) {
		if (instance == null) {
			p.getPacketSender().sendMessage("There is no free-for-all lobby to join currently.");
			return false;
		}
		if (isGameStarted()) {
			p.getPacketSender().sendMessage("The free-for-all game has already started.");
			return false;
		}
		if (!p.getInventory().isEmpty()) {
			p.getPacketSender().sendMessage("Your inventory must be empty to join the free-for-all.");
			return false;
		}
		if (!p.getEquipment().isEmpty()) {
			p.getPacketSender().sendMessage("Your equipment must be empty to join the free-for-all.");
			return false;
		}
		if (p.getSummoning().getFamiliar() != null) {
			p.getPacketSender().sendMessage("You must dismiss your familiar to join the free-for-all.");
			return false;
		}
		if (p.getLocation() != Locations.Location.DEFAULT) {
			p.getPacketSender().sendMessage("You cannot join the free-for-all from this location.");
			return false;
		}

		p.getPA().closeAllWindows();
		p.moveTo(LOBBY_POS);
		instance.players.add(p);
		return true;
	}

	private void handleRewards(Player p) {
		int points;
		switch (this.players.size()) {
			case 1:
				points = 45;
				break;
			default:
				points = 10;
				break;
		}
		p.getPointsHandler().setTriviaPoints(points, true);
		p.getPacketSender().sendMessage("Thanks for playing FFA. You've been rewarded " + points + " participation points.");
	}

	public static void updateSkills(Player player) {
		player.getSkillManager().updateSkill(Skill.ATTACK);
		player.getSkillManager().updateSkill(Skill.AGILITY);
		player.getSkillManager().updateSkill(Skill.CONSTITUTION);
		player.getSkillManager().updateSkill(Skill.CONSTRUCTION);
		player.getSkillManager().updateSkill(Skill.COOKING);
		player.getSkillManager().updateSkill(Skill.CRAFTING);
		player.getSkillManager().updateSkill(Skill.DEFENCE);
		player.getSkillManager().updateSkill(Skill.DUNGEONEERING);
		player.getSkillManager().updateSkill(Skill.FARMING);
		player.getSkillManager().updateSkill(Skill.FIREMAKING);
		player.getSkillManager().updateSkill(Skill.FISHING);
		player.getSkillManager().updateSkill(Skill.FLETCHING);
		player.getSkillManager().updateSkill(Skill.HERBLORE);
		player.getSkillManager().updateSkill(Skill.HUNTER);
		player.getSkillManager().updateSkill(Skill.MAGIC);
		player.getSkillManager().updateSkill(Skill.MINING);
		player.getSkillManager().updateSkill(Skill.PRAYER);
		player.getSkillManager().updateSkill(Skill.RANGED);
		player.getSkillManager().updateSkill(Skill.RUNECRAFTING);
		player.getSkillManager().updateSkill(Skill.SLAYER);
		player.getSkillManager().updateSkill(Skill.SMITHING);
		player.getSkillManager().updateSkill(Skill.STRENGTH);
		player.getSkillManager().updateSkill(Skill.SUMMONING);
		player.getSkillManager().updateSkill(Skill.THIEVING);
		player.getSkillManager().updateSkill(Skill.WOODCUTTING);
	}

	public static void leaveGame(Player p) {
		if (instance == null || !instance.players.contains(p)) {
			p.moveTo(GameSettings.DEFAULT_POSITION);
			return;
		}
		if (isGameStarted()) {
			p.getSkillManager().getSkills().level = p.oldSkillLevels;
			p.getSkillManager().getSkills().experience = p.oldSkillXP;
			p.getSkillManager().getSkills().maxLevel = p.oldSkillMaxLevels;
			updateSkills(p);
			p.getEquipment().deleteAll();
			p.getInventory().deleteAll();
			instance.handleRewards(p);
			p.setSpecialPercentage(100);
			EquipPacketListener.resetWeapon(p);
		}
		instance.players.remove(p);
		p.moveTo(GameSettings.DEFAULT_POSITION);
		p.getPacketSender().sendInterfaceRemoval();
		p.getPacketSender().sendInteractionOption("null", 2, true);
	}
}
