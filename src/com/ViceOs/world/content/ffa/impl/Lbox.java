package com.ViceOs.world.content.ffa.impl;

import com.ViceOs.world.content.ffa.FreeForAll;
import com.ViceOs.world.entity.impl.player.Player;

/**
 * Author: Nick (MisterEman22)
 * Created: 6/27/2017
 */
public class Lbox extends FreeForAll {
    @Override
    protected void setItems(Player p) {
        p.getInventory().add(15501, 16);
        p.getInventory().add(391, 12);
    }
}
