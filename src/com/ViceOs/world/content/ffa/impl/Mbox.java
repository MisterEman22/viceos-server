package com.ViceOs.world.content.ffa.impl;

import com.ViceOs.world.content.ffa.FreeForAll;
import com.ViceOs.world.entity.impl.player.Player;

/**
 * Author: Nick (MisterEman22)
 * Created: 6/20/2017
 */
public class Mbox extends FreeForAll {
    @Override
    protected void setItems(Player p) {
        p.getInventory().add(6199, 16);
        p.getInventory().add(15501, 1);
        p.getInventory().add(391, 11);
    }
}
