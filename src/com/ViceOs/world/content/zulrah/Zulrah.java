package com.ViceOs.world.content.zulrah;

import com.ViceOs.GameSettings;
import com.ViceOs.engine.task.Task;
import com.ViceOs.engine.task.TaskManager;
import com.ViceOs.model.*;
import com.ViceOs.util.Misc;
import com.ViceOs.world.World;
import com.ViceOs.world.content.CustomObjects;
import com.ViceOs.world.content.combat.strategy.impl.zulrah.BlueZulrahStrategy;
import com.ViceOs.world.content.combat.strategy.impl.zulrah.GreenZulrahStrategy;
import com.ViceOs.world.content.combat.strategy.impl.zulrah.RedZulrahStrategy;
import com.ViceOs.world.content.combat.strategy.impl.zulrah.ZulrahStrategy;
import com.ViceOs.world.entity.impl.Character;
import com.ViceOs.world.entity.impl.npc.NPC;
import com.ViceOs.world.entity.impl.player.Player;

/**
 * Created by Greg on 01/05/2017.
 */
public class Zulrah {

    public static Animation rise = new Animation(5073);
    Player player;
    public NPC ZULRAH;
    public Direction location = Direction.NORTH;
    public NPC CLOUD1, CLOUD2, CLOUD3, CLOUD4, CLOUD5, CLOUD6, CLOUD7, CLOUD8;
    public NPC[] cloudTargets = {CLOUD1, CLOUD2, CLOUD3, CLOUD4, CLOUD5, CLOUD6, CLOUD7, CLOUD8};
    private static final int SNAKELING = 2045;
    private static final int[][] cloudLocations = {{2263, 3076}, {2263, 3073}, {2263, 3070}, {2266, 3069}, {2269, 3069}, {2272, 3070}, {2273, 3073}, {2273, 3076}};
    private static final int[][] locations = {{2268, 3074}, {2277, 3074}, {2270, 3065}, {2259, 3070}};

    public boolean cloudsSpawned = false;
    public boolean isSpawning = false;
    public boolean spawningEnded = false;
    public int entityConstitution = 0;
    public int phase = 1;


    public void reset() {
        despawnCloudTargets();
        location = Direction.NORTH;
        cloudsSpawned = false;
        isSpawning = false;
        spawningEnded = false;
        entityConstitution = 0;
        phase = 1;
        if (ZULRAH != null && World.getNpcs().contains(ZULRAH))
            World.deregister(ZULRAH);
        setNpc(null);
        World.getNpcs().forEach(n -> n.removeInstancedNpcs(Locations.Location.ZULRAH, player.getIndex() * 4));
    }

    public void handleSpawn(ZulrahStrategy type) {
        if (type instanceof GreenZulrahStrategy) {
            switch (phase) {
                case 1:
                    player.getZulrah().cloudsSpawned = false;
                    ZulrahGreen.spawn(player, 2, entityConstitution);
                    break;
                case 2:
                    ZulrahRed.spawn(player, entityConstitution);
                    break;
                case 3:
                    ZulrahBlue.spawn(player, 2, entityConstitution);
                    break;
                case 4:
                    ZulrahGreen.spawn(player, 5, entityConstitution);
                    break;
                case 5:
                    ZulrahBlue.spawn(player, 3, entityConstitution);
                    break;
                case 6:
                    ZulrahBlue.spawn(player, 4, entityConstitution);
                    break;
            }
        } else if (type instanceof BlueZulrahStrategy) {
            switch (phase) {
                case 1:
                    ZulrahGreen.spawn(player, 3, entityConstitution);
                    break;
                case 2:
                    ZulrahGreen.spawn(player, 4, entityConstitution);
                    break;
                case 3:
                    ZulrahGreen.spawn(player, 6, entityConstitution);
                    break;
                case 4:
                    ZulrahGreen.spawn(player, 1, entityConstitution);
                    break;
            }
        } else if (type instanceof RedZulrahStrategy) {
            ZulrahBlue.spawn(player, 1, entityConstitution);
        }
    }

    public Zulrah(Player player) {
        this.player = player;
    }

    public NPC getNpc() {
        return ZULRAH;
    }

    public void setNpc(NPC npc) {
        this.ZULRAH = npc;
    }

    private void spawnCloudTargets() {
        for (int i = 0; i < cloudTargets.length; i++) {
            cloudTargets[i] = new NPC(1, new Position(cloudLocations[i][0], cloudLocations[i][1], player.getIndex() * 4));
            World.register(cloudTargets[i]);
        }
        cloudsSpawned = true;
    }

    public void despawnCloudTargets() {
        if (cloudsSpawned)
            for (int i = 0; i < cloudTargets.length; i++)
                World.deregister(cloudTargets[i]);
        cloudsSpawned = false;
    }

    public void spawnSnake(Character zulrah) {
        isSpawning = true;
        int rand = Misc.random(cloudTargets.length - 1);
        if (!cloudsSpawned)
            spawnCloudTargets();

        TaskManager.submit(new Task(1, true) {
            int tick;

            @Override
            public void execute() {
                if (zulrah.getConstitution() < 1 || zulrah == null)
                    stop();

                switch (tick) {
                    case 0:
                        zulrah.getCombatBuilder().attack(cloudTargets[rand]);
                        break;
                    case 2:
                        zulrah.performAnimation(new Animation(5069));
                        new Projectile(zulrah, cloudTargets[rand], 1047, 44, 1, 43, 0, 0).sendProjectile();
                        break;
                    case 4:
                        NPC snake = new NPC(SNAKELING, new Position(cloudLocations[rand][0], cloudLocations[rand][1], player.getIndex() * 4));
                        World.register(snake);
                        snake.getCombatBuilder().attack(player);
                        break;
                    case 6:
                        isSpawning = false;
                        zulrah.getCombatBuilder().attack(player);
                        break;
                }
                tick++;
            }
        });
    }

    public Position getPosition(int z) {
        int index = 0;
        switch (location) {
            case EAST:
                index = 1;
                break;
            case SOUTH:
                index = 2;
                break;
            case WEST:
                index = 3;
                break;
        }
        return new Position(locations[index][0], locations[index][1], z);
    }

    public void spawnToxicClouds(Character zulrah) {
        if (!cloudsSpawned)
            spawnCloudTargets();
        isSpawning = true;
        player.setCloudsSpawned(true);
        TaskManager.submit(new Task(1, true) {
            int tick;

            @Override
            public void execute() {
                if (zulrah.getConstitution() < 1 || zulrah == null)
                    stop();

                int start = 1;
                int end = 8;
                switch (location) {
                    case SOUTH:
                        start = 2;
                        end = 6;
                        break;
                    case EAST:
                        start = 5;
                        end = 8;
                        break;
                    case WEST:
                        start = 0;
                        end = 3;
                        break;
                }
                for (int i = start; i < end; i += 2) {
                    int t = i * 2;
                    if (tick == t)
                        zulrah.getCombatBuilder().attack(cloudTargets[i]);

                    if (tick == t + 2) {
                        zulrah.performAnimation(new Animation(5069));
                        new Projectile(zulrah, cloudTargets[i], 1045, 44, 1, 43, 0, 0).sendProjectile();
                    }

                    if (tick == t + 3) {
                        CustomObjects.zulrahToxicClouds(new GameObject(11700, cloudTargets[i].getPosition().copy().add(-1, -1)), player, 80);
                        CustomObjects.zulrahToxicClouds(new GameObject(11700, cloudTargets[i + 1].getPosition().copy().add(-1, -1)), player, 80);
                    }

                    if (tick == end * 2) {
                        isSpawning = false;
                        spawningEnded = true;
                        zulrah.getCombatBuilder().attack(player);
                        stop();
                    }
                }
                if (tick >= 34)
                    stop();
                tick++;
            }
        });
    }

    public void rise() {
        getNpc().performAnimation(rise);
    }

    public void onDeath() {
        World.getNpcs().forEach(n -> n.removeInstancedNpcs(Locations.Location.ZULRAH, player.getPosition().getZ()));
        TaskManager.submit(new Task(8, player, true) {
            int tick = 30;

            @Override
            public void execute() {
                if (player == null || player.getLocation() != Locations.Location.ZULRAH) {
                    stop();
                    return;
                }

                player.getPacketSender().sendMessage("Teleporting home in... " + tick + " seconds.");

                if (tick <= 0) {
                    player.moveTo(GameSettings.DEFAULT_POSITION);
                    stop();
                    return;
                }


                tick -= 5;
            }
        });
    }
}
