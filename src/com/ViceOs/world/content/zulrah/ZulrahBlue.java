package com.ViceOs.world.content.zulrah;

import com.ViceOs.model.Direction;
import com.ViceOs.world.World;
import com.ViceOs.world.entity.impl.npc.NPC;
import com.ViceOs.world.entity.impl.player.Player;

/**
 * Created by Greg on 01/05/2017.
 */
public class ZulrahBlue {

    private static final Direction[] locations = {Direction.WEST, Direction.EAST, Direction.NORTH, Direction.NORTH};

    public static void spawn(Player player, int next, int constitution) {
        player.getZulrah().location = locations[next - 1];
        player.getZulrah().setNpc(new NPC(2042, player.getZulrah().getPosition(player.getIndex() * 4)));
        World.register(player.getZulrah().getNpc());
        player.getZulrah().rise();
        player.getZulrah().entityConstitution = 5000 - constitution;
        player.getZulrah().getNpc().setConstitution(5000 - player.getZulrah().entityConstitution);
        player.getZulrah().phase = next;
    }
}
