package com.ViceOs.world.content.zulrah;

import com.ViceOs.model.Direction;
import com.ViceOs.model.RegionInstance;
import com.ViceOs.world.World;
import com.ViceOs.world.entity.impl.npc.NPC;
import com.ViceOs.world.entity.impl.player.Player;

/**
 * Created by Greg on 01/05/2017.
 */
public class ZulrahGreen {
    private static final Direction[] locations = {Direction.NORTH, Direction.EAST, Direction.SOUTH, Direction.NORTH, Direction.WEST, Direction.EAST};

    public static void spawn(Player player, int next, int constitution) {
        if (next == 1)
            player.setRegionInstance(new RegionInstance(player, RegionInstance.RegionInstanceType.ZULRAHS_SHRINE));
        player.getZulrah().location = locations[next - 1];
        player.getZulrah().setNpc(new NPC(2043, player.getZulrah().getPosition(player.getIndex() * 4)));
        World.register(player.getZulrah().getNpc());
        player.getZulrah().rise();
        player.getZulrah().entityConstitution = 5000 - constitution;
        player.getZulrah().getNpc().setConstitution(5000 - player.getZulrah().entityConstitution);
        player.getZulrah().phase = next;
    }
}
