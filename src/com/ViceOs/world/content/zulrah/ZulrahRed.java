package com.ViceOs.world.content.zulrah;

import com.ViceOs.model.Direction;
import com.ViceOs.world.World;
import com.ViceOs.world.entity.impl.npc.NPC;
import com.ViceOs.world.entity.impl.player.Player;

/**
 * Created by Greg on 01/05/2017.
 */
public class ZulrahRed {

    public static void spawn(Player player, int constitution){
        player.getZulrah().location = Direction.NORTH;
        player.getZulrah().setNpc(new NPC(2044, player.getZulrah().getPosition(player.getIndex() * 4)));
        World.register(player.getZulrah().ZULRAH);
        player.getZulrah().rise();
        player.getZulrah().entityConstitution = 5000 - constitution;
        player.getZulrah().getNpc().setConstitution(5000 - player.getZulrah().entityConstitution);
    }
}
