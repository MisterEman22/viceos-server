package com.ViceOs.world.entity.impl.player;

import java.io.File;

import com.ViceOs.model.GameMode;
import com.ViceOs.model.container.impl.Bank;
import com.ViceOs.model.definitions.ItemDefinition;
import com.ViceOs.world.content.KillsTracker;
import com.ViceOs.world.content.StartScreen;
import com.ViceOs.world.content.grandexchange.GrandExchangeSlot;

/*
 * @Author Bas - Restoring BoxRune w/ this file
 */

public class EcoReset {

	public static void main(String[] args) {

		ItemDefinition.init();

		int count = 0;

		/*
		 * Loading character files
		 */
		for (File file : new File("data/saves/characters/").listFiles()) {
			Player player = new Player(null);
			player.setUsername(file.getName().substring(0, file.getName().length()-5));
		
			PlayerLoading.getResult(player, true); //comment out line 78-81 in playerloading.java for this
			
			/*
			 * Money pouch, inventory, equipment, and dung bound items
			 */
			player.setMoneyInPouch(0);
			player.getInventory().resetItems();
			player.getEquipment().resetItems();
			player.getMinigameAttributes().getDungeoneeringAttributes().setBoundItems(new int[5]);
			player.setGrandExchangeSlots(new GrandExchangeSlot[6]);
			
			/*
			 * Reset Bank
			 */
			for (Bank bank : player.getBanks()) {
				if (bank == null) {
					return;
				}
				bank.resetItems();
			}
			
			/*
			 * Clear pack yack / beast of burden
			 */
			if (player.getSummoning().getBeastOfBurden() != null) {
				player.getSummoning().getBeastOfBurden().resetItems();
			}
			
			/*
			 * Reset the points
			 */
			//leave loyalty points
			player.getPointsHandler().setPkPoints(0, false);
			player.getPointsHandler().setDonationPoints(player.getAmountDonated(), false);
			player.getPointsHandler().setDungeoneeringTokens(0, false);
			player.getPointsHandler().setCommendations(0, false);
			player.getPointsHandler().setPrestigePoints(0, false);
			//player.getPointsHandler().setTriviaPoints(0, false);
			//player.getPointsHandler().setSlayerPoints(0, false);
			//player.getPointsHandler().setVotingPoints(0, false);

			player.getPointsHandler().setCommendations(0, false);

			//StartScreen.sendStartPackItems(player, GameModes.NORMAL);
			int bossPoints = 0;
			for (KillsTracker.KillsEntry e : player.getKillsTracker()) {
				if (e.isBoss())
					bossPoints += e.getAmount();
			}
			player.setBossPoints(bossPoints);

			StartScreen.addStarterToInv(player, StartScreen.GameModes.NORMAL, false);

			if (player.getBossPoints() > 100) {
				System.out.println("Account flagged: " + player.getUsername() + " has " + player.getBossPoints() + " boss points");
			}
			if (player.getAmountDonated() > 50) {
				System.out.println("Account flagged: " + player.getUsername() + " has donated $" + player.getAmountDonated());
			}
			
			/*
			 * Save File
			 */
			PlayerSaving.save(player);
			count++;
		}
		System.out.println("Players reset: " + count);
	}
}
