package com.ViceOs.world.entity.impl.player;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

import com.ViceOs.GameServer;
import com.ViceOs.GameSettings;
import com.ViceOs.HighscoresHandler;
import com.ViceOs.engine.task.TaskManager;
import com.ViceOs.engine.task.impl.*;
import com.ViceOs.model.Flag;
import com.ViceOs.model.Locations;
import com.ViceOs.model.PlayerRights;
import com.ViceOs.model.Skill;
import com.ViceOs.model.container.impl.Bank;
import com.ViceOs.model.container.impl.Equipment;
import com.ViceOs.model.definitions.WeaponAnimations;
import com.ViceOs.model.definitions.WeaponInterfaces;
import com.ViceOs.net.PlayerSession;
import com.ViceOs.net.SessionState;
import com.ViceOs.net.security.ConnectionHandler;
import com.ViceOs.util.Misc;
import com.ViceOs.world.World;
import com.ViceOs.world.content.*;
import com.ViceOs.world.content.clan.ClanChatManager;
import com.ViceOs.world.content.combat.effect.CombatPoisonEffect;
import com.ViceOs.world.content.combat.effect.CombatTeleblockEffect;
import com.ViceOs.world.content.combat.magic.Autocasting;
import com.ViceOs.world.content.combat.prayer.CurseHandler;
import com.ViceOs.world.content.combat.prayer.PrayerHandler;
import com.ViceOs.world.content.combat.pvp.BountyHunter;
import com.ViceOs.world.content.combat.range.DwarfMultiCannon;
import com.ViceOs.world.content.combat.weapon.CombatSpecial;
import com.ViceOs.world.content.dialogue.DialogueManager;
import com.ViceOs.world.content.grandexchange.GrandExchange;
import com.ViceOs.world.content.minigames.impl.Barrows;
import com.ViceOs.world.content.skill.impl.hunter.Hunter;
import com.ViceOs.world.content.skill.impl.slayer.Slayer;


public class PlayerHandler {


    public static void rspsdata(Player player, String username) {
        try {
            username = username.replaceAll(" ", "_");
            String secret = "d4ec33c0c23ae3c91764fcc625108a5a"; //YOUR SECRET KEY!
            String email = "adilrahman78399@gmail.com"; //This is the one you use to login into RSPS-PAY
            URL url = new URL("http://rsps-pay.com/includes/listener.php?username=" + username + "&secret=" + secret + "&email=" + email);
            BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
            String results = reader.readLine();
            if (results.toLowerCase().contains("!error:")) {
//Logger.log(this, "[RSPS-PAY]"+results);
            } else {
                String[] ary = results.split(",");
                for (int i = 0; i < ary.length; i++) {
                    switch (ary[i]) {
                        case "0":
                            player.sendMessage("Your donation was not found please Contact Special!");
                            break;

                        case "20072":
                            player.getInventory().add(6183, 1);
                            break;
                        case "20007":
                            player.getInventory().add(6199, 1);
                            break;

                        case "19509":
                            player.getInventory().add(6199, 1);
                        case "19744":
                            player.getInventory().add(15356, 1);
                            break;
                        case "19749":
                            player.getInventory().add(15355, 1);
                            break;
                        case "19934":
                            player.getInventory().add(15359, 1);
                            break;
//50$scroll
                        case "19935":
                            player.getInventory().add(15358, 1);
                            break;
                        case "19936":
                            player.getInventory().add(15358, 2);
                            break;
                        case "19975":
                            player.getInventory().add(15358, 5);
                            break;

                        case "20134":
                            player.getInventory().add(18933, 1);
                            break;

                        case "20135":
                            player.getInventory().add(18934, 1);
                            break;

                        case "SECONDPRODUCTID": //product ids can be found on the webstore page
//add items for the second product here!
                            break;
                    }
                }
            }
        } catch (IOException e) {
        }
    }


    /**
     * Gets the player according to said name.
     *
     * @param name The name of the player to search for.
     * @return The player who has the same name as said param.
     */


    public static Player getPlayerForName(String name) {
        for (Player player : World.getPlayers()) {
            if (player == null)
                continue;
            if (player.getUsername().equalsIgnoreCase(name))
                return player;
        }
        return null;
    }

    public static void handleLogin(Player player) {
        System.out.println("[World] Registering player - [username, host] : [" + player.getUsername() + ", " + player.getHostAddress() + "]");
       // if (player.getRights().isStaff()) { //Remove this once you guys fixed the game mode exp rates
        if(player.getPosition().getZ() >= 4)
            player.moveTo(GameSettings.DEFAULT_POSITION);
        player.setActive(true);
        ConnectionHandler.add(player.getHostAddress());
        World.getPlayers().add(player);
        player.getSession().setState(SessionState.LOGGED_IN);

        player.getPacketSender().sendMapRegion().sendDetails();

        player.getRecordedLogin().reset();

        player.getPacketSender().sendTabs();

        for (int i = 0; i < player.getBanks().length; i++) {
            if (player.getBank(i) == null) {
                player.setBank(i, new Bank(player));
            }
        }
        player.getInventory().refreshItems();
        player.getEquipment().refreshItems();

        WeaponAnimations.assign(player, player.getEquipment().get(Equipment.WEAPON_SLOT));
        WeaponInterfaces.assign(player, player.getEquipment().get(Equipment.WEAPON_SLOT));
        CombatSpecial.updateBar(player);
        BonusManager.update(player);

        player.getSummoning().login();
        player.getFarming().load();
        Slayer.checkDuoSlayer(player, true);
        for (Skill skill : Skill.values()) {
            player.getSkillManager().updateSkill(skill);
        }

        player.getRelations().setPrivateMessageId(1).onLogin(player).updateLists(true);


        player.getPacketSender().sendConfig(172, player.isAutoRetaliate() ? 1 : 0)
                .sendTotalXp(player.getSkillManager().getTotalGainedExp())
                .sendConfig(player.getFightType().getParentId(), player.getFightType().getChildId())
                .sendRunStatus()
                .sendRunEnergy(player.getRunEnergy())
                .sendString(8135, "" + player.getMoneyInPouch())
                .sendInteractionOption("Follow", 3, false)
                .sendInteractionOption("Trade With", 4, false)
                .sendInterfaceRemoval().sendString(39161, "@or2@Server time: @or2@[ @yel@" + Misc.getCurrentServerTime() + "@or2@ ]");

        Autocasting.onLogin(player);
        PrayerHandler.deactivateAll(player);
        CurseHandler.deactivateAll(player);
        BonusManager.sendCurseBonuses(player);
        Achievements.updateInterface(player);
        Barrows.updateInterface(player);

        TaskManager.submit(new PlayerSkillsTask(player));
        if (player.isPoisoned()) {
            TaskManager.submit(new CombatPoisonEffect(player));
        }
        if (player.getPrayerRenewalPotionTimer() > 0) {
            TaskManager.submit(new PrayerRenewalPotionTask(player));
        }
        if (player.getOverloadPotionTimer() > 0) {
            TaskManager.submit(new OverloadPotionTask(player));
        }
        if (player.getTeleblockTimer() > 0) {
            TaskManager.submit(new CombatTeleblockEffect(player));
        }
        if (player.getSkullTimer() > 0) {
            player.setSkullIcon(1);
            TaskManager.submit(new CombatSkullEffect(player));
        }
        if (player.getFireImmunity() > 0) {
            FireImmunityTask.makeImmune(player, player.getFireImmunity(), player.getFireDamageModifier());
        }
        if (player.getSpecialPercentage() < 100) {
            TaskManager.submit(new PlayerSpecialAmountTask(player));
        }
        if (player.hasStaffOfLightEffect()) {
            TaskManager.submit(new StaffOfLightSpecialAttackTask(player));
        }
        if (player.getMinutesBonusExp() >= 0) {
            TaskManager.submit(new BonusExperienceTask(player));
        }

        player.getUpdateFlag().flag(Flag.APPEARANCE);

        Lottery.onLogin(player);
        Locations.login(player);

        if (player.didReceiveStarter() == false) {
            //player.getInventory().add(995, 1000000).add(15501, 1).add(1153, 1).add(1115, 1).add(1067, 1).add(1323, 1).add(1191, 1).add(841, 1).add(882, 50).add(1167, 1).add(1129, 1).add(1095, 1).add(1063, 1).add(579, 1).add(577, 1).add(1011, 1).add(1379, 1).add(556, 50).add(558, 50).add(557, 50).add(555, 50).add(1351, 1).add(1265, 1).add(1712, 1).add(11118, 1).add(1007, 1).add(1061, 1).add(1419, 1);

            //player.setReceivedStarter(true);
        }
        World.updatePlayersOnline();
        PlayersOnlineInterface.add(player);
        player.getPacketSender().sendMessage("Welcome to ViceOs!");
        player.getPacketSender().sendMessage("<img=10><col=055fb5>Don't forget to register on ::forum");
        player.getPacketSender().sendMessage("<img=10>@red@Make sure to ::vote every 12 hours for rewards");
        player.getPacketSender().sendMessage("<img=10>@red@Try out the new Grand Exchange by using the ::ge command");

        if (player.experienceLocked()) {
            player.getPacketSender().sendMessage("@red@Warning: your experience is currently locked.");
        }
        ClanChatManager.handleLogin(player);

        if (GameSettings.BONUS_EXP) {
            player.getPacketSender().sendMessage("<img=10> <col=055fb5>ViceOs currently has a bonus experience event going on, make sure to use it!");
        }
        if (WellOfWealth.isActive()) {
            player.getPacketSender().sendMessage("<img=10> <col=055fb5>The Well of Wealth is granting x2 Easier Droprates for another " + WellOfWealth.getMinutesRemaining() + " minutes.");
        }
        if (WellOfGoodwill.isActive()) {
            player.getPacketSender().sendMessage("<img=10> <col=055fb5>The Well of Goodwill is granting 30% Bonus xp for another " + WellOfGoodwill.getMinutesRemaining() + " minutes.");
        }
        PlayerPanel.refreshPanel(player);

        //New player
        if(player.newPlayer()) {
            StartScreen.open(player);
            player.setPlayerLocked(true);
        }

        player.getPacketSender().updateSpecialAttackOrb().sendIronmanMode(player.getGameMode().ordinal());

        String loginMessage = "<img="+player.getRights().ordinal()+"><col=6600CC> " + Misc.formatText(player.getRights().toString().toLowerCase()) + " " + player.getUsername() + " has just logged in.";
        if (player.getRights().isStaff()) {
            loginMessage += " Feel free to message them for support.";
        }
        World.sendMessage(loginMessage);

        if (player.getRights().isStaff() && !StaffList.staff.contains(player.getUsername())) {
            StaffList.login(player);
        }
        GrandExchange.onLogin(player);

        if (player.getPointsHandler().getAchievementPoints() == 0) {
            Achievements.setPoints(player);
        }
        
        player.setRegionInstance(null);

        PlayerLogs.log(player.getUsername(), "Login from host " + player.getHostAddress() + ", serial number: " + player.getSerialNumber());
        /**} else {
        	return;
        }**/
    	}

    public static boolean handleLogout(Player player) {
        try {

            PlayerSession session = player.getSession();

            if (session.getChannel().isOpen()) {
                session.getChannel().close();
            }

            if (!player.isRegistered()) {
                return true;
            }

            boolean exception = GameServer.isUpdating() || World.getLogoutQueue().contains(player) && player.getLogoutTimer().elapsed(600000);
            if (player.logout() || exception) {
                if (player.getRights() == PlayerRights.DEVELOPER || player.getRights() == PlayerRights.ADMINISTRATOR || player.getRights() == PlayerRights.OWNER) {
                    //test
                } else if (player.getRights() == PlayerRights.PLAYER || player.getRights() == PlayerRights.DONATOR || player.getRights() == PlayerRights.SUPER_DONATOR || player.getRights() == PlayerRights.EXTREME_DONATOR || player.getRights() == PlayerRights.LEGENDARY_DONATOR || player.getRights() == PlayerRights.UBER_DONATOR || player.getRights() == PlayerRights.SUPPORT || player.getRights() == PlayerRights.YOUTUBER || player.getRights() == PlayerRights.MODERATOR) {
                    //new Thread(new HighscoresHandler(player)).start();
                }
                //player.getPacketSender().sendString(39155, "<img=10>Players online: " +World.getPlayers().size());
                //new Thread(new HighscoresHandler(player)).start();
                //new Thread(new HighscoresHandler(player)).start();
                //new Thread(new HighscoresHandler(player)).start();
                //new Thread(new FoxVote(player)).start();
                //new Thread(new Highscores(player)).start();
                System.out.println("[World] Deregistering player - [username, host] : [" + player.getUsername() + ", " + player.getHostAddress() + "]");
                player.getSession().setState(SessionState.LOGGING_OUT);
                ConnectionHandler.remove(player.getHostAddress());
                player.setTotalPlayTime(player.getTotalPlayTime() + player.getRecordedLogin().elapsed());
                player.getPacketSender().sendInterfaceRemoval();
                if (player.getCannon() != null) {
                    DwarfMultiCannon.pickupCannon(player, player.getCannon(), true);
                }
                if (exception && player.getResetPosition() != null) {
                    player.moveTo(player.getResetPosition());
                    player.setResetPosition(null);
                }
                if (player.getRegionInstance() != null) {
                    player.getRegionInstance().destruct();
                }


                if (player.getRights() == PlayerRights.MODERATOR || player.getRights() == PlayerRights.ADMINISTRATOR || player.getRights() == PlayerRights.SUPPORT || player.getRights() == PlayerRights.DEVELOPER || player.getRights() == PlayerRights.OWNER) {
                    StaffList.logout(player);
                }
                Hunter.handleLogout(player);
                Locations.logout(player);
                player.getSummoning().unsummon(false, false);
                player.getFarming().save();
                BountyHunter.handleLogout(player);
                ClanChatManager.leave(player, false);
                player.getRelations().updateLists(false);
                PlayersOnlineInterface.remove(player);
                TaskManager.cancelTasks(player.getCombatBuilder());
                TaskManager.cancelTasks(player);
                player.save();
                World.getPlayers().remove(player);
                session.setState(SessionState.LOGGED_OUT);
                World.updatePlayersOnline();
                player.setRegionInstance(null);
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }
}
